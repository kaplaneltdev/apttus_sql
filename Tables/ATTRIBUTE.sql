USE [ClassIntegration]
GO
/****** Object:  Table [APTTUS].[ATTRIBUTE]    Script Date: 08/04/2018 17:41:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [APTTUS].[ATTRIBUTE](
	[AttributeId] [int] IDENTITY(1,1) NOT NULL,
	[AttributeName] [varchar](200) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[AttributeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [APTTUS].[ATTRIBUTE] ON 

INSERT [APTTUS].[ATTRIBUTE] ([AttributeId], [AttributeName]) VALUES (1, N'Homestay Room Type')
INSERT [APTTUS].[ATTRIBUTE] ([AttributeId], [AttributeName]) VALUES (2, N'Residence Room Type')
INSERT [APTTUS].[ATTRIBUTE] ([AttributeId], [AttributeName]) VALUES (3, N'Location(Zone)')
INSERT [APTTUS].[ATTRIBUTE] ([AttributeId], [AttributeName]) VALUES (4, N'Residence Name')
INSERT [APTTUS].[ATTRIBUTE] ([AttributeId], [AttributeName]) VALUES (5, N'Private Bath')
INSERT [APTTUS].[ATTRIBUTE] ([AttributeId], [AttributeName]) VALUES (6, N'Meal Options')
INSERT [APTTUS].[ATTRIBUTE] ([AttributeId], [AttributeName]) VALUES (7, N'Intensive Course Type')
INSERT [APTTUS].[ATTRIBUTE] ([AttributeId], [AttributeName]) VALUES (14, N'Special Diet')
INSERT [APTTUS].[ATTRIBUTE] ([AttributeId], [AttributeName]) VALUES (16, N'airport_station')
INSERT [APTTUS].[ATTRIBUTE] ([AttributeId], [AttributeName]) VALUES (17, N'vehicle')
INSERT [APTTUS].[ATTRIBUTE] ([AttributeId], [AttributeName]) VALUES (18, N'Fixed Weeks')
INSERT [APTTUS].[ATTRIBUTE] ([AttributeId], [AttributeName]) VALUES (19, N'Intensity')
INSERT [APTTUS].[ATTRIBUTE] ([AttributeId], [AttributeName]) VALUES (24, N'Exam Type')
SET IDENTITY_INSERT [APTTUS].[ATTRIBUTE] OFF
