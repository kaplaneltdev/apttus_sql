USE [ClassIntegration]
GO
/****** Object:  Table [APTTUS].[PRODUCTGROUP]    Script Date: 08/04/2018 17:41:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [APTTUS].[PRODUCTGROUP](
	[ProductGroupId] [int] IDENTITY(1,1) NOT NULL,
	[ProductGroupName] [varchar](200) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [APTTUS].[PRODUCTGROUP] ON 

INSERT [APTTUS].[PRODUCTGROUP] ([ProductGroupId], [ProductGroupName]) VALUES (1, N'Products without Attributes')
INSERT [APTTUS].[PRODUCTGROUP] ([ProductGroupId], [ProductGroupName]) VALUES (2, N'Homestay')
INSERT [APTTUS].[PRODUCTGROUP] ([ProductGroupId], [ProductGroupName]) VALUES (3, N'Residence')
INSERT [APTTUS].[PRODUCTGROUP] ([ProductGroupId], [ProductGroupName]) VALUES (4, N'Intensive Courses')
INSERT [APTTUS].[PRODUCTGROUP] ([ProductGroupId], [ProductGroupName]) VALUES (5, N'Extra Night Homestay')
INSERT [APTTUS].[PRODUCTGROUP] ([ProductGroupId], [ProductGroupName]) VALUES (6, N'Special Diet')
INSERT [APTTUS].[PRODUCTGROUP] ([ProductGroupId], [ProductGroupName]) VALUES (8, N'Extra Night Residence')
INSERT [APTTUS].[PRODUCTGROUP] ([ProductGroupId], [ProductGroupName]) VALUES (9, N'Transfer Service')
INSERT [APTTUS].[PRODUCTGROUP] ([ProductGroupId], [ProductGroupName]) VALUES (10, N'Academic Semester')
INSERT [APTTUS].[PRODUCTGROUP] ([ProductGroupId], [ProductGroupName]) VALUES (11, N'Academic Year')
INSERT [APTTUS].[PRODUCTGROUP] ([ProductGroupId], [ProductGroupName]) VALUES (12, N'CELTA')
INSERT [APTTUS].[PRODUCTGROUP] ([ProductGroupId], [ProductGroupName]) VALUES (14, N'Exam')
INSERT [APTTUS].[PRODUCTGROUP] ([ProductGroupId], [ProductGroupName]) VALUES (15, N'Fixed Length Courses')
INSERT [APTTUS].[PRODUCTGROUP] ([ProductGroupId], [ProductGroupName]) VALUES (16, N'Evening Courses')
SET IDENTITY_INSERT [APTTUS].[PRODUCTGROUP] OFF
