USE [ClassIntegration]
GO
/****** Object:  Table [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE]    Script Date: 08/04/2018 17:41:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE](
	[ProductGroupAttributeId] [int] IDENTITY(1,1) NOT NULL,
	[ProductGroupId] [int] NOT NULL,
	[AttributeId] [int] NOT NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ON 

INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupAttributeId], [ProductGroupId], [AttributeId]) VALUES (1, 2, 1)
INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupAttributeId], [ProductGroupId], [AttributeId]) VALUES (2, 2, 3)
INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupAttributeId], [ProductGroupId], [AttributeId]) VALUES (3, 2, 5)
INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupAttributeId], [ProductGroupId], [AttributeId]) VALUES (4, 2, 6)
INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupAttributeId], [ProductGroupId], [AttributeId]) VALUES (5, 3, 2)
INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupAttributeId], [ProductGroupId], [AttributeId]) VALUES (6, 3, 4)
INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupAttributeId], [ProductGroupId], [AttributeId]) VALUES (7, 3, 5)
INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupAttributeId], [ProductGroupId], [AttributeId]) VALUES (8, 3, 6)
INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupAttributeId], [ProductGroupId], [AttributeId]) VALUES (9, 4, 7)
INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupAttributeId], [ProductGroupId], [AttributeId]) VALUES (11, 5, 1)
INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupAttributeId], [ProductGroupId], [AttributeId]) VALUES (19, 8, 1)
INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupAttributeId], [ProductGroupId], [AttributeId]) VALUES (20, 8, 5)
INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupAttributeId], [ProductGroupId], [AttributeId]) VALUES (21, 8, 6)
INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupAttributeId], [ProductGroupId], [AttributeId]) VALUES (23, 8, 4)
INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupAttributeId], [ProductGroupId], [AttributeId]) VALUES (24, 9, 16)
INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupAttributeId], [ProductGroupId], [AttributeId]) VALUES (25, 9, 17)
INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupAttributeId], [ProductGroupId], [AttributeId]) VALUES (26, 10, 18)
INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupAttributeId], [ProductGroupId], [AttributeId]) VALUES (27, 10, 19)
INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupAttributeId], [ProductGroupId], [AttributeId]) VALUES (12, 5, 3)
INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupAttributeId], [ProductGroupId], [AttributeId]) VALUES (13, 5, 5)
INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupAttributeId], [ProductGroupId], [AttributeId]) VALUES (14, 5, 6)
INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupAttributeId], [ProductGroupId], [AttributeId]) VALUES (15, 6, 14)
INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupAttributeId], [ProductGroupId], [AttributeId]) VALUES (28, 11, 18)
INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupAttributeId], [ProductGroupId], [AttributeId]) VALUES (29, 11, 19)
INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupAttributeId], [ProductGroupId], [AttributeId]) VALUES (30, 12, 18)
INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupAttributeId], [ProductGroupId], [AttributeId]) VALUES (31, 13, 23)
SET IDENTITY_INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] OFF
/****** Object:  Index [UK_PRODUCT_ATTRIBUTE_CLASSDB]    Script Date: 08/04/2018 17:41:10 ******/
ALTER TABLE [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ADD  CONSTRAINT [UK_PRODUCT_ATTRIBUTE_CLASSDB] UNIQUE NONCLUSTERED 
(
	[ProductGroupId] ASC,
	[AttributeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
