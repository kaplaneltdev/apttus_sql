USE [ClassIntegration]
GO
/****** Object:  StoredProcedure [APTTUS].[CreateInvoiceLine]    Script Date: 18/11/2020 12:32:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [APTTUS].[CreateInvoiceLine] 

	@intEnrolId VARCHAR(10),
	@intRecordId VARCHAR(10),
	@intModuleId VARCHAR(10),
	@txtCode VARCHAR(25),
	@source VARCHAR(30),
	@txtNarrative VARCHAR(200),
	@txtLanguageName VARCHAR(200),
	@intPriceItemId VARCHAR(10),
	@dteFromDate DATETIME,
	@dteToDate DATETIME,
	@curBasic MONEY,
	@curDue MONEY,
	@curMemoDue MONEY,
	@intDiscountPercent VARCHAR(10),
	@curDiscount MONEY,
	@txtInvoiceType CHAR(1),
	@intPriceListId VARCHAR(10),
	@intTaxId VARCHAR(10),
	@curTax MONEY,
	@blnDirect VARCHAR(1),
	@quantity varchar(10),
	@currencyCode varchar(3)

	
AS

DECLARE @sql nvarchar(max);
	DECLARE @duplicateFlag BIT = 0;
	SET NOCOUNT ON; 
	DECLARE @intEPackageId int = NULL; 
	SET @sql = 'SELECT @duplicateFlag =  CASE WHEN ISNULL(d.cnt,0) > ISNULL(a.cnt,0)    THEN 1
	ELSE  0 END 
	FROM (select  count(intinvoicelineid) as cnt,intEnrolId,intModuleId,intRecordId FROM ' + @source + '.[dbo].[tblInvoiceLine] where   txtInvoiceType = ''D'' 
group by intEnrolId,intModuleId,intRecordId) d
left outer join (select  count(intinvoicelineid) as cnt,intEnrolId,intModuleId,intRecordId from
	 ' +  @source + '.[dbo].[tblInvoiceLine]  WHERE txtInvoiceType = ''C''  
	 group by intEnrolId,intModuleId,intRecordId) a on a.intEnrolId = d.intEnrolId and a.intModuleId = d.intModuleId and a.intRecordId = d.intRecordId
WHERE
	d.intEnrolId = CAST(@intEnrolId AS INT) AND
	d.intRecordId = CAST(@intRecordId AS INT)
	AND d.intModuleId = CAST(@intModuleId AS INT)
	';
	EXECUTE sp_executesql @sql,N'@source  VARCHAR(30),@intEnrolId  VARCHAR(10),@intRecordId VARCHAR(10),
	@intModuleId VARCHAR(10),@duplicateFlag BIT OUT',@source,@intEnrolId,@intRecordId,@intModuleId,@duplicateFlag OUT;
	SELECT @duplicateFlag;
	IF @duplicateFlag = 0
	BEGIN
	
	--SET @sql = 'SELECT @intEPackageId = CASE WHEN @intmoduleid in (1600,1800,1900,2000) THEN intEPackageId WHEN @intModuleId = 1550 THEN @intRecordId END
	--FROM ['+@source+'].[dbo].[tblEPackage] EP
	--INNER JOIN ['+@source+'].[dbo].[tblPackage] P on EP.intPackageId = P.intPackageId
	--INNER JOIN  ['+@source+'].[dbo].[tblPackageItem] item on item.intPackageId = P.intPackageId AND item.intPriceItemId = @intPriceItemId and intModuleId = @intModuleId '
	--select @sql;
	--EXEC sp_executesql @sql, N'@intEPackageId INT OUT,@intPriceItemId INT,@intModuleId int,@intRecordId int ', @intEPackageId OUT,@intPriceItemId,@intModuleId,@intRecordId;
	IF @intRecordId  IS NULL 
	BEGIN
		THROW 50000, 'No Booking Id passed to Debit Line Creation!', 1
	END
	IF  @intPriceItemId IS NULL
	BEGIN
		THROW 50000, 'No PriceItemId passed to Debit Line Creation!', 1
	END
	SET @sql = '
DECLARE @Rowcount INT;

DECLARE @blnLive  BIT ;
SELECT @blnLive = 1;



	
	SELECT @intEPackageId = intEPackageId
		FROM ['+@source+'].[dbo].[tblEPackage] EP
		LEFT OUTER JOIN ['+@source+'].[dbo].[tblPackage] P on EP.intPackageId = P.intPackageId AND p.blnActive = 1 AND EP.intEnrolid  = @intenrolid
		LEFT OUTER JOIN  ['+@source+'].[dbo].[tblPackageItem] item on item.intPackageId = P.intPackageId AND item.intPriceItemId = @intPriceItemId and intModuleId = @intModuleId
		WHERE ( EP.intEnrolId = @intEnrolId AND @intModuleId = 1550 ) OR item.intPackageId IS NOT NULL;
		
select @intEPackageId as packageid,@intPriceItemId as intPriceItemId;

--DECLARE @txtLanguageName VARCHAR(200)= '''';

DECLARE  @currency_code varchar(3);
SELECT @currency_code = CASE WHEN @source = ''CFW_ASPECT_GBR'' AND @currencyCode = ''EUR'' THEN ''EUR''
							 WHEN @source = ''CFW_ASPECT_IRL'' THEN @currencyCode
					         WHEN @source = ''CFW_ASPECT_AUS'' THEN ''AUD''
							 WHEN @source = ''CFW_ASPECT_CAN'' THEN ''CAD''
							 WHEN @source = ''CFW_ASPECT_GBR'' THEN ''GBP''
							 WHEN @source = ''CFW_ASPECT_NZL'' THEN ''NZD''
							 WHEN @source = ''CFW_ASPECT_USA'' THEN ''USD''
						END;

DECLARE @intCurrencyId INT;
DECLARE @intCommissionAgentCurrencyId INT;
DECLARE @intMemoCurrencyId INT;
SELECT @intCurrencyId = intCurrencyId,
@intCommissionAgentCurrencyId = CASE WHEN @intDiscountPercent IS NULL THEN 0
									 ELSE intCurrencyId
								END,
@intMemoCurrencyId = intCurrencyId
FROM [' +@source+ '].[dbo].tblCurrency
WHERE txtCode = @currency_code;
DECLARE @dteCreationDate DATE;
SELECT @dteCreationDate = CONVERT(DATE, getdate());
DECLARE @dteBookingDate DATE;
SELECT @dteBookingDate = CONVERT(DATE, getdate());
DECLARE @intNumberOfUnits INT;
SELECT @intNumberOfUnits = CAST(CAST(CASE WHEN a.productGroupId = 5 OR a.productCode = ''XRESIDENCE'' THEN CAST(''0'' AS INT)   WHEN @intModuleId = 1550 THEN CEILING(CONVERT(FLOAT,DATEDIFF(dd,@dteFromDate,@dteToDate))/7 ) ELSE @quantity END AS DECIMAL) AS INT)
FROM [apttus].[product] a 
left outer join [' +@source+ '].[dbo].tblPriceItem p on a.productCode = p.txtCode 
left outer join [' +@source+ '].[dbo].tblePackage pck on a.productCode =  pck.txtCode 
where p.intPriceItemId = @intPriceItemId or pck.intePackageId = @intRecordId;
DECLARE @intNumberOfPartUnits INT;
SELECT @intNumberOfPartUnits = CAST(CAST(CASE WHEN a.productGroupId = 5 OR a.productCode = ''XRESIDENCE''  THEN @quantity WHEN @intModuleId = 1550 THEN DATEDIFF(dd,@dteFromDate,@dteToDate)%7  ELSE  CAST(''0'' AS  INT)  END AS DECIMAL) AS INT)
FROM [apttus].[product] a 
left outer join [' +@source+ '].[dbo].tblPriceItem p on a.productCode = p.txtCode 
left outer join [' +@source+ '].[dbo].tblePackage pck on a.productCode = pck.txtCode 
where p.intPriceItemId = @intPriceItemId or pck.intePackageId = @intRecordId;
select @intNumberOfUnits as intNumberOfUnits;
DECLARE @intCommissionPercent DECIMAL(9) = 0.0000;
DECLARE @blnAlwaysCommission BIT = 0;
DECLARE @curCommission MONEY = 0.00; 
DECLARE @int2ndCommissionPercent DECIMAL(9) = 0.0000;
DECLARE @cur2ndCommission MONEY = 0.00;
DECLARE @blnPrinted BIT = 0; 
DECLARE @blnPosted BIT = 0;
DECLARE @decStatisticalWeeks NUMERIC(18,4);
SELECT @decStatisticalWeeks = CASE WHEN a.productGroupId = 5 OR a.productCode = ''XRESIDENCE''  THEN CAST(datediff(d,@dteFromDate,@dteToDate) AS NUMERIC(18,4))/7  ELSE  CAST(@quantity AS NUMERIC(18,4)) END
FROM [apttus].[product] a 
left outer join [' +@source+ '].[dbo].tblPriceItem p on a.productCode = p.txtCode 
left outer join [' +@source+ '].[dbo].tblePackage pck on a.productCode = pck.txtCode 
where p.intPriceItemId = @intPriceItemId or pck.intePackageId = @intRecordId;
DECLARE @memInvoicePriceCalculation VARCHAR(5120) = '''';
DECLARE @memMemoPriceCalculation VARCHAR(5120) = '''';
DECLARE @txtAmended CHAR(1) = '''';
DECLARE @blnNotRefinanced BIT = 0;

DECLARE @curCostPrice MONEY = 0.00;


BEGIN


		SET NOCOUNT OFF;
		
		INSERT INTO [' +@source+ '].[dbo].tblInvoiceLine
		(
			intEnrolId,
			
			txtInvoiceType,
			intModuleId,
			intRecordId,
			intPriceListId,
			intPriceItemId,
			intCurrencyId,
			intCommissionAgentCurrencyId,
			intMemoCurrencyId,
			intTaxId,
			blnDirect,
			txtCode,
			txtNarrative,
			dteCreationDate,
			dteBookingDate,
			dteFromDate,
			dteToDate,
			intNumberOfUnits,
			intNumberOfPartUnits,
			curBasic,
			intDiscountPercent,
			curDiscount,
			curTax,
			curDue,
			curMemoDue,
			intCommissionPercent,
			blnAlwaysCommission,
			curCommission,
			int2ndCommissionPercent,
			cur2ndCommission,
			blnLive,
			blnPrinted,
			blnPosted,
			decStatisticalWeeks,
			memInvoicePriceCalculation,
			memMemoPriceCalculation,
			txtAmended,
			blnNotRefinanced,
			txtLanguageName,
			curCostPrice,
			intEPackageId
			
	
		)
		VALUES
		(
			CAST (@intEnrolId AS VARCHAR(10)),
			
			@txtInvoiceType,
			CAST(@intModuleId AS VARCHAR(10)),
			CAST (@intRecordId AS VARCHAR(10)),
			CAST(@intPriceListId AS VARCHAR(10)),
			CAST(@intPriceItemId AS VARCHAR(10)),
			@intCurrencyId,
			@intCommissionAgentCurrencyId,
			@intMemoCurrencyId,
			CAST(@intTaxId AS VARCHAR(10)),
			CAST(@blnDirect AS BIT),
			@txtCode,
			@txtNarrative,
			@dteCreationDate,
			@dteBookingDate,
			@dteFromDate,
			@dteToDate,
			@intNumberOfUnits,
			@intNumberOfPartUnits,
			@curBasic,
			CAST(@intDiscountPercent AS VARCHAR(10)),
			@curDiscount,
			@curTax,
			@curDue,
			@curMemoDue,
			@intCommissionPercent,
			@blnAlwaysCommission,
			@curCommission,
			@int2ndCommissionPercent,
			@cur2ndCommission,
			@blnLive,
			@blnPrinted,
			@blnPosted,
			@decStatisticalWeeks,
			@memInvoicePriceCalculation,
			@memMemoPriceCalculation,
			@txtAmended,
			@blnNotRefinanced,
			ISNULL(@txtLanguageName,'' ''),
			@curCostPrice,
			@intEPackageId
			
		)

		
		IF(@@ROWCOUNT != 1)
			RAISERROR(''No Invoice Line inserted!'', 16, 1)
	
		END';


		select @sql;
		EXECUTE sp_executesql @sql,N'@intEnrolId VARCHAR(10), @intRecordId VARCHAR(10), @intModuleId VARCHAR(10), @txtCode VARCHAR(25),
	@source VARCHAR(30),@currencyCode varchar(3), @txtNarrative VARCHAR(200), @intPriceItemId VARCHAR(10), 
	@dteFromDate DATETIME, @dteToDate DATETIME, @curBasic MONEY, @curDue MONEY, @curMemoDue MONEY, @intDiscountPercent VARCHAR(10),
	@curDiscount MONEY, @txtInvoiceType CHAR(1), @intPriceListId VARCHAR(10), @intTaxId VARCHAR(10),
	@curTax MONEY, @blnDirect VARCHAR(1),@quantity varchar(10),@txtLanguageName VARCHAR(200),@intEPackageId int', @intEnrolId, @intRecordId, @intModuleId, @txtCode, @source, @currencyCode ,@txtNarrative,
@intPriceItemId, @dteFromdate,
@dteTodate, @curBasic, @curDue, @curMemoDue, @intDiscountPercent, @curDiscount,
@txtInvoiceType, @intPriceListId, @intTaxId, @curTax,@blnDirect,@quantity,@txtLanguageName,@intEPackageId;




end
		
RETURN 0






