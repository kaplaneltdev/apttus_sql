USE [ClassIntegration]
GO
/****** Object:  StoredProcedure [APTTUS].[UpsertGroup]    Script Date: 08/01/2020 14:34:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [APTTUS].[UpsertGroup] 
@source varchar(30),
@txtExternalReference       varchar(30),
@txtEnrolBookingStatus     varchar(30),
@txtEnrolArrivalStatus varchar(30),
@txtGroupName        varchar(30),
@txtGroupCode        varchar(20),
@txtSchoolCode       varchar(10),
@txtMainAgentCode    varchar(45),
@txtCommissionAgentCode    varchar(45),
@dteFromDate date,
@dteToDate date,
@currencyCode varchar(3),
@numberOfStudents int



AS
BEGIN

DECLARE @sql nvarchar(max);


DECLARE @intSchoolId int;
DECLARE @intGroupId_OUT int;




DECLARE @blnGroupInvoice	BIT = 1;
DECLARE @blnCreditAndReInvoice BIT = 1;


SET @sql = '
SELECT @intSchoolId = intSchoolId
FROM [' +@source+ '].[dbo].tblSchool 
WHERE txtCode = @txtSchoolCode;';
EXECUTE sp_executesql @sql,N'@txtSchoolCode     varchar(10),@intSchoolId int out',@txtSchoolCode,@intSchoolId out;
DECLARE @intEnrolId  int;



DECLARE @intEnrolBookingStatusId int;
SET @sql = 'SELECT @intEnrolBookingStatusId = intEnrolBookingStatusId
FROM [' +@source+ '].[dbo].tblEnrolBookingStatus
WHERE txtName = @txtEnrolBookingStatus;';
EXECUTE sp_executesql @sql,N' @txtEnrolBookingStatus varchar(30), @intEnrolBookingStatusId int out',@txtEnrolBookingStatus,@intEnrolBookingStatusId out;

DECLARE @intEnrolArrivalStatusId int;
SET @sql = 'SELECT @intEnrolArrivalStatusId = intEnrolArrivalStatusId
FROM [' +@source+ '].[dbo].tblEnrolArrivalStatus
WHERE txtName = @txtEnrolArrivalStatus;';
EXECUTE sp_executesql @sql,N' @txtEnrolArrivalStatus varchar(30), @intEnrolArrivalStatusId int out',@txtEnrolArrivalStatus, @intEnrolArrivalStatusId out;


DECLARE @intGroupId int;
SET @sql = 'SELECT @intGroupId = intGroupId
FROM classintegration.apttus.group_mapping
WHERE txtExternalRef = substring(@txtExternalReference,0,19)';
EXECUTE sp_executesql @sql,N'@txtExternalReference varchar(30), @intGroupId int out',@txtExternalReference,@intGroupId out;


DECLARE  @currency_code varchar(3);
SET @sql = 'SELECT @currency_code = CASE WHEN @source = ''CFW_ASPECT_GBR'' AND @currencyCode = ''EUR'' THEN ''EUR''
                                        WHEN @source = ''CFW_ASPECT_AUS'' THEN ''AUD''
                                        WHEN @source = ''CFW_ASPECT_CAN'' THEN ''CAD''
                                        WHEN @source = ''CFW_ASPECT_GBR'' THEN ''GBP''
                                        WHEN @source = ''CFW_ASPECT_IRL'' THEN ''EUR''
                                        WHEN @source = ''CFW_ASPECT_NZL'' THEN ''NZD''
                                        WHEN @source = ''CFW_ASPECT_USA'' THEN ''USD''
                                    END;';
EXECUTE sp_executesql @sql,N'@source varchar(30), @currencyCode varchar(3),@currency_code varchar(3) out',@source, @currencyCode, @currency_code out;


DECLARE @intAgentCurrencyId int;

DECLARE @intMemoCurrencyId int;
SET @sql = 'SELECT 
@intAgentCurrencyId = intCurrencyId,
@intMemoCurrencyId = intCurrencyId
FROM [' +@source+ '].[dbo].tblCurrency
WHERE txtCode = @currency_code;';
EXECUTE sp_executesql @sql,N'@intAgentCurrencyId int out, @intMemoCurrencyId int out,@currency_code varchar(3)', @intAgentCurrencyId out,@intMemoCurrencyId out, @currency_code;

DECLARE @intMainAgentId int;
SET @sql = 'SELECT @intMainAgentId = intAgentId
FROM [' +@source+ '].[dbo].tblAgent
WHERE txtCode = @txtMainAgentCode
AND intAgentStatusId = 200
AND intAgentcurrencyId = @intAgentCurrencyId;';
EXECUTE sp_executesql @sql,N'@intAgentCurrencyId int, @txtMainAgentCode varchar(45),@intMainAgentId int out', @intAgentCurrencyId, @txtMainAgentCode, @intMainAgentId out;

DECLARE @intCommissionAgentId int;
SET @sql = 'SELECT @intCommissionAgentId = intAgentId 
FROM [' +@source+ '].[dbo].tblAgent 
WHERE txtCode = @txtCommissionAgentCode
AND intAgentStatusId = 200
AND intAgentcurrencyId = @intAgentCurrencyId;';
EXECUTE sp_executesql @sql,N'@intAgentCurrencyId int, @txtCommissionAgentCode varchar(45),@intCommissionAgentId int out', @intAgentCurrencyId, @txtCommissionAgentCode, @intCommissionAgentId out;
SELECT @intCommissionAgentId = CASE WHEN @intCommissionAgentId IS NULL THEN @intMainAgentId ELSE @intCommissionAgentId END;
DECLARE @intAgentPriceListId int = 1000;
DECLARE @intStudentPriceListId int = 1000;


DECLARE @intDurationWeeks INT;
SET @sql = 'SELECT @intDurationWeeks = DATEDIFF(dd,@dteFromDate,@dteToDate)/7 ; ';
EXECUTE sp_executesql @sql,N'@dteFromDate date,@dteToDate date,@intDurationWeeks  int out',@dteFromDate,@dteToDate,@intDurationWeeks out;

DECLARE @intDurationDays INT;
SET @sql = 'SELECT @intDurationDays = DATEDIFF(dd,@dteFromDate,@dteToDate)%7;';
EXECUTE sp_executesql @sql,N'@dteFromDate date,@dteToDate date,@intDurationDays int out',@dteFromDate,@dteToDate,@intDurationDays out;



SET @sql = '




       SET NOCOUNT ON;
       IF (ISNULL(@intGroupId,0) != 0)
       BEGIN
              UPDATE [' +@source+ '].[dbo].[tblGroup]
              SET intSchoolId  = @intSchoolId ,
              intAmendedUserId = 9999,
              dteAmendedDateTime = getdate(),
              intEnrolBookingStatusId = CASE WHEN @intEnrolBookingStatusId = 100 AND intEnrolBookingStatusId IN (200,300) THEN intEnrolBookingStatusId
                                             WHEN  @intEnrolBookingStatusId = 200 AND  intEnrolBookingStatusId = 300 THEN intEnrolBookingStatusId
                                        ELSE @intEnrolBookingStatusId END,
              intDurationWeeks = @intDurationWeeks,
              intDurationDays = @intDurationDays,
              intMainAgentId  = @intMainAgentId,
              intCommissionAgentId = @intCommissionAgentId,
              intCurrencyId = @intAgentCurrencyId,
              intMemoCurrencyId = @intAgentCurrencyId,
              intPriceListId = @intAgentPriceListId,
              dteFromDate = @dteFromDate,
              dteToDate = @dteToDate,
              dteInvoiceDueDate = @dteFromDate,
              blnCreditAndReInvoice = 1,
			  blnGroupInvoice = 1,
			  txtName = @txtGroupName,
			  txtCode = @txtGroupCode ,
			  intNumberOfBookings = @numberOfStudents,
			  intNumberOfArrivals = @numberOfStudents
              WHERE intGroupId = @intGroupId;

              IF(@@ROWCOUNT != 1)
                  RAISERROR(''No Group updated!'', 16, 1)
			  
			  update classintegration.apttus.group_mapping set datemodified = getdate() where intGroupId = @intGroupId;

       END ' ;
       EXECUTE sp_executesql @sql,N'@source varchar(30),
@intEnrolBookingStatusId int,
@intEnrolArrivalStatusId int,
@txtGroupName        varchar(30),
@txtGroupCode        varchar(20),
@intSchoolId	     int,
@intMainAgentId	     int,
@intCommissionAgentId int,
@intDurationWeeks     int,	
@intDurationDays      int,
@intAgentCurrencyId   int,
@intAgentPriceListId int,
@dteFromDate date,
@dteToDate date,
@numberOfStudents int,
@intGroupId int output
',@source,
@intEnrolBookingStatusId,
@intEnrolArrivalStatusId,
@txtGroupName,
@txtGroupCode,
@intSchoolId,
@intMainAgentId,
@intCommissionAgentId,
@intDurationWeeks,
@intDurationDays,
@intAgentCurrencyId,
@intAgentPriceListId,
@dteFromDate,
@dteToDate,
@numberOfStudents,
@intGroupId output;

    IF (ISNULL(@intGroupId,0) = 0)
       BEGIN
		SET @sql ='   INSERT INTO [' +@source+ '].[dbo].[tblGroup](
              intDurationWeeks,
              intDurationDays,
              intCreatedUserId,
              dteCreatedDateTime,
              intAmendedUserId,
              dteAmendedDateTime,
              intEnrolBookingStatusId,
              intEnrolArrivalStatusId,
              intModuleId,
              intSchoolId,
              intMainAgentId,
              intCommissionAgentId,
              intCurrencyId,
              intMemoCurrencyId,
              intPriceListId,
              dteCreationDate,
              dteFromDate,
              dteToDate,
              dteInvoiceDueDate,
              txtCode,
              txtName,
              txtInvoiceComment,
              txtNote1,
              txtNote2,
              txtNote3,
              txtAnalysis1,
              txtAnalysis2,
              txtAnalysis3,
              txtAnalysis4,
              txtAnalysis5,
              txtAnalysis6,
              blnCreditAndReInvoice,
			  blnGroupInvoice,
			  intNumberOfBookings,
			  intNumberOfArrivals
			  )
              VALUES (
              @intDurationWeeks,
              @intDurationDays,
              9999,
              GETDATE(),
              9999,
              GETDATE(),
              @intEnrolBookingStatusId,
              @intEnrolArrivalStatusId,
              1400,
              @intSchoolId,
              @intMainAgentId,
              @intCommissionAgentId,
              @intAgentCurrencyId,
              @intAgentCurrencyId,
              @intAgentPriceListId,
              CAST(GETDATE() AS DATE),
              @dteFromDate,
              @dteToDate,
              @dteFromDate,
              @txtGroupCode,
              @txtGroupName,
              '''',
              '''',
              '''',
              '''',
              '''',
              '''',
              '''',
              '''',
              '''',
              '''',
              1,
			  1,
			  @numberOfStudents,
			  @numberOfStudents);
			  SELECT @intGroupId = SCOPE_IDENTITY();
			  insert into classintegration.apttus.group_mapping(intGroupId,txtExternalRef,DateCreated,DateModified) values (@intGroupId,substring(@txtExternalReference,0,19),getdate(),getdate());
              ';
			  EXECUTE sp_executesql @sql,N'@source varchar(30),
@intEnrolBookingStatusId int,
@intEnrolArrivalStatusId int,
@txtGroupName        varchar(30),
@txtGroupCode        varchar(20),
@intSchoolId	     int,
@intMainAgentId	     int,
@intCommissionAgentId int,
@intDurationWeeks     int,	
@intDurationDays      int,
@intAgentCurrencyId   int,
@intAgentPriceListId int,
@dteFromDate date,
@dteToDate date,
@txtExternalReference       varchar(30),
@numberOfStudents int,
@intGroupId int output',@source,
@intEnrolBookingStatusId,
@intEnrolArrivalStatusId,
@txtGroupName,
@txtGroupCode,
@intSchoolId,
@intMainAgentId,
@intCommissionAgentId,
@intDurationWeeks,
@intDurationDays,
@intAgentCurrencyId,
@intAgentPriceListId,
@dteFromDate,
@dteToDate,
@txtExternalReference,
@numberOfStudents,
@intGroupId output;


	   END


              
RETURN 0
END
