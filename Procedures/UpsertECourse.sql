USE [ClassIntegration]
GO
/****** Object:  StoredProcedure [APTTUS].[UpsertECourse]    Script Date: 18/11/2020 12:27:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [APTTUS].[UpsertECourse] 
	-- Add the parameters for the stored procedure here
	@source  varchar(20),
	@intEnrolId varchar(10),
	@txtSchoolCode varchar(3),
	@productCode varchar(30),
	@ProductName varchar(100),
	@CourseIntensity varchar(100),
	@txtECourseStatus varchar(30),
	@blndirect varchar(1),
	@txtNote varchar(5120),
	--@txtCode VARCHAR(25),
	@dteFromDate DATETIME,
	@dteToDate DATETIME,
	@quantity varchar(10),
	@txtExternalRef varchar(30),
	@txtExternalRefDerivedFrom varchar(30),
	@fixedWeeks varchar(100),
	@intensity varchar(100),
	@numberOfLessons varchar(10),
	@intECourseId_OUT INT OUTPUT,
	@intECourseBookingId_OUT INT OUTPUT,
	@txtPriceItemCode_OUT varchar(30) OUTPUT,
	@intPriceItemId_OUT int output,
	@txtPriceItemName_OUT VARCHAR(100) OUTPUT
			
AS

BEGIN
	
		DECLARE @intSchoolId int;
		EXEC APTTUS.GetSchoolId @txtSchoolCode,@source,@intSchoolId OUT;
		-------IF(@productCode IS NOT NULL
		--IF @productName like '%Academic%' 
		--	SELECT @productName = 'Academic Year/Semester';
	
	
		DECLARE @weeks int = 0;
		IF @fixedWeeks != 'null'
			SELECT @weeks = CAST(CAST(@fixedweeks AS  decimal) as INT);

		IF @productName like '%Academic%' or @ProductName like '%Cambridge/EAP%' or @ProductName like 'Cambridge FCE%' or @ProductName like 'Cambridge CAE%'
			EXEC APTTUS.GetProductCode @ProductName,@source,'Fixed Weeks','Intensity',null,null,@weeks,@intensity,null,null, @productCode OUT;
		Else IF (@productName like '%Intensive Courses%'	or @ProductName  in ('GMAT/GRE Extension','GMAT/GRE Higher Score Guarantee','GMAT/GRE Online Access') )
			EXEC APTTUS.GetProductCode @ProductName,@source,'Intensive Course Type',null,null,null,@CourseIntensity,null,null,null, @ProductCode OUT;
		Else IF @productName in ( 'GMAT/GRE')
			EXEC APTTUS.GetProductCode @ProductName,@source,'Intensity',null,null,null,@intensity,null,null,null, @productCode OUT;
		Else 
			EXEC APTTUS.GetProductCode @ProductName,@source,null,null,null,null,null,null,null,null, @ProductCode OUT;
		DECLARE @intPriceItemId INT;
		DECLARE @query nvarchar(max);
	
		SET @query = '
		SELECT  @intPriceItemId' + char(61) + ' [intPriceItemId]  , @txtPriceItemName_OUT' + char(61) + ' [txtName]  
		FROM  [' + @source+ '].[dbo].[tblPriceItem]
		WHERE [txtCode] = '+ CHAR(39) + @ProductCode + CHAR(39) +
		' AND blnActive = 1 AND intSchoolId = ' +  CAST(@intSchoolId AS VARCHAR(4));
		--select @query;
		EXEC sp_executesql @query, N'@intPriceItemId INT OUT, @txtPriceItemName_OUT VARCHAR(100) OUT', @intPriceItemId OUT, @txtPriceItemName_OUT OUT;
		SELECT @txtPriceItemCode_OUT = @productCode,
		@intPriceItemId_OUT = @intPriceItemId;

		DECLARE @intEPackageId int = NULL; 
		SET @query = 'SELECT @intEPackageId = intEPackageId
		FROM ['+@source+'].[dbo].[tblEPackage] EP
		INNER JOIN ['+@source+'].[dbo].[tblPackage] P on EP.intPackageId = P.intPackageId AND P.intSchoolId = @intSchoolId AND p.blnActive = 1 AND EP.intEnrolid  = @intenrolid
		INNER JOIN  ['+@source+'].[dbo].[tblPackageItem] item on item.intPackageId = P.intPackageId AND item.intPriceItemId = @intPriceItemId and intModuleId = 1600 '
		--select @query;
		EXEC sp_executesql @query, N'@intEPackageId INT OUT,@intPriceItemId INT,@intSchoolId INT,@intenrolid int', @intEPackageId OUT,@intPriceItemId,@intSchoolId,@intEnrolId;

		IF  @intPriceItemId IS NULL
		BEGIN
			THROW 50000, 'No PriceItem mapped for this Course Booking!', 1
		END

		DECLARE @a  auditLog;
		DECLARE @operation VARCHAR(1);

		DECLARE @intECourseId INT;	
		DECLARE @intEcourseBookingId INT;	
		DECLARE @intNumberOfUnits INT ; 
		DECLARE @intNumberOfPartUnits INT = 0;
		DECLARE @intChargeUnits INT;
		DECLARE @intChargePartUnits INT = 0;
		DECLARE @intChargeRate INT;
		DECLARE @dteCreatedDateTime DATETIME;
		DECLARE @intCreatedUserId INT = 9999;
		DECLARE @dteCreationDate DATE;
		DECLARE @intAmendedUserId INT = 9999;
		DECLARE @dteAmendedDateTime DATETIME = getdate();
		DECLARE @intNumberOfLessons INT = 0;
		DECLARE @prevChargeUnits INT;
		DECLARE @prevChargeRate INT;
		DECLARE @prevNumberOfLessons INT;
		DECLARE @prevFromDate Date;
		DECLARE @prevToDate Date;
		DECLARE @prevDirectFlag BIT;
		DECLARE  @intCourseId INT;
		DECLARE @intEcourseStatusId INT;
		DECLARE @intDurationWeeksRounded INT;
		DECLARE @decStatisticalWeeks DEC;
		DECLARE @sql nvarchar(max);
		DECLARE @courseCode VARCHAR(30);
		SET NOCOUNT ON; 

		
		DECLARE @Rowcount INT;
				
		SELECT @dteCreatedDateTime = getdate();
		SELECT @dteCreationDate = CONVERT(DATE, getdate());
		SELECT @dteCreatedDateTime = getdate();
		SELECT @intNumberOfUnits = CAST(CAST( @quantity  AS DECIMAL) AS INT);
		SELECT @intChargeUnits = CAST(CAST( @quantity  AS DECIMAL) AS INT);
		SELECT @intChargeRate = CAST(CAST( @quantity  AS DECIMAL) AS INT);

		SET @sql = 'SELECT @intNumberOfLessons = CASE WHEN @numberOfLessons is null THEN intLessonsPerWeek
													  ELSE CAST(CAST(@numberOfLessons AS DECIMAL) AS INT) END 
		FROM [' +@source+ '].[dbo].tblPriceItem 
		WHERE intPriceItemId = @intPriceItemId
		AND intModuleId = 1600
		AND blnActive = 1';
		EXECUTE sp_executesql @sql, 	N'@intNumberOfLessons int output,@numberOfLessons varchar(10) output,@intPriceItemId int,@quantity varchar(10),@productCode varchar(30)', @intNumberOfLessons out,@numberOfLessons out,@intPriceItemId,@quantity,@productCode;
	
		SET @sql = 'SELECT @intEcourseId = intEcourseId,
		@prevChargeUnits = intChargeUnits,
		@prevChargeRate = intChargeRate,
		@prevNumberOfLessons = intNumberOfLessons,
		@prevFromDate = dteFromDate,
		@prevToDate = dteToDate, 
		@prevDirectFlag = blnDirect 
		FROM [' +@source+ '].[dbo].[tblECourse]
		WHERE txtExternalRef = @txtExternalRefDerivedFrom
		OR txtExternalRef = @txtExternalRef';
	

		EXECUTE sp_executesql @sql, 	N'@intEcourseId int output, @prevChargeUnits int output,@prevChargeRate int output, @prevNumberOfLessons int output, @prevFromDate date output, @prevToDate date output, @prevDirectFlag BIT output, @txtExternalRefDerivedFrom VARCHAR(30),@txtExternalRef VARCHAR(30),@source varchar(20) ', @intEcourseId out, @prevChargeUnits out,@prevChargeRate out, @prevNumberOfLessons out, @prevFromDate out, @prevToDate out, @prevDirectFlag out, @txtExternalRefDerivedFrom,@txtExternalRef,@source;
		
		SELECT @INTecourseid AS 'INTECOURSEID';
		SET @sql = 'SELECT @intCourseId = intCourseId,
		@courseCode = txtCode
		FROM [' +@source+ '].[dbo].[tblCourse]
		WHERE txtCode = (CASE WHEN @ProductCode = ''ASGEN6'' THEN ''ASGEN''
							  WHEN @ProductCode = ''ASINT6'' THEN ''ASINT''
							  WHEN @ProductCode = ''ENGINTCC'' THEN ''ENGINT''
							  WHEN @ProductCode like ''%PR'' THEN SUBSTRING(@ProductCode,0,CHARINDEX(''PR'',@ProductCode,0))
							  WHEN @ProductCode like ''%HSG'' THEN SUBSTRING(@ProductCode,0,CHARINDEX(''HSG'',@ProductCode,0))
							  WHEN @ProductCode  = ''IGREX'' THEN ''IGRE''
							  WHEN @ProductCode  = ''IGMATX'' THEN ''IGMAT''
							   WHEN @ProductCode  in ( ''CAMINT10'' , ''CAMINT12'' ) AND @source = ''CFW_ASPECT_NZL'' THEN ''CAMINT''
							  ELSE @ProductCode END)
		AND intSchoolId = @intSchoolId
		AND blnActive = 1;';
		EXECUTE sp_executesql @sql, 	N'@intSchoolId int, @productCode varchar(30) , @intCourseId int output,@source varchar(20),@courseCode varchar(30) output', @intSchoolId, @productCode, @intCourseId out,@source, @courseCode output;
		--SELECT @intDurationWeeksRounded = CAST(CAST(CASE WHEN @productCode IN ('OTOILS','TTOILS') AND @fixedWeeks = 'null' THEN CEILING(CONVERT(FLOAT,DATEDIFF(dd,@dteFromDate,DATEADD(dd,3,@dteToDate)))/7 ) WHEN @productCode IN ('OTOILS','TTOILS') AND @fixedWeeks != 'null' THEN @fixedWeeks ELSE @quantity END AS DECIMAL) AS INT);
		SELECT @intDurationWeeksRounded = CAST(CAST( @quantity  AS DECIMAL) AS INT);
		--SELECT @intDurationWeeksRounded = CEILING(CONVERT(FLOAT,DATEDIFF(dd,@dteFromDate,@dteToDate))/7 );


		--SELECT @decStatisticalWeeks = CAST(CASE WHEN @productCode IN ('OTOILS','TTOILS') AND @fixedWeeks = 'null' THEN CEILING(CONVERT(FLOAT,DATEDIFF(dd,@dteFromDate,DATEADD(dd,3,@dteToDate)))/7 ) WHEN @productCode IN ('OTOILS','TTOILS') AND @fixedWeeks != 'null' THEN @fixedWeeks ELSE @quantity END AS DECIMAL) ;
		SELECT @decStatisticalWeeks = CAST( @quantity  AS DECIMAL);
		--SELECT @decStatisticalWeeks = CONVERT(FLOAT,DATEDIFF(dd,@dteFromDate,@dteToDate))/7;
		

		SET @sql = 'SELECT @intEcourseStatusId  = intEcourseStatusId
		FROM [' +@source+ '].[dbo].[tblECourseStatus]
		WHERE  txtName = @txtECourseStatus;';
		EXECUTE sp_executesql @sql, 	N'@intEcourseStatusId int output, @txtECourseStatus varchar(30) , @source varchar(30)',@intECourseStatusId out, @txtECourseStatus,@source;

		SET @sql = 'SELECT @intEcourseBookingId = intEcourseBookingId
		FROM [' +@source+ '].[dbo].[tblECourseBooking]
		WHERE intECourseId = @intECourseId;';
		
		EXECUTE sp_executesql @sql, 	N'@intEcourseBookingId int output, @intECourseId int, @source varchar(20)',@intEcourseBookingId out,@intECourseId,@source;

		SELECT @intECourseId_OUT = @intECourseID;
		SELECT @intECourseBookingId_OUT = @intECourseBookingID;
	    DECLARE @alreadyCancelled BIT = 0;
		IF (ISNULL(@intEcourseId,0) != 0)
		BEGIN
		SET @operation = 'U';
		SET @sql = '
			
		UPDATE [' +@source+ '].[dbo].[tblECourse]
			SET 
			intPriceItemId = @intPriceItemId,
			intNumberOfUnits = @intNumberofUnits,
			intNumberOfPartUnits = @intNumberOfPartUnits,
			intChargeUnits = @intChargeUnits,
			intChargePartUnits = @intChargePartUnits,
			intChargeRate = @intChargeRate,
			intNumberOfLessons = @intNumberOfLessons,
			dteFromDate = @dteFromDate,
			dteToDate = @dteToDate,
			txtCode = @ProductCode,
			intEcourseStatusId = @intECourseStatusId,
			dteAmendedDateTime = @dteAmendedDateTime,
			intAmendedUserId = @intAmendedUserId,
			blnDirect = @blnDirect,
			txtNote = CASE WHEN @txtNote = txtNote THEN txtNote
							WHEN @txtNote LIKE ''%[a-z0-9]%'' AND CHARINDEX(@txtNote,txtNote) > 0 THEN txtNote
							WHEN @txtNote LIKE ''%[a-z0-9]%'' AND CHARINDEX(@txtNote,txtNote)  = 0 AND txtNote LIKE ''%[a-z0-9]%'' AND LEN(txtNote + '' | '' + @txtNote) <= 5120 THEN txtNote + '' | '' + @txtNote
							WHEN @txtNote LIKE ''%[a-z0-9]%'' AND CHARINDEX(@txtNote,txtNote)  = 0 AND txtNote LIKE ''%[a-z0-9]%'' AND LEN(txtNote + '' | '' + @txtNote) > 5120 THEN txtNote 
							WHEN @txtNote LIKE ''%[a-z0-9]%'' AND txtNote NOT LIKE ''%[a-z0-9]%''  AND LEN(@txtNote) <= 5120 THEN @txtNote
							ELSE txtNote
							END,
			txtExternalRef = @txtExternalRef,
			intEPackageId = @intEPackageId
			WHERE intECourseId = @intEcourseId
			AND intECourseStatusId != 200;
			
			IF (@@ROWCOUNT != 1)
			BEGIN
				PRINT ERROR_MESSAGE();
				RAISERROR(''No ECourse Updated'',16, 1);
				IF (@intECourseStatusId = 200)
				SELECT @alreadyCancelled = 1;
			END
			
			if (ISNULL(@intCourseId,0) != 0 AND ISNULL(@intEcourseBookingId,0) != 0 )
			BEGIN
				SET NOCOUNT ON; 
				UPDATE [' +@source+ '].[dbo].[tblECourseBooking]
				SET 
				dteFromDate = @dteFromDate,
				dteToDate = @dteToDate,
				txtCode = @courseCode,
				intEcourseBookingStatusId = @intECourseStatusId,
				intNumberOfUnits = @intNumberOfUnits,
				intNumberOfPartUnits = @intNumberOfPartUnits,
				intDurationWeeksRounded = @intDurationWeeksRounded,
				intNumberOfLessons  = @intNumberOfLessons,
				decStatisticalWeeks = @decStatisticalWeeks,
				intCourseId = @intCourseId,
				dteAmendedDateTime = @dteAmendedDateTime,
				intAmendedUserId = @intAmendedUserId
				WHERE  intECourseId = @intECourseId
				AND intECourseBookingId = @intEcourseBookingId
				AND intECourseBookingStatusId != 200;
			
				IF (@@ROWCOUNT != 1)
				BEGIN
					PRINT ERROR_MESSAGE();
					RAISERROR(''No ECourseBooking Updated'',16, 1);
				  
				END
			
			END'
			
			EXECUTE sp_executesql @sql, 	N'@intNumberOfUnits INT OUTPUT, @intNumberOfPartUnits INT  , @intChargeUnits INT , @intChargePartUnits INT  , 
@intChargeRate INT  , @dteCreatedDateTime DATETIME , @intCreatedUserId INT   , @dteCreationDate DATE  , 
@intAmendedUserId INT , @dteAmendedDateTime DATETIME  , @intNumberOfLessons INT  , @prevChargeUnits INT  , 
@prevChargeRate INT  , @prevNumberOfLessons INT  , @prevFromDate Date  , 
@prevToDate Date  , 
@prevDirectFlag BIT  , 
@intCourseId INT ,@intECourseStatusId INT ,@intDurationWeeksRounded  INT, @decStatisticalWeeks DEC ,@source  varchar(20),@intEnrolId VARCHAR(10),@intSchoolId INT,@intPriceItemId INT,@txtECourseStatus varchar(30),@blndirect VARCHAR(1),@ProductCode VARCHAR(30),@dteFromDate DATETIME,@dteToDate DATETIME
	,@txtNote varchar(5120)
	,@txtExternalRefDerivedFrom varchar(30),@txtExternalRef varchar(30),@intEPackageId int, @quantity varchar(10), @intEcourseId INT,@intECourseBookingId INT, @intEcourseId_OUT INT OUTPUT,@intEcourseBookingId_OUT INT OUTPUT,@alreadyCancelled BIT OUTPUT,@courseCode varchar(30) output',@intNumberOfUnits   , 
@intNumberOfPartUnits  ,
@intChargeUnits  ,
@intChargePartUnits  ,
@intChargeRate  ,
@dteCreatedDateTime  ,
@intCreatedUserId   ,
@dteCreationDate ,
@intAmendedUserId   ,
@dteAmendedDateTime   ,
@intNumberOfLessons  ,
@prevChargeUnits  ,
@prevChargeRate  ,
@prevNumberOfLessons  ,
@prevFromDate ,
@prevToDate ,
@prevDirectFlag ,
@intCourseId, @intEcourseStatusId, @intDurationWeeksRounded, @decStatisticalWeeks, @source, @intEnrolId, @intSchoolId, @intPriceItemId,
	@txtECourseStatus, @blnDirect, @ProductCode, @dteFromDate, @dteToDate
	, @txtNote
	, @txtExternalRefDerivedFrom, @txtExternalRef, @intEPackageId, @quantity ,@intEcourseId,@intEcourseBookingId ,@intECourseId_OUT OUT,@intEcourseBookingId_OUT OUT, @alreadyCancelled OUT,@courseCode OUT ;
		IF (@alreadyCancelled = 1)
		 RETURN
		If(@prevChargeUnits != @intChargeUnits)
				INSERT INTO @a (intPriceItemId,
									intGroupId,
									intEnrolId,
									intEnrolBookingId,
									intModuleId,
									intInvoiceLineId,
									intUserId,
									intActionId,
									dteTimestamp,
									txtUsername,
									decOldValue,
									decNewValue,
									decDifference
									)
				values				(
									@intPriceItemId,
									-1,
									CAST(@intEnrolId AS INT),
									@intEcourseId,
									1600,
									-1,
									9999,
									34,
									getdate(),
									'Apttus',
									@prevChargeUnits,
									@intChargeUnits,
									@intChargeUnits - @prevChargeUnits
									)
															
			If(@prevFromDate != @dteFromDate)
					INSERT INTO @a (intPriceItemId,
									intGroupId,
									intEnrolId,
									intEnrolBookingId,
									intModuleId,
									intInvoiceLineId,
									intUserId,
									intActionId,
									dteTimestamp,
									txtUsername,
									dteOldDateValue,
									dteNewDateValue,
									decDifference
									)
					values			(
									@intPriceItemId,
									-1,
									CAST(@intEnrolId AS INT),
									@intEcourseId,
									1600,
									-1,
									9999,
									4,
									getdate(),
									'Apttus',
									@prevFromDate,
									@dteFromDate,
									DATEDIFF(DD,@dteFromDate, @prevFromDate)
									)						
			If(@prevToDate != @dteToDate)
					INSERT INTO @a (intPriceItemId,
									intGroupId,
									intEnrolId,
									intEnrolBookingId,
									intModuleId,
									intInvoiceLineId,
									intUserId,
									intActionId,
									dteTimestamp,
									txtUsername,
									dteOldDateValue,
									dteNewDateValue,
									decDifference
									)
					values			(
									@intPriceItemId,
									-1,
									CAST(@intEnrolId AS INT),
									@intEcourseId,
									1600,
									-1,
									9999,
									5,
									getdate(),
									'Apttus',
									@prevToDate,
									@dteToDate,
									DATEDIFF(DD,@dteToDate, @prevToDate)
									)	
			If(@prevChargeRate != @intChargeRate)
					INSERT INTO @a (intPriceItemId,
									intGroupId,
									intEnrolId,
									intEnrolBookingId,
									intModuleId,
									intInvoiceLineId,
									intUserId,
									intActionId,
									dteTimestamp,
									txtUsername,
									decOldValue,
									decNewValue,
									decDifference
									)
					values			(
									@intPriceItemId,
									-1,
									CAST(@intEnrolId AS INT),
									@intEcourseId,
									1600,
									-1,
									9999,
									36,
									getdate(),
									'Apttus',
									@prevChargeRate,
									@intChargeRate,
									@intChargeRate - @prevChargeRate
									)	
				If(@prevNumberOfLessons != @intNumberOfLessons)
				INSERT INTO @a (intPriceItemId,
									intGroupId,
									intEnrolId,
									intEnrolBookingId,
									intModuleId,
									intInvoiceLineId,
									intUserId,
									intActionId,
									dteTimestamp,
									txtUsername,
									decOldValue,
									decNewValue,
									decDifference
									)
					values			(
									@intPriceItemId,
									-1,
									CAST(@intEnrolId AS INT),
									@intEcourseId,
									1600,
									-1,
									9999,
									6,
									getdate(),
									'Apttus',
									@prevNumberOfLessons,
									@intNumberOfLessons,
									@intNumberOfLessons - @prevNumberOfLessons
									)	
				
				If(@prevDirectFlag != @blnDirect)
					INSERT INTO @a (intPriceItemId,
									intGroupId,
									intEnrolId,
									intEnrolBookingId,
									intModuleId,
									intInvoiceLineId,
									intUserId,
									intActionId,
									dteTimestamp,
									txtUsername,
									decOldValue,
									decNewValue,
									decDifference
									)
					values			(
									@intPriceItemId,
									-1,
									CAST(@intEnrolId AS INT),
									@intEcourseId,
									1600,
									-1,
									9999,
									44,
									getdate(),
									'Apttus',
									@prevDirectFlag,
									@blnDirect,
									0
									)	
			If(@txtECourseStatus = 'Cancelled')
					INSERT INTO @a (intPriceItemId,
									intGroupId,
									intEnrolId,
									intEnrolBookingId,
									intModuleId,
									intInvoiceLineId,
									intUserId,
									intActionId,
									dteTimestamp,
									txtUsername,
									decOldValue,
									decNewValue,
									decDifference
									)
					values			(
									@intPriceItemId,
									-1,
									CAST(@intEnrolId AS INT),
									@intEcourseId,
									1600,
									-1,
									9999,
									31,
									getdate(),
									'Apttus',
									0,
									0,
									0
									)								
				--SELECT @intEcourseBookingId_OUT = CASE WHEN IDENT_CURRENT('[' + @source + '].[dbo].[tblECourseBooking]') IS NOT NULL THEN IDENT_CURRENT('[' + @source + '].[dbo].[tblECourseBooking]') ELSE @intECourseBookingId END;

				--SELECT @intEcourseId_OUT = CASE WHEN IDENT_CURRENT('[' + @source + '].[dbo].[tblECourse]') IS NOT NULL THEN IDENT_CURRENT('[' + @source + '].[dbo].[tblECourse]') ELSE @intECourseId END;

		END
		ELSE IF (@txtECourseStatus != 'Cancelled')
		BEGIN
		SET @operation= 'I'	;
	SET @sql = '		INSERT INTO ['+@source+'].[dbo].[tblECourse]
			(
				intEnrolId,
				intSchoolId,
				intPriceItemId,
				intECourseStatusId,
				dteCreationDate,
				blnDirect,
				txtCode,
				dteFromDate,
				dteToDate,
				intNumberOfUnits,
				intNumberOfPartUnits,
				intChargeUnits,
				intChargePartUnits,
				intChargeRate,
				intNumberOfLessons,
				txtNote,
				txtExternalRef,
				dteCreatedDateTime,
				intCreatedUserId,
				dteAmendedDateTime,
				intAmendedUserId,
				intEPackageId

			)
			VALUES
			(
				CAST(@intEnrolId AS INT),
				@intSchoolId,
				@intPriceItemId,
				@intECourseStatusId,
				@dteCreationDate,
				CAST(@blnDirect AS BIT),
				@ProductCode,
				@dteFromDate,
				@dteToDate,
				@intNumberOfUnits,
				@intNumberOfPartUnits,
				@intChargeUnits,
				@intChargePartUnits,
				@intChargeRate,
				@intNumberOfLessons,
				@txtNote,
				@txtExternalRef,
				@dteCreatedDateTime,
				@intCreatedUserId,
				@dteAmendedDateTime,
				@intAmendedUserId,
				@intEPackageId
			);
		
		IF(@@ROWCOUNT != 1)
			RAISERROR(''No ECourse inserted!'', 16, 1);
	
	    SELECT @intECourseId = SCOPE_IDENTITY();

	   if (ISNULL(@intCourseId,0) != 0 )
	   BEGIN
			INSERT INTO ['+@source+'].[dbo].[tblECourseBooking]
			(
				intEnrolId,
				intECourseId,
				intCourseId,
				intECourseBookingStatusId,
				dteCreatedDateTime,
				intCreatedUserId,
				dteAmendedDateTime,
				intAmendedUserId,
				dteCreationDate,
				txtCode,
				dteFromDate,
				dteToDate,
				intNumberOfUnits,
				intNumberOfPartUnits,
				intDurationWeeksRounded,
				intNumberOfLessons,
				decStatisticalWeeks,
				txtNote

			)
			VALUES
			(
				CAST(@intEnrolId AS INT),
				@intECourseId,
				@intCourseId,
				@intECourseStatusId,
				@dteCreatedDateTime,
				@intCreatedUserId,
				@dteAmendedDateTime,
				@intAmendedUserId,
				@dteCreationDate,
				@courseCode,
				@dteFromDate,
				@dteToDate,
				@intNumberOfUnits,
				@intNumberOfPartUnits,
				@intDurationWeeksRounded,
				@intNumberOfLessons,
				@decStatisticalWeeks,
				'' ''
			);
			IF(@@ROWCOUNT = 0)
			RAISERROR(''No ECourseBooking inserted!'', 16, 1)
			SELECT @intECourseBookingId = SCOPE_IDENTITY();
		END';
		EXECUTE sp_executesql @sql, 	N'@intNumberOfUnits INT OUTPUT, 
@intNumberOfPartUnits INT  , 
@intChargeUnits INT , 
@intChargePartUnits INT  , 
@intChargeRate INT  , 
@dteCreatedDateTime DATETIME , 
@intCreatedUserId INT   , 
@dteCreationDate DATE  , 
@intAmendedUserId INT , 
@intEPackageId INT,
@dteAmendedDateTime DATETIME  , 
@intNumberOfLessons INT  , 
@prevChargeUnits INT  , 
@prevChargeRate INT  , 
@prevNumberOfLessons INT  , 
@prevFromDate Date  , 
@prevToDate Date  , 
@prevDirectFlag BIT  , 
@intCourseId INT ,@intECourseStatusId INT ,@intDurationWeeksRounded  INT, @decStatisticalWeeks DEC ,@source  varchar(20),@intEnrolId VARCHAR(10),@intSchoolId INT,@intPriceItemId INT,@txtECourseStatus varchar(30),@blndirect VARCHAR(1),@ProductCode VARCHAR(30),@dteFromDate DATETIME,@dteToDate DATETIME
	,@txtNote varchar(5120)
	,@txtExternalRef varchar(30), @quantity varchar(10), @courseCode varchar(30) output, @intECourseId INT OUTPUT, @intECourseBookingId INT OUTPUT',@intNumberOfUnits   , 
@intNumberOfPartUnits  ,
@intChargeUnits  ,
@intChargePartUnits  ,
@intChargeRate  ,
@dteCreatedDateTime  ,
@intCreatedUserId   ,
@dteCreationDate ,
@intAmendedUserId   ,
@intEPackageId,
@dteAmendedDateTime   ,
@intNumberOfLessons  ,
@prevChargeUnits  ,
@prevChargeRate  ,
@prevNumberOfLessons  ,
@prevFromDate ,
@prevToDate ,
@prevDirectFlag ,
@intCourseId, @intEcourseStatusId, @intDurationWeeksRounded, @decStatisticalWeeks, @source, @intEnrolId, @intSchoolId, @intPriceItemId,
	@txtECourseStatus, @blnDirect, @ProductCode, @dteFromDate, @dteToDate
	, @txtNote
	, @txtExternalRef, @quantity, @courseCode OUT, @intECourseId = @intECourseId_OUT out, @intEcourseBookingId = @intECourseBookingId_OUT out;
	

	


				--SELECT @intEcourseBookingId_OUT = CASE WHEN IDENT_CURRENT('[' + @source + '].[dbo].[tblECourseBooking]') IS NOT NULL THEN IDENT_CURRENT('[' + @source + '].[dbo].[tblECourseBooking]') ELSE @intECourseBookingId_OUT END;

				--SELECT @intEcourseId_OUT = CASE WHEN IDENT_CURRENT('[' + @source + '].[dbo].[tblECourse]') IS NOT NULL THEN IDENT_CURRENT('[' + @source + '].[dbo].[tblECourse]') ELSE @intECourseId_OUT END;

		If @operation = 'I'
			INSERT INTO @a (intPriceItemId,
					intGroupId,
					intEnrolId,
					intEnrolBookingId,
					intModuleId,
					intInvoiceLineId,
					intUserId,
					intActionId,
					dteTimestamp,
					txtUsername,
					decOldValue,
					decNewValue,
					decDifference
					)
			values (
					@intPriceItemId,
					-1,
					CAST(@intEnrolId AS INT),
					@intEcourseId_OUT,
					1600,
					-1,
					9999,
					22,
					getdate(),
					'Apttus',
					0,
					0,
					0
					)	
	END;
	SEt @sql = 'INSERT INTO [' +  @source + '].[dbo].[tblBookingAudit]	( intPriceItemId ,
	 intGroupId  ,
	 intEnrolId  ,
	 intEnrolBookingId ,
	 intModuleId  ,
	 intInvoiceLineId ,
	 intUserId  ,
	 intActionId,
	 dteTimestamp ,
	 txtUsername  ,
	 dteOldDateValue ,
	 dteNewDatevalue  ,
	 decOldValue  ,
	 decNewValue  ,
	 decDifference ,
	 txtNote  ,
	 txtComment )	
	SELECT  intPriceItemId ,
	 intGroupId  ,
	 intEnrolId  ,
	 intEnrolBookingId ,
	 intModuleId  ,
	 intInvoiceLineId ,
	 intUserId  ,
	 intActionId,
	 dteTimestamp ,
	 txtUsername  ,
	 dteOldDateValue ,
	 dteNewDatevalue  ,
	 decOldValue  ,
	 decNewValue  ,
	 decDifference ,
	 txtNote  ,
	 txtComment  from @a';
	 EXECUTE sp_executesql @sql,N'@a auditLog READONLY',@a
	
RETURN 0
END

