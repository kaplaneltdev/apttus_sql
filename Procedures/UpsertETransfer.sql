USE [ClassIntegration]
GO
/****** Object:  StoredProcedure [APTTUS].[UpsertETransfer]    Script Date: 07/01/2020 15:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [APTTUS].[UpsertETransfer] 
	-- Add the parameters for the stored procedure here

	@vehicle varchar(30),
	@airport_station  varchar(100),

	@source  varchar(20),
	@intEnrolId varchar(10),
	@txtSchoolCode varchar(3),
	@intETransferStatusId varchar(10),
	@intTransferTypeId varchar(10),
	@blndirect BIT,
	@txtCode VARCHAR(25),
	@dtePickUpDate DATE,
	@dtePickUpTime [char](5),
	@txtNote varchar(5120), -- need to check 
	@txtAnalysis1 varchar(30), -- need to check 
	@txtAnalysis2 varchar(30), -- need to check 
	@txtAnalysis3 varchar(30), -- need to check 
	@txtAnalysis4 varchar(30), -- need to check 
	@txtAnalysis5 varchar(30), -- need to check 
	@txtAnalysis6 varchar(30), -- need to check 
	@txtExternalRef varchar(30),
	@txtExternalRefDerivedFrom varchar(30),
	@productName varchar(100),
	@intETransferId_OUT INT OUTPUT,
	@txtPriceItemCode_OUT varchar(30) OUTPUT,
	@intPriceItemId_OUT int output,
	@txtPriceItemName_OUT VARCHAR(100) OUTPUT


AS

BEGIN
		DECLARE @intSchoolId int;
		EXEC APTTUS.GetSchoolId @txtSchoolCode,@source,@intSchoolId OUT;
		DECLARE @intPriceItemId int;

		DECLARE @operation varchar(1);	
		IF @productName not like '%Group Departure Transfer Service%' AND @productName not like '%Group Arrival Transfer Service%' AND @productName not like '%Arrival Transfer Service (Junior)%' AND @productName not like '%Departure Transfer Service (Junior)%' and @productName != 'No Transfer'
			EXEC APTTUS.GetProductCode 'Transfer Service',@source,'airport_station','vehicle',null,null,@airport_station,@vehicle,null, null, @txtCode OUT;
	    ELSE
			EXEC APTTUS.GetProductCode @productName,@source,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, @txtCode OUT;	
		DECLARE @query nvarchar(max);
	
		SET @query = '
		SELECT  @intPriceItemId' + char(61) + ' [intPriceItemId]  , @txtPriceItemName_OUT' + CHAR(61) + ' txtName
		FROM  [' + @source+ '].[dbo].[tblPriceItem]
		WHERE [txtCode] = '+ CHAR(39) + @txtCode + CHAR(39) +
		' AND blnActive = 1 AND intSchoolId = ' +  CAST(@intSchoolId AS VARCHAR(4));
		
		EXEC sp_executesql @query, N'@intPriceItemId INT OUT, @txtPriceItemName_OUT VARCHAR(100) OUT', @intPriceItemId OUT, @txtPriceItemName_OUT OUT;
		SELECT @txtPriceItemCode_OUT = @txtCode,
		@intPriceItemId_OUT = @intPriceItemId;
		

		DECLARE @intEPackageId int = NULL; 
		SET @query = 'SELECT @intEPackageId = intEPackageId
		FROM ['+@source+'].[dbo].[tblEPackage] EP
		INNER JOIN ['+@source+'].[dbo].[tblPackage] P on EP.intPackageId = P.intPackageId
		INNER JOIN  ['+@source+'].[dbo].[tblPackageItem] item on item.intPackageId = P.intPackageId AND item.intPriceItemId = @intPriceItemId and intModuleId = 1900 
		WHERE EP.intEnrolId = @intEnrolId'
		select @query;
		EXEC sp_executesql @query, N'@intEPackageId INT OUT,@intPriceItemId INT,@intEnrolId varchar(10)', @intEPackageId OUT,@intPriceItemId,@intEnrolId;
	
		IF  @intPriceItemId IS NULL
		BEGIN
			THROW 50000, 'No PriceItem mapped for this Transfer Booking!', 1
		END
		DECLARE @sql nvarchar(max);
		SET NOCOUNT ON; 
		
		
		DECLARE @Rowcount INT;

		DECLARE @a  auditLog;
		DECLARE @prevDate Date;
		DECLARE @prevDirectFlag BIT;

		DECLARE @dteCreatedDateTime DATETIME;
		SELECT @dteCreatedDateTime = getdate();

		DECLARE @intCreatedUserId INT = 9999;

		DECLARE @dteCreationDate DATE;
		SELECT @dteCreationDate = CONVERT(DATE, getdate());

		DECLARE @intAmendedUserId INT = 9999;

		DECLARE @dteAmendedDateTime DATETIME;
		SELECT @dteAmendedDateTime = getdate();
		DECLARE @prevBookingStatusId int;
		DECLARE @intETransferId INT;
	    DECLARE @alreadyCancelled BIT = 0;
		SET @sql = 'SELECT @intETransferId = intETransferId,
		@prevDate = dtePickUpDate,
		@prevDirectFlag = blnDirect,
		@prevBookingStatusId = intETransferStatusId
		FROM [' +@source+ '].[dbo].[tblETransfer]
		WHERE txtExternalRef = @txtExternalRefDerivedFrom
		OR txtExternalRef = @txtExternalRef';
		EXEC sp_executesql @sql, N'@prevDate DATE OUTPUT, @prevDirectFlag BIT OUTPUT, @txtExternalRefDerivedFrom varchar(30),@txtExternalRef varchar(30),@intETransferId  int OUTPUT,@prevBookingStatusId int output', @prevDate OUTPUT, @prevDirectFlag OUTPUT, @txtExternalRefDerivedFrom,@txtExternalRef,@intETransferId OUTPUT,@prevBookingStatusId output;
		SELECT @prevBookingStatusId  = CASE WHEN @prevBookingStatusId  IS NULL THEN @intETransferStatusId ELSE @prevBookingStatusId END;
		select @sql;
		select @intETransferId
		IF (@intETransferId IS NOT NULL)
		BEGIN
			SET @operation = 'U';
			SET @sql = 'UPDATE [' +@source+ '].[dbo].[tblETransfer]
			SET intETransferStatusId = CAST(CASE WHEN @intETransferStatusId = ''100'' THEN @intETransferStatusID
			WHEN @intETransferStatusId = ''200'' AND  @intETransferStatusId != @prevBookingStatusId THEN @prevBookingStatusId 
			ELSE @intETransferStatusId END AS  INT),
			intTransferTypeId = CAST(@intTransferTypeId AS INT),
			intPriceItemId = @intPriceItemId,
			txtCode = @txtCode,
			blnDirect = @blnDirect,
			dtePickUpDate = ISNULL(@dtePickUpDate,'''') , ' + CASE WHEN @source != 'CFW_ASPECT_NZL' AND @source != 'CFW_ASPECT_AUS' THEN '
			dtePickUpTime = CASE WHEN @dtePickUpTime = dtePickUpTime THEN dtePickUpTime
							WHEN @dtePickUpTime LIKE ''%[a-z0-9]%'' AND LEN(@dtePickUpTime) <= 5 THEN @dtePickUpTime
							ELSE dtePickUpTime
							END,
			txtNote =       CASE WHEN @txtNote = txtNote THEN txtNote
							WHEN @txtNote LIKE ''%[a-z0-9]%'' AND CHARINDEX(@txtNote,txtNote) > 0 THEN txtNote
							WHEN @txtNote LIKE ''%[a-z0-9]%'' AND CHARINDEX(@txtNote,txtNote)  = 0 AND txtNote LIKE ''%[a-z0-9]%'' AND LEN(txtNote + '' | '' + @txtNote) <= 5120 THEN txtNote + '' | '' + @txtNote
							WHEN @txtNote LIKE ''%[a-z0-9]%'' AND CHARINDEX(@txtNote,txtNote)  = 0 AND txtNote LIKE ''%[a-z0-9]%'' AND LEN(txtNote + '' | '' + @txtNote) > 5120 THEN txtNote 
							WHEN @txtNote LIKE ''%[a-z0-9]%''  AND txtNote NOT LIKE ''%[a-z0-9]%'' AND LEN(@txtNote) <= 5120 THEN @txtNote
							ELSE  txtNote
							END,
			txtAnalysis1 =	CASE WHEN @txtAnalysis1 = txtAnalysis1 THEN txtAnalysis1
							WHEN @txtAnalysis1 LIKE ''%[a-z0-9]%'' AND LEN(@txtAnalysis1) <= 30 THEN @txtAnalysis1
							ELSE txtAnalysis1
							END,
			txtAnalysis2 =	CASE WHEN @txtAnalysis2 = txtAnalysis2 THEN txtAnalysis2
							WHEN @txtAnalysis2 LIKE ''%[a-z0-9]%'' AND LEN(@txtAnalysis2) <= 30 THEN @txtAnalysis2
							ELSE txtAnalysis2
							END,
			txtAnalysis3 =	CASE WHEN @txtAnalysis3 = txtAnalysis3 THEN txtAnalysis3
							WHEN @txtAnalysis3 LIKE ''%[a-z0-9]%'' AND LEN(@txtAnalysis3) <= 30 THEN @txtAnalysis3
							ELSE txtAnalysis3
							END,
			txtAnalysis4 =	CASE WHEN @txtAnalysis4 = txtAnalysis4 THEN txtAnalysis4
							WHEN @txtAnalysis4 LIKE ''%[a-z0-9]%'' AND LEN(@txtAnalysis4) <= 30 THEN @txtAnalysis4
							ELSE txtAnalysis4
							END,
			txtAnalysis5 =	CASE WHEN @txtAnalysis5 = txtAnalysis5 THEN txtAnalysis5
							WHEN @txtAnalysis5 LIKE ''%[a-z0-9]%'' AND LEN(@txtAnalysis5) <= 30 THEN @txtAnalysis5
							ELSE txtAnalysis5
							END,
			txtAnalysis6 =	CASE WHEN @txtAnalysis6 = txtAnalysis6 THEN txtAnalysis6
							WHEN @txtAnalysis6 LIKE ''%[a-z0-9]%'' AND LEN(@txtAnalysis6) <= 30 THEN @txtAnalysis6
							ELSE txtAnalysis6
							END, ' ELSE 'dtePickUpTime = CASE WHEN @dtePickUpTime = dtePickUpTime THEN dtePickUpTime
							WHEN dtePickUpTime LIKE ''%[a-z0-9]%''  THEN  dtePickUpTime
							WHEN @dtePickUpTime LIKE ''%[a-z0-9]%'' AND dtePickUpTime NOT LIKE ''%[a-z0-9]%'' AND LEN(@dtePickUpTime) <= 5 THEN @dtePickUpTime
							ELSE dtePickUpTime
							END,
			txtNote =       CASE WHEN @txtNote = txtNote THEN txtNote
							WHEN txtNote LIKE ''%[a-z0-9]%''  THEN  txtNote
							WHEN @txtNote LIKE ''%[a-z0-9]%'' AND  txtNote NOT LIKE ''%[a-z0-9]%'' AND LEN(@txtNote) <= 5120 THEN @txtNote
							ELSE txtNote
							END,
			txtAnalysis1 =	CASE WHEN @txtAnalysis1 = txtAnalysis1 THEN txtAnalysis1
							WHEN txtAnalysis1 LIKE ''%[a-z0-9]%''  THEN  txtAnalysis1
							WHEN @txtAnalysis1 LIKE ''%[a-z0-9]%'' AND  txtAnalysis1 NOT LIKE ''%[a-z0-9]%'' AND LEN(@txtAnalysis1) <= 30 THEN @txtAnalysis1
							ELSE txtAnalysis1
							END,
			txtAnalysis2 =	CASE WHEN @txtAnalysis2 = txtAnalysis2 THEN txtAnalysis2
							WHEN txtAnalysis2 LIKE ''%[a-z0-9]%''  THEN  txtAnalysis2
							WHEN @txtAnalysis2 LIKE ''%[a-z0-9]%'' AND txtAnalysis2 NOT LIKE ''%[a-z0-9]%'' AND LEN(@txtAnalysis2) <= 30 THEN @txtAnalysis2
							ELSE txtAnalysis2
							END,
			txtAnalysis3 =	CASE WHEN @txtAnalysis3 = txtAnalysis3 THEN txtAnalysis3
							WHEN txtAnalysis3 LIKE ''%[a-z0-9]%''  THEN txtAnalysis3
							WHEN @txtAnalysis3 LIKE ''%[a-z0-9]%'' AND  txtAnalysis3 NOT LIKE ''%[a-z0-9]%'' AND LEN(@txtAnalysis3) <= 30 THEN @txtAnalysis3
							ELSE txtAnalysis3
							END,
			txtAnalysis4 =	CASE WHEN @txtAnalysis4 = txtAnalysis4 THEN txtAnalysis4
							WHEN txtAnalysis4 LIKE ''%[a-z0-9]%''  THEN txtAnalysis4
							WHEN @txtAnalysis4 LIKE ''%[a-z0-9]%'' AND  txtAnalysis4 NOT LIKE ''%[a-z0-9]%''  AND LEN(@txtAnalysis4) <= 30 THEN @txtAnalysis4
							ELSE txtAnalysis4
							END,
			txtAnalysis5 =	CASE WHEN @txtAnalysis5 = txtAnalysis5 THEN txtAnalysis5
							WHEN txtAnalysis5 LIKE ''%[a-z0-9]%'' THEN  txtAnalysis5
							WHEN @txtAnalysis5 LIKE ''%[a-z0-9]%'' AND  txtAnalysis5 NOT LIKE ''%[a-z0-9]%'' AND LEN(@txtAnalysis5) <= 30 THEN @txtAnalysis5
							ELSE txtAnalysis5
							END,
			txtAnalysis6 =	CASE WHEN @txtAnalysis6 = txtAnalysis6 THEN txtAnalysis6
							WHEN txtAnalysis6 LIKE ''%[a-z0-9]%'' THEN  txtAnalysis6
							WHEN @txtAnalysis6 LIKE ''%[a-z0-9]%'' AND  txtAnalysis6 NOT LIKE ''%[a-z0-9]%'' AND LEN(@txtAnalysis6) <= 30 THEN @txtAnalysis6
							ELSE txtAnalysis6
							END,' END  +'
			dteAmendedDateTime = @dteAmendedDateTime,
			txtExternalRef = @txtExternalRef,
			intEPackageId = @intEPackageId,
			intAmendedUserId = @intAmendedUserId
			WHERE (txtExternalRef = @txtExternalRefDerivedFrom
			OR txtExternalRef = @txtExternalRef)
			AND intETransferStatusId != 400;
			
			IF (@@Rowcount != 1)
			BEGIN
				RAISERROR(''No ETransfer Updated'',16, 1);
				IF(@intETransferStatusId = 400)
					SELECT @alreadyCancelled = 1;
            END'

			EXECUTE sp_executesql @sql, 	N'@dteAmendedDateTime DATETIME,@intAmendedUserId int, @source  varchar(20),
			@intEnrolId varchar(10),
			@intETransferStatusId varchar(10),
			@intTransferTypeId varchar(10),
			@blndirect BIT,
			@txtCode VARCHAR(25),
			@dtePickUpDate DATE,
			@dtePickUpTime [char](5),
			@txtNote varchar(5120),
			@txtAnalysis1 varchar(30),
			@txtAnalysis2 varchar(30),
			@txtAnalysis3 varchar(30),
			@txtAnalysis4 varchar(30),
			@txtAnalysis5 varchar(30),
			@txtAnalysis6 varchar(30),
			@txtExternalRef varchar(30),@intEPackageId int,@txtExternalRefDerivedFrom varchar(30), @intPriceItemId int, @alreadyCancelled BIT OUT,@prevBookingStatusId INT OUTPUT',@dteAmendedDateTime, @intAmendedUserId, @source, @intEnrolId, 
			@intETransferStatusId, @intTransferTypeId, @blnDirect, @txtCode, @dtePickUpDate,@dtePickUpTime, @txtNote, @txtAnalysis1,@txtAnalysis2,@txtAnalysis3,@txtAnalysis4,@txtAnalysis5,@txtAnalysis6,@txtExternalRef,@intEPackageId, @txtExternalRefDerivedFrom,@intPriceItemId, @alreadyCancelled OUT,@prevBookingStatusId OUTPUT;
			SELECT @intETransferId_OUT = @intETransferId
		    IF (@alreadyCancelled = 1)
				RETURN
		 
			If(@prevDate != @dtePickUpDate)
					INSERT INTO @a (intPriceItemId,
									intGroupId,
									intEnrolId,
									intEnrolBookingId,
									intModuleId,
									intInvoiceLineId,
									intUserId,
									intActionId,
									dteTimestamp,
									txtUsername,
									dteOldDateValue,
									dteNewDateValue,
									decDifference
									)
					values			(
									@intPriceItemId,
									-1,
									CAST(@intEnrolId AS INT),
									@intETransferId_OUT,
									1900,
									-1,
									9999,
									9,
									getdate(),
									'Apttus',
									@prevDate,
									@dtePickUpDate,
									DATEDIFF(dd,@dtePickUpDate, @prevDate)
									)	
				
				If(@prevDirectFlag != @blnDirect)
					INSERT INTO @a (intPriceItemId,
									intGroupId,
									intEnrolId,
									intEnrolBookingId,
									intModuleId,
									intInvoiceLineId,
									intUserId,
									intActionId,
									dteTimestamp,
									txtUsername,
									decOldValue,
									decNewValue,
									decDifference
									)
					values			(
									@intPriceItemId,
									-1,
									CAST(@intEnrolId AS INT),
									@intETransferId_OUT,
									1900,
									-1,
									9999,
									47,
									getdate(),
									'Apttus',
									@prevDirectFlag,
									@blnDirect,
									0
									)	
				If(@intETransferStatusId = 400)
					INSERT INTO @a (intPriceItemId,
									intGroupId,
									intEnrolId,
									intEnrolBookingId,
									intModuleId,
									intInvoiceLineId,
									intUserId,
									intActionId,
									dteTimestamp,
									txtUsername,
									decOldValue,
									decNewValue,
									decDifference
									)
					values			(
									@intPriceItemId,
									-1,
									CAST(@intEnrolId AS INT),
									@intETransferId_OUT,
									1900,
									-1,
									9999,
									31,
									getdate(),
									'Apttus',
									0,
									0,
									0
									)															
		END
		ELSE IF (@intETransferStatusId != '400')
		BEGIN
			SET @operation = 'I';
			SET @sql = 'INSERT INTO [' +@source+ '].[dbo].[tblETransfer]
			(
				intEnrolId,
				
				intPriceItemId,
				intETransferStatusId,
				intTransferTypeId,
				dteCreationDate,
				blnDirect,
				txtCode,
				dtePickUpDate,
				dtePickUpTime,
				txtExternalRef,
				dteCreatedDateTime,
				intCreatedUserId,
				dteAmendedDateTime,
				intAmendedUserId,
				txtNote,
				txtAnalysis1,
				txtAnalysis2,
				txtAnalysis3,
				txtAnalysis4,
				txtAnalysis5,
				txtAnalysis6,
				intModuleId,
				intEPackageId
			)
			VALUES
			(
				CAST(@intEnrolId AS INT),
				@intPriceItemId,
				CAST(@intETransferStatusId AS INT),
				CAST(@intTransferTypeId AS INT),
				@dteCreationDate,
				@blnDirect,
				@txtCode,
				ISNULL(@dtePickUpDate,''''),
				ISNULL(@dtePickUpTime,''''),
				@txtExternalRef,				
				@dteCreatedDateTime,
				@intCreatedUserId,
				@dteAmendedDateTime,
				@intAmendedUserId,
				ISNULL(@txtNote,''''),
				ISNULL(@txtAnalysis1,''''),
				ISNULL(@txtAnalysis2,''''),
				ISNULL(@txtAnalysis3,''''),
				ISNULL(@txtAnalysis4,''''),
				ISNULL(@txtAnalysis5,''''),
				ISNULL(@txtAnalysis6,''''),
				1900,
				@intEPackageId
			)
		IF (@@ROWCOUNT != 1)
			RAISERROR(''No ETransfer inserted'',16, 1)
				SELECT @intETransferId = SCOPE_IDENTITY()';
			EXECUTE sp_executesql @sql, 	N'@intAmendedUserId int,@dteAmendedDateTime datetime,@intCreatedUserId int,@dteCreatedDateTime datetime,@dteCreationDate date, @source  varchar(20),
	@intEnrolId varchar(10),
	@intETransferStatusId varchar(10),
	@intTransferTypeId varchar(10),
	@blndirect BIT,
	@txtCode VARCHAR(25),
	@dtePickUpDate DATE,
	@dtePickUpTime [char](5),
	@txtNote varchar(5120),
	@txtAnalysis1 varchar(30),
	@txtAnalysis2 varchar(30),
	@txtAnalysis3 varchar(30),
	@txtAnalysis4 varchar(30),
	@txtAnalysis5 varchar(30),
	@txtAnalysis6 varchar(30),
	@intEPackageId int,
	@txtExternalRef varchar(30), @intPriceItemId int,@intETransferId int output',@intAmendedUserId, @dteAmendedDateTime,@intCreatedUserId,@dteCreatedDateTime,@dteCreationDate, @source, @intEnrolId, 
	@intETransferStatusId, @intTransferTypeId, @blnDirect, @txtCode, @dtePickUpDate,@dtePickUpTime, @txtNote, @txtAnalysis1,@txtAnalysis2,@txtAnalysis3,@txtAnalysis4,@txtAnalysis5,@txtAnalysis6,@intEPackageId,@txtExternalRef,@intPriceItemId,@intETransferId = @intETransferId_OUT OUTPUT;

	If @operation = 'I'
			INSERT INTO @a (intPriceItemId,
					intGroupId,
					intEnrolId,
					intEnrolBookingId,
					intModuleId,
					intInvoiceLineId,
					intUserId,
					intActionId,
					dteTimestamp,
					txtUsername,
					decOldValue,
					decNewValue,
					decDifference
					)
			values (
					@intPriceItemId,
					-1,
					CAST(@intEnrolId AS INT),
					@intETransferId_OUT,
					1900,
					-1,
					9999,
					22,
					getdate(),
					'Apttus',
					0,
					0,
					0
					)	
	SEt @sql = 'INSERT INTO [' +  @source + '].[dbo].[tblBookingAudit]	( intPriceItemId ,
	 intGroupId  ,
	 intEnrolId  ,
	 intEnrolBookingId ,
	 intModuleId  ,
	 intInvoiceLineId ,
	 intUserId  ,
	 intActionId,
	 dteTimestamp ,
	 txtUsername  ,
	 dteOldDateValue ,
	 dteNewDatevalue  ,
	 decOldValue  ,
	 decNewValue  ,
	 decDifference ,
	 txtNote  ,
	 txtComment )	
	SELECT  intPriceItemId ,
	 intGroupId  ,
	 intEnrolId  ,
	 intEnrolBookingId ,
	 intModuleId  ,
	 intInvoiceLineId ,
	 intUserId  ,
	 intActionId,
	 dteTimestamp ,
	 txtUsername  ,
	 dteOldDateValue ,
	 dteNewDatevalue  ,
	 decOldValue  ,
	 decNewValue  ,
	 decDifference ,
	 txtNote  ,
	 txtComment  from @a';
	 EXECUTE sp_executesql @sql,N'@a auditLog READONLY',@a
	END;
	


RETURN 0
END




