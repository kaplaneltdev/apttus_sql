USE [ClassIntegration]
GO
/****** Object:  StoredProcedure [APTTUS].[GetBookedItem]    Script Date: 18/11/2020 12:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [APTTUS].[GetBookedItem]
@ModuleId VARCHAR(10),
@ClassDB VARCHAR(14),
@LineItemId VARCHAR(30),
@LineItemIdDerivedFrom VARCHAR(30),
@EnrolId int output, 
@dteFromDate date  = NULL OUTPUT,
@dteToDate date = NULL  output,
@intPriceItemId int output,
@intNumberOfUnits int output,
@intNumberOfPartUnits int output,
@RecordId INT OUTPUT, 
@DebitInvoiceLineId INT = 0 OUTPUT,
@status varchar(30) output,
@price money output,
@commission money output,
@name varchar(30) output,
@code varchar(30) output,
@intPackageId int output,
@paidBy BIT output
AS
BEGIN
	DECLARE @sql nvarchar(max);

	
	SET @sql = '
	SELECT   @EnrolId = intEnrolId, @RecordId' + char(61) +  CASE WHEN  @ModuleId = '1600' THEN ' intEcourseId'
										  WHEN  @ModuleId = '1800' THEN ' intEhostId'
										  WHEN  @ModuleId = '1900' THEN ' intETransferId'
										  WHEN  @ModuleId = '2000' THEN ' intESundryId'
										  WHEN @ModuleId = '1550' THEN 'p.intEPackageId'
									 END +	  
									 --CASE WHEN  @ModuleId IN ('1600','1800','2000') THEN ',@dteFromDate = dteFromDate,
										--													@dteToDate = dteToDate '
										--  WHEN  @ModuleId = '1900' THEN ' ,@dteFromDate = dtePickupDate,
										--  @dteToDate = dtePickupDate'
									 --END
									 --+
',@intPriceItemId = '+ CASE WHEN @ModuleId = '1550' THEN '-1' ELSE 'intPriceItemId ' END    + ',
@intNumberOfUnits = intNumberOfUnits,
@intNumberOfPartUnits = intNumberOfPartUnits '									 
									 + ',@status ='+
										+ CASE WHEN @ModuleId = '1600' THEN  'CASE WHEN   intECourseStatusId = 100  THEN '   + '''Active''' +
										  ' WHEN  intECourseStatusId = 200  THEN '  + '''Cancelled'' END'  
										  WHEN @ModuleId = '1800' THEN 'CASE WHEN   intEHostStatusId = 100  THEN ' +  '''Active''' +
										' WHEN   intEHostStatusId = 200  THEN ' +  '''Cancelled'' END' 
										  WHEN @ModuleId = '1900' THEN 'CASE WHEN intETransferStatusId != 400  THEN'  + '''Active'''+
										' WHEN   intETransferStatusId = 400 THEN' +  '''Cancelled'' END' 
										WHEN @ModuleId = '2000' THEN 'CASE WHEN   intESundryStatusId = 100  THEN' + '''Active''' + 
										' WHEN   intESundryStatusId = 200  THEN' +  '''Cancelled'' END' 
										WHEN @ModuleId = '1550' THEN 'CASE WHEN   intEPackageStatusId = 100  THEN' + '''Active''' + 
										' WHEN   intEPackageStatusId = 200  THEN' +  '''Cancelled'' END' 
									 END + CASE WHEN @ModuleId = 1550 THEN ',@intPackageId = p.intPackageId ' ELSE ' ' END +
									 +
    ' FROM  [' + @ClassDB+ '].[dbo].' + CASE WHEN  @ModuleId = '1600' THEN 'tblEcourse'
										  WHEN @ModuleId  = '1800' THEN 'tblEHost'
										  WHEN @ModuleId  = '1900' THEN 'tblETransfer'
										  WHEN @ModuleId  = '2000' THEN 'tblESundry' 
										  WHEN @ModuleId  = '1550' THEN 'tblEPackage p LEFT OUTER JOIN APTTUS.Epackage_Mapping m on m.intEPackageId = p.intEPackageId'
									 END +
    ' WHERE  
	 [txtExternalRef] = ''' +  @LineItemId + '''
	 OR [txtExternalRef] = ''' +  @LineItemIdDerivedFrom + '''';
	--select @sql;
	EXEC sp_executesql @sql, N'@EnrolId INT OUT, @RecordId INT OUT, @intPriceItemId int out, @intNumberOfUnits int out, @intNumberOfPartUnits int out, @status varchar(30) out,@intPackageId int out',  @EnrolId OUT,  @RecordId OUT,@intPriceItemId out,@intNumberOfUnits out,@intNumberOfPartUnits out, @status out,@intPackageId out;
		
	SET @sql = '
	SELECT  @DebitInvoiceLineId '  + char(61) + ' max(intInvoiceLineId) 

    FROM  [' + @ClassDB+ '].[dbo].[tblInvoiceLine]
    WHERE  
	 [intModuleId] = ' +  CAST(@ModuleId AS  VARCHAR(10))+
	' AND [intEnrolId] = ' +  CAST(@EnrolId AS  VARCHAR(10)) +
	' AND txtInvoiceType = ''D'' AND [intRecordId] = ' +  CAST(@RecordId AS  VARCHAR(10)) 
	;
	--SELECT @sql;
	EXEC sp_executesql @sql, N'@DebitInvoiceLineId INT OUT', @DebitInvoiceLineId OUT;
	
	SET @sql = '
	SELECT  @dteFromDate = dteFromDate,
	@dteToDate = dteToDate,
	@price = curBasic,
	@commission = curDiscount,
	@name = txtNarrative,
	@code = txtCode,
	@paidBy = blndirect
	FROM  [' + @ClassDB+ '].[dbo].[tblInvoiceLine]
    WHERE  
	[intInvoiceLineId] = ' +  CAST(@DebitInvoiceLineId AS  VARCHAR(10))+
	' AND [intModuleId] = ' +  CAST(@ModuleId AS  VARCHAR(10))+
	' AND [intEnrolId] = ' +  CAST(@EnrolId AS  VARCHAR(10)) +
	' AND txtInvoiceType = ''D'' AND [intRecordId] = ' +  CAST(@RecordId AS  VARCHAR(10)) 
	;
	--SELECT @sql;
	EXEC sp_executesql @sql, N'@DebitInvoiceLineId INT,@dteFromDate DATE OUT,@dteToDate DATE OUT,@price MONEY OUT, @commission money out,@name varchar(30) out,@code varchar(30) out,@paidBy BIT output', @DebitInvoiceLineId,@dteFromDate OUT,@dteToDate OUT,@price OUT,@commission out,@name OUT,@code out,@paidBy out;
	
    RETURN 0;
	END




