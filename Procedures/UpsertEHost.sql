USE [ClassIntegration]
GO
/****** Object:  StoredProcedure [APTTUS].[UpsertEHost]    Script Date: 18/11/2020 12:21:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [APTTUS].[UpsertEHost] 
	-- Add the parameters for the stored procedure here

	@homestayRoomType varchar(100),
	@locationZone varchar(100),
	@mealOptions varchar(100),
	@private_Bath varchar(100),
	@residenceName varchar(100),
	@residenceRoomType varchar(100),
	@productName varchar(100), 
	@source  varchar(20),
	@intEnrolId varchar(10),
	@txtSchoolCode varchar(3),
	
	@intEHostStatusId varchar(10),
	@intEHostBookingStatusId varchar(10),
	@blndirect varchar(1),
	@txtCode VARCHAR(25),
	@dteFromDate DATETIME,
	@dteToDate DATETIME,
	@quantity varchar(10),
	@txtNote varchar(5120),
	@txtSpecialRequestNote varchar(5120),
	@txtStudentAddress varchar(5120),
	@txtExternalRef varchar(30),
	@txtExternalRefDerivedFrom varchar(30),
	@intEHostId_OUT INT OUTPUT,
	@intEHostBookingId_OUT INT OUTPUT,
	@txtPriceItemCode_OUT varchar(30) OUTPUT,
	@intPriceItemId_OUT int output,
	@txtPriceItemName_OUT VARCHAR(100) OUTPUT


AS

BEGIN
		--SELECT @mealOptions = REPLACE(@mealOptions, ' ', '');

		DECLARE @intSchoolId int;
		DECLARE @prevChargeUnits INT;
		DECLARE @prevChargeRate INT;

		DECLARE @prevFromDate Date;
		DECLARE @prevToDate Date;
		DECLARE @prevDirectFlag BIT;
		DECLARE @prevNote VARCHAR(5120);
		DECLARE @prevSpecialRequestNote VARCHAR(5120);
		DECLARE @prevPriceItem INT;
		DECLARE @matchupRelatedChange BIT;
		EXEC APTTUS.GetSchoolId @txtSchoolCode,@source,@intSchoolId OUT;
		IF @locationZone = 'Close To School' or @locationZone IS NULL
			SELECT @locationZone = 'NA';
		IF @productName in ( 'Residence','Extra Night Residence','Residence ALP')
			EXEC APTTUS.GetProductCode @ProductName,@source,'Residence Room Type','Residence Name','Private Bath','Meal Options',@residenceRoomType,@residenceName,@private_Bath,@mealOptions, @txtCode OUT;
		ELSE IF @productName in ( 'Homestay','Extra Night Homestay','Fixed Length Accommodation (Homestay)','Homestay ALP')
			EXEC APTTUS.GetProductCode @ProductName,@source,'Homestay Room Type','Location(Zone)','Private Bath','Meal Options',@homestayRoomType,@locationZone,@private_Bath,@mealOptions, @txtCode OUT;
		ELSE 
			EXEC APTTUS.GetProductCode @ProductName,@source,null,null,null,null,null,null,null,null, @txtCode OUT;
		select @txtcode;
		DECLARE @intPriceItemId INT;
		DECLARE @intHostPriceItemId INT;
		DECLARE @query nvarchar(max);
	
		SET @query = '
		SELECT  @intPriceItemId' + char(61) + ' [intPriceItemId], @txtPriceItemName_OUT'  + CHAR(61) + ' txtName, @intHostPriceItemId ' + CHAR(61) + 'intHostPriceItemId
		FROM  [' + @source+ '].[dbo].[tblPriceItem]
		WHERE [txtCode] = '+ CHAR(39) + @txtCode + CHAR(39) +
		' AND blnActive = 1 AND intSchoolId = ' +  CAST(@intSchoolId AS VARCHAR(4));
		
		EXEC sp_executesql @query, N'@intPriceItemId INT OUT,@txtPriceItemName_OUT VARCHAR(100) OUT, @intHostPriceItemId INT OUT', @intPriceItemId OUT, @txtPriceItemName_OUT OUT, @intHostPriceItemId OUT;
		SELECT @txtPriceItemCode_OUT = @txtCode,
		@intPriceItemId_OUT = @intPriceItemId;


		DECLARE @intEPackageId int = NULL; 
		SET @query = 'SELECT @intEPackageId = intEPackageId
		FROM ['+@source+'].[dbo].[tblEPackage] EP
		INNER JOIN ['+@source+'].[dbo].[tblPackage] P on EP.intPackageId = P.intPackageId
		INNER JOIN  ['+@source+'].[dbo].[tblPackageItem] item on item.intPackageId = P.intPackageId AND item.intPriceItemId = @intPriceItemId and intModuleId = 1800 
		WHERE EP.intEnrolId = @intEnrolId'
		select @query;
		EXEC sp_executesql @query, N'@intEPackageId INT OUT,@intPriceItemId INT,@intEnrolId varchar(10)', @intEPackageId OUT,@intPriceItemId,@intEnrolId;

		IF  @intPriceItemId IS NULL
		BEGIN
			THROW 50000, 'No PriceItem mapped for this Host Booking!', 1
		END
		
		DECLARE @intEHostId INT;	

		DECLARE @intEHostBookingId INT;	
		DECLARE @sql nvarchar(max);
		SET NOCOUNT ON; 
		DECLARE @operation varchar(1);
		
		DECLARE @Rowcount INT;

		DECLARE @dteCreatedDateTime DATETIME;
		SELECT @dteCreatedDateTime = getdate();

		DECLARE @intCreatedUserId INT = 9999;

		DECLARE @dteCreationDate DATE;
		SELECT @dteCreationDate = CONVERT(DATE, getdate());

		DECLARE @intAmendedUserId INT = 9999;

		DECLARE @dteAmendedDateTime DATETIME = getdate();
		SELECT @dteCreatedDateTime = getdate();

		DECLARE @intNumberOfUnits INT ; 
		SELECT @intNumberOfUnits = CAST(CAST(CASE WHEN @productName in ('Extra Night Homestay','Extra Night Residence') THEN '0.0' ELSE @quantity END AS DECIMAL) AS INT);
		--SELECT @intNumberOfUnits = CEILING(CONVERT(FLOAT,DATEDIFF(dd,@dteFromDate,@dteToDate))/7 );

		DECLARE @intNumberOfPartUnits INT;
		SELECT @intNumberOfPartUnits = CAST(CAST(CASE WHEN @productName in ('Extra Night Homestay','Extra Night Residence') THEN @quantity ELSE '0.0' END AS DECIMAL) AS INT);
		--SELECT @intNumberOfPartUnits = DATEDIFF(dd,@dteFromDate,@dteToDate)%7;

		DECLARE @intChargeUnits INT;
		SELECT @intChargeUnits = CAST(CAST(CASE WHEN @productName in ('Extra Night Homestay','Extra Night Residence') THEN '0.0' ELSE @quantity END AS DECIMAL) AS INT);
		--SELECT @intChargeUnits = CEILING(CONVERT(FLOAT,DATEDIFF(dd,@dteFromDate,@dteToDate))/7 );

		DECLARE @intChargePartUnits INT ;
		SELECT @intChargePartUnits = CAST(CAST(CASE WHEN @productName in ('Extra Night Homestay','Extra Night Residence') THEN @quantity ELSE '0.0' END AS DECIMAL) AS INT);
		--SELECT @intChargePartUnits =  DATEDIFF(dd,@dteFromDate,@dteToDate)%7;

		DECLARE @intChargeRate INT;
		SELECT @intChargeRate = CAST(CAST(CASE WHEN @productName in ('Extra Night Homestay','Extra Night Residence') THEN '0.0' ELSE @quantity END AS DECIMAL) AS INT);
		--SELECT @intChargeRate = CEILING(CONVERT(FLOAT,DATEDIFF(dd,@dteFromDate,@dteToDate))/7 );
		DECLARE @a  auditLog;
		
		SET @sql = 'SELECT @intEHostId = intEHostId,
		@prevChargeUnits = intChargeUnits,
		@prevChargeRate = intChargeRate,
		@prevFromDate = dteFromDate,
		@prevToDate = dteToDate, 
		@prevDirectFlag = blnDirect,
		@prevPriceItem = intPriceItemId,
		@prevNote = txtNote,
		@prevSpecialRequestNote  = txtSpecialRequestNote
		FROM [' +@source+ '].[dbo].[tblEHost]
		WHERE txtExternalRef = @txtExternalrefDerivedFrom
		OR txtExternalRef = @txtExternalref';
		EXEC sp_executesql @sql,N'@intEHostId int output,@prevChargeUnits int output,@prevChargeRate int output, @prevFromDate date output, @prevToDate date output, @prevDirectFlag BIT output,@txtExternalrefDerivedFrom varchar(30),@txtExternalref varchar(30), @prevPriceItem int output,@prevNote varchar(5120) output,@prevSpecialRequestNote varchar(5120) output',@intEHostId output, @prevChargeUnits out,@prevChargeRate out,  @prevFromDate out, @prevToDate out, @prevDirectFlag out,@txtExternalrefDerivedFrom,@txtExternalref, @prevPriceItem output,@prevNote  output,@prevSpecialRequestNote output; 
		
		SELECT @matchupRelatedChange = CASE WHEN @prevFromDate != @dteFromDate OR @prevToDate != @dteToDate OR @prevPriceItem != @intPriceItemId  OR  @intEHostBookingStatusId = 400 THEN 1
										ELSE 0 END;
		select @matchupRelatedChange as 'matchupchange';
		DECLARE  @intHostId INT;
		SET @sql = 'SELECT @intHostId = intHostId
		FROM [' +@source+ '].[dbo].[tblHost]
		WHERE txtCode = @txtCode';
		EXECUTE sp_executesql @sql,N'@intHostId int OUTPUT, @txtCode varchar(25)',@intHostId,@txtCode;


		DECLARE @alreadyCancelled BIT = 0;
		DECLARE @prevEHostBookingStatusId int;
		
		
		SET @sql = 'SELECT @intEHostBookingId = intEHostBookingId,
		@prevEHostBookingStatusId = intEHostBookingStatusId
		FROM [' +@source+ '].[dbo].[tblEHostBooking]
		WHERE intEHostId = @intEHostId AND intEhostBookingStatusId < 400';
		EXECUTE sp_executesql @sql,N'@intEHostBookingId int output,@intEHostId int,@prevEHostBookingStatusId int output',@intEHostBookingId output,@intEHostId, @prevEHostBookingStatusId out
		SELECT @prevEHostBookingStatusId = CASE WHEN @prevEHostBookingStatusId IS NULL THEN @intEHostBookingStatusId ELSE @prevEHostBookingStatusId END;
		SELECT @intEHostId_OUT = @intEHostId;
		SELECT @intEHostBookingId_OUT = @intEHostBookingID;
		DECLARE @numberOfMatchUpRecords as int;
        SET @sql = 'SELECT @numberOfMatchUpRecords = COUNT(intEHostBookingId) 
        FROM [' +@source+ '].[dbo].[tblEHostBooking]
        WHERE intEHostId = @intEHostId AND intEHostBookingStatusId < 400';
        EXECUTE sp_executesql @sql,N'@numberOfMatchUpRecords int output,@intEHostId int',@numberOfMatchUpRecords  output,@intEHostId;
            
            
		IF (ISNULL(@intEHostId,0) != 0)
		BEGIN
		SET @operation = 'U';
			SET @sql = '
			select ''inside update'';
			UPDATE [' +@source+ '].[dbo].[tblEHost]
			SET 
			dteFromDate = @dteFromDate,
			dteToDate = @dteToDate,
			intPriceItemId = @intPriceItemId,
			intNumberOfUnits  = @intNumberOfUnits,
			blnDirect = CAST(@blnDirect AS BIT),
			intNumberOfPartUnits = @intNumberOfPartUnits,
			intChargeUnits = @intChargeUnits ,
			intChargePartUnits  = @intChargePartUnits,
			intChargeRate = @intChargeRate, 
						txtNote = CASE WHEN @txtNote = txtNote THEN txtNote
							WHEN @txtNote LIKE ''%[a-z0-9]%'' AND CHARINDEX(@txtNote,txtNote) > 0 THEN txtNote
							WHEN @txtNote LIKE ''%[a-z0-9]%'' AND CHARINDEX(@txtNote,txtNote)  = 0 AND txtNote LIKE ''%[a-z0-9]%'' AND LEN(txtNote + '' | '' + @txtNote) <= 5120 THEN txtNote + '' | '' + @txtNote
							WHEN @txtNote LIKE ''%[a-z0-9]%'' AND CHARINDEX(@txtNote,txtNote)  = 0 AND txtNote LIKE ''%[a-z0-9]%'' AND LEN(txtNote + '' | '' + @txtNote) > 5120 THEN txtNote 
							WHEN @txtNote LIKE ''%[a-z0-9]%'' AND txtNote NOT LIKE ''%[a-z0-9]%'' AND LEN(@txtNote) <= 5120 THEN @txtNote
							ELSE  txtNote
							END,
			txtSpecialRequestNote = CASE WHEN @txtSpecialRequestNote = txtSpecialRequestNote THEN txtSpecialRequestNote
							WHEN @txtSpecialRequestNote LIKE ''%[a-z0-9]%'' AND CHARINDEX(@txtSpecialRequestNote,txtSpecialRequestNote) > 0 THEN txtSpecialRequestNote
							WHEN @txtSpecialRequestNote LIKE ''%[a-z0-9]%'' AND CHARINDEX(@txtSpecialRequestNote,txtSpecialRequestNote)  = 0 AND txtSpecialRequestNote LIKE ''%[a-z0-9]%'' AND LEN(txtSpecialRequestNote + '' | '' + @txtSpecialRequestNote) <= 5120 THEN txtSpecialRequestNote + '' | '' + @txtSpecialRequestNote
							WHEN @txtSpecialRequestNote LIKE ''%[a-z0-9]%'' AND CHARINDEX(@txtSpecialRequestNote,txtSpecialRequestNote)  = 0 AND txtSpecialRequestNote LIKE ''%[a-z0-9]%'' AND LEN(txtSpecialRequestNote + '' | '' + @txtSpecialRequestNote) > 5120 THEN txtSpecialRequestNote 
							WHEN @txtSpecialRequestNote LIKE ''%[a-z0-9]%'' AND  txtSpecialRequestNote NOT LIKE ''%[a-z0-9]%'' AND LEN(@txtSpecialRequestNote) <= 5120 THEN @txtSpecialRequestNote
							ELSE  txtSpecialRequestNote
							END,
			txtCode = @txtCode,
			intEHostStatusId = CAST(@intEHostStatusId AS INT),
			dteAmendedDateTime = @dteAmendedDateTime,
			intAmendedUserId = @intAmendedUserId,
			txtExternalRef = @txtExternalRef,
		    txtStudentAddress = @txtStudentAddress,
			intEPackageId = @intEPackageId
			WHERE (txtExternalRef = @txtExternalRefDerivedFrom
			OR txtExternalRef = @txtExternalRef)
			AND intEHostStatusId != 200;
			
			IF (@@ROWCOUNT = 0)
			BEGIN
				PRINT ERROR_MESSAGE();
				RAISERROR(''No EHost Updated'',16, 1);
		        IF (@intEHostStatusId = 200)
				SELECT @alreadyCancelled = 1;
			END
			IF(ISNULL(@intHostPriceItemId,0) != 0 and @numberOfMatchUpRecords = 1)
			BEGIN
				UPDATE [' +@source+ '].[dbo].[tblEHostBooking]
				SET 
				dteFromDate = @dteFromDate,
				dteToDate = @dteToDate,
				dtePaidUpToDate = @dteFromDate,
				--txtNote = CASE WHEN @txtNote = txtNote THEN txtNote
				--			WHEN @txtNote LIKE ''%[a-z0-9]%'' AND CHARINDEX(@txtNote,txtNote) > 0 THEN txtNote
				--			WHEN @txtNote LIKE ''%[a-z0-9]%'' AND CHARINDEX(@txtNote,txtNote)  = 0 AND txtNote LIKE ''%[a-z0-9]%'' AND LEN(txtNote + ''|'' + @txtNote) <= 5120 THEN txtNote + ''|'' + @txtNote
				--			WHEN @txtNote LIKE ''%[a-z0-9]%'' AND CHARINDEX(@txtNote,txtNote)  = 0 AND txtNote LIKE ''%[a-z0-9]%'' AND LEN(txtNote + ''|'' + @txtNote) > 5120 THEN txtNote 
				--			ELSE  @txtNote
				--			END,
				intEHostBookingStatusId = CAST(CASE WHEN @intEHostBookingStatusId  = ''100'' AND @prevEHostBookingStatusId = 200 AND @matchupRelatedChange = 1 THEN 300
												WHEN  @intEHostBookingStatusId  = ''100'' AND @intEHostBookingStatusId != @prevEHostBookingStatusId THEN @prevEHostBookingStatusId
												ELSE @intEHostBookingStatusId END  AS INT),
				intNumberOfUnits = @intNumberOfUnits,
				intNumberOfPartUnits = @intNumberOfPartUnits,
				--intHostId = @intHostId,
				dteAmendedDateTime = @dteAmendedDateTime,
				intAmendedUserId = @intAmendedUserId,
				intPriceItemId = @intHostPriceItemId
				WHERE  (intEHostId = @intEHostId
				AND intEHostBookingId = @intEHostBookingId)
				AND intEHostBookingStatusId < 400;
		
				--IF (@@ROWCOUNT = 0)
				--BEGIN
				--	PRINT ERROR_MESSAGE();
				--	RAISERROR(''No EHostBooking Updated'',16, 1);
		
				--END
			END
			ELSE IF(ISNULL(@intHostPriceItemId,0) != 0  and @numberOfMatchUpRecords > 1 and  @matchupRelatedChange = 1)
                     BEGIN
                           UPDATE [' +@source+ '].[dbo].[tblEHostBooking]
                           SET 
                           intEHostBookingStatusId = CAST(CASE WHEN @intEHostBookingStatusId  = ''100'' AND @prevEHostBookingStatusId = 200 AND @matchupRelatedChange = 1 THEN 300
                                                                                  WHEN  @intEHostBookingStatusId  = ''100'' AND @intEHostBookingStatusId != @prevEHostBookingStatusId THEN @prevEHostBookingStatusId
                                                                                  ELSE @intEHostBookingStatusId END  AS INT),
                           
                           dteAmendedDateTime = @dteAmendedDateTime,
                           intAmendedUserId = @intAmendedUserId
                           
                           WHERE  (intEHostId = @intEHostId
                           )
                           AND intEHostBookingStatusId < 400;
              
                            --IF (@@ROWCOUNT = 0)
                           --BEGIN
                             --     PRINT ERROR_MESSAGE();
                               --   RAISERROR(''No EHostBooking Updated'',16, 1);
              
                           --END
                     END
				';
			EXECUTE sp_executesql @sql, 	N'
@numberOfMatchUpRecords int,
@dteCreatedDateTime DATETIME,
@intCreatedUserId INT,
@dteCreationDate DATE,
@intAmendedUserId INT,
@dteAmendedDateTime DATETIME,
@intNumberOfUnits INT,
@intNumberOfPartUnits INT,
@intChargeUnits INT,
@intChargePartUnits INT, 
@intChargeRate INT,
@intHostId int,
  @source  varchar(20),@intEnrolId varchar(10),@intSchoolId INT,@intPriceItemId INT,
	@intEHostStatusId VARCHAR(10),@intEHostBookingStatusId varchar(10), @blndirect varchar(1),
	@txtCode VARCHAR(25),@dteFromDate DATETIME,@dteToDate DATETIME,@quantity varchar(10),@txtNote varchar(5120),@txtSpecialRequestNote varchar(5120)
	,@txtExternalRef varchar(30),@txtExternalRefDerivedFrom varchar(30), @alreadyCancelled BIT OUT,@prevEHostBookingStatusId int ,@matchupRelatedChange BIT,@txtStudentAddress varchar(5120),@intEPackageId int,@intHostPriceItemId INT OUTPUT, @intEHostId INT OUTPUT, @intEHostBookingId INT OUTPUT,@intEHostId_OUT INT OUTPUT, @intEHostBookingId_OUT INT OUTPUT',
	 @numberOfMatchUpRecords,
	@dteCreatedDateTime ,	
	@intCreatedUserId 	,	
	@dteCreationDate 	,	
	@intAmendedUserId 	,	
	@dteAmendedDateTime ,	
	@intNumberOfUnits 	,	
	@intNumberOfPartUnits, 	
	@intChargeUnits 		,
	@intChargePartUnits 	,
	@intChargeRate 			,
	@intHostId 				
	,@source, @intEnrolId, @intSchoolId, @intPriceItemId,
	@intEHostStatusId,@intEHostBookingStatusId, @blnDirect,
	 @txtCode, @dteFromDate, @dteToDate, @quantity, @txtNote,@txtSpecialRequestNote
  , @txtExternalRef,@txtExternalRefDerivedFrom, @alreadyCancelled OUT,@prevEHostBookingStatusId ,@matchupRelatedChange,@txtStudentAddress,@intEPackageId , @intHostPriceItemId OUTPUT, @intEHostId OUTPUT,@intEHostBookingId OUTPUT,@intEHostId_OUT OUTPUT, @intEHostBookingId_OUT OUTPUT;
		select @intEHostId as '@intEHostId',@intEHostBookingId as '@intEHostBookingId', @intEHostId_OUT as 'intEHostId_OUT',@intEHostBookingId_OUT as 'intEHostBookingId_OUT';
		IF (@alreadyCancelled = 1)
		 RETURN
		If(@prevChargeUnits != @intChargeUnits)
				INSERT INTO @a (intPriceItemId,
									intGroupId,
									intEnrolId,
									intEnrolBookingId,
									intModuleId,
									intInvoiceLineId,
									intUserId,
									intActionId,
									dteTimestamp,
									txtUsername,
									decOldValue,
									decNewValue,
									decDifference
									)
				values				(
									@intPriceItemId,
									-1,
									CAST(@intEnrolId AS INT),
									@intEHostId,
									1800,
									-1,
									9999,
									37,
									getdate(),
									'Apttus',
									@prevChargeUnits,
									@intChargeUnits,
									@intChargeUnits - @prevChargeUnits
									)						
			If(@prevFromDate != @dteFromDate)
					INSERT INTO @a (intPriceItemId,
									intGroupId,
									intEnrolId,
									intEnrolBookingId,
									intModuleId,
									intInvoiceLineId,
									intUserId,
									intActionId,
									dteTimestamp,
									txtUsername,
									dteOldDateValue,
									dteNewDateValue,
									decDifference
									)
					values			(
									@intPriceItemId,
									-1,
									CAST(@intEnrolId AS INT),
									@intEHostId,
									1800,
									-1,
									9999,
									7,
									getdate(),
									'Apttus',
									@prevFromDate,
									@dteFromDate,
									DATEDIFF(dd,@dteFromDate, @prevFromDate)
									)						
			If(@prevToDate != @dteToDate)
					INSERT INTO @a (intPriceItemId,
									intGroupId,
									intEnrolId,
									intEnrolBookingId,
									intModuleId,
									intInvoiceLineId,
									intUserId,
									intActionId,
									dteTimestamp,
									txtUsername,
									dteOldDateValue,
									dteNewDateValue,
									decDifference
									)
					values			(
									@intPriceItemId,
									-1,
									CAST(@intEnrolId AS INT),
									@intEHostId,
									1800,
									-1,
									9999,
									8,
									getdate(),
									'Apttus',
									@prevToDate,
									@dteToDate,
									DATEDIFF(dd,@dteToDate ,@prevToDate)
									)	
			If(@prevChargeRate != @intChargeRate)
					INSERT INTO @a (intPriceItemId,
									intGroupId,
									intEnrolId,
									intEnrolBookingId,
									intModuleId,
									intInvoiceLineId,
									intUserId,
									intActionId,
									dteTimestamp,
									txtUsername,
									decOldValue,
									decNewValue,
									decDifference
									)
					values			(
									@intPriceItemId,
									-1,
									CAST(@intEnrolId AS INT),
									@intEHostId,
									1800,
									-1,
									9999,
									39,
									getdate(),
									'Apttus',
									@prevChargeRate,
									@intChargeRate,
									@intChargeRate - @prevChargeRate
									)	
			
				If(@prevDirectFlag != @blnDirect)
					INSERT INTO @a (intPriceItemId,
									intGroupId,
									intEnrolId,
									intEnrolBookingId,
									intModuleId,
									intInvoiceLineId,
									intUserId,
									intActionId,
									dteTimestamp,
									txtUsername,
									decOldValue,
									decNewValue,
									decDifference
									)
					values			(
									@intPriceItemId,
									-1,
									CAST(@intEnrolId AS INT),
									@intEHostId,
									1800,
									-1,
									9999,
									45,
									getdate(),
									'Apttus',
									@prevDirectFlag,
									@blnDirect,
									0
									)	
			If(@intEHostStatusId = 200)
					INSERT INTO @a (intPriceItemId,
									intGroupId,
									intEnrolId,
									intEnrolBookingId,
									intModuleId,
									intInvoiceLineId,
									intUserId,
									intActionId,
									dteTimestamp,
									txtUsername,
									decOldValue,
									decNewValue,
									decDifference
									)
					values			(
									@intPriceItemId,
									-1,
									CAST(@intEnrolId AS INT),
									@intEHostId,
									1800,
									-1,
									9999,
									31,
									getdate(),
									'Apttus',
									0,
									0,
									0
									)	
		--SELECT @intEHostBookingId_OUT = CASE WHEN IDENT_CURRENT('[' + @source + '].[dbo].[tblEHostBooking]') IS NOT NULL THEN IDENT_CURRENT('[' + @source + '].[dbo].[tblEHostBooking]') ELSE @intEHostBookingId END;

		--SELECT @intEHostId_OUT = CASE WHEN IDENT_CURRENT('[' + @source + '].[dbo].[tblEHost]') IS NOT NULL THEN IDENT_CURRENT('[' + @source + '].[dbo].[tblEHost]') ELSE @intEHostId END;

		END
	
		ELSE IF (@intEHostStatusId != '200')
		BEGIN
		set @operation = 'I';
		SET @sql = '	INSERT INTO ['+@source+'].[dbo].[tblEHost]
			(
				intEnrolId,
				intSchoolId,
				intPriceItemId,
				intEHostStatusId,
				dteCreationDate,
				blnDirect,
				txtCode,
				dteFromDate,
				dteToDate,
				intNumberOfUnits,
				intNumberOfPartUnits,
				intChargeUnits,
				intChargePartUnits,
				intChargeRate,
				txtNote,
				txtSpecialRequestNote,
				txtExternalRef,
				dteCreatedDateTime,
				intCreatedUserId,
				dteAmendedDateTime,
				intAmendedUserId,
				txtStudentAddress,
				intEPackageId

			)
			VALUES
			(
				CAST(@intEnrolId AS INT),
				@intSchoolId,
				@intPriceItemId,
				CAST(@intEHostStatusId AS INT),
				@dteCreationDate,
				CAST(@blnDirect AS BIT),
				@txtCode,
				@dteFromDate,
				@dteToDate,
				@intNumberOfUnits,
				@intNumberOfPartUnits,
				@intChargeUnits,
				@intChargePartUnits,
				@intChargeRate,
				@txtNote,
				@txtSpecialRequestNote,
				@txtExternalRef,
				@dteCreatedDateTime,
				@intCreatedUserId,
				@dteAmendedDateTime,
				@intAmendedUserId,
				@txtStudentAddress,
				@intEPackageId
			);
		
		IF (@@ROWCOUNT = 0)
		BEGIN
			PRINT ERROR_MESSAGE();
			RAISERROR(''No EHost inserted'',16, 1);
			
		END
		
		SELECT @intEHostId = SCOPE_IDENTITY();
	

		IF(ISNULL(@intHostPriceItemId,0) NOT IN (0,-1) )
		BEGIN
			INSERT INTO ['+@source+'].[dbo].[tblEHostBooking]
			(
				intEnrolId,
				intEHostId,
				intHostId,
				intEHostBookingStatusId,
				dteCreatedDateTime,
				intCreatedUserId,
				dteAmendedDateTime,
				intAmendedUserId,
				dteCreationDate,
				dteFromDate,
				dteToDate,
				dtePaidUpToDate,
				intNumberOfUnits,
				intNumberOfPartUnits,
				txtNote,
				intPriceItemId

			)
			VALUES
			(
				CAST(@intEnrolId AS INT),
				@intEHostId,
				@intHostId,
				CAST(@intEHostBookingStatusId AS INT),
				@dteCreatedDateTime,
				@intCreatedUserId,
				@dteAmendedDateTime,
				@intAmendedUserId,
				@dteCreationDate,
				@dteFromDate,
				@dteToDate,
				@dteFromDate,
				@intNumberOfUnits,
				@intNumberOfPartUnits,
				@txtNote,
				@intHostPriceItemId
			);
		
			IF (@@ROWCOUNT = 0)
			BEGIN
				PRINT ERROR_MESSAGE();
				RAISERROR(''No EHostBooking inserted'',16, 1);
			END
			SELECT @intEHostBookingId = SCOPE_IDENTITY();
		END'
		EXECUTE sp_executesql @sql, 	N'
@dteCreatedDateTime 			DATETIME,
@intCreatedUserId 				INT,
@dteCreationDate 				DATE,
@intAmendedUserId 				INT,
@dteAmendedDateTime 			DATETIME,
@intNumberOfUnits 				INT,
@intNumberOfPartUnits 				INT,
@intChargeUnits 				INT,
@intChargePartUnits 				INT, 
@intChargeRate 					INT,
@intHostId 					int,
@source  varchar(20),@intEnrolId varchar(10),@intSchoolId INT,@intPriceItemId INT,
	@intEHostStatusId VARCHAR(10),@intEHostBookingStatusId varchar(10), @blndirect varchar(1),
	@txtCode VARCHAR(25),@dteFromDate DATETIME,@dteToDate DATETIME,@quantity varchar(10),@txtNote varchar(5120),@txtSpecialRequestNote varchar(5120)
	,@txtExternalRef varchar(30),@txtStudentAddress varchar(5120),@intEPackageId int, @intHostPriceItemId INT OUTPUT, @intEHostId INT OUTPUT, @intEHostBookingId INT OUTPUT',
		
	@dteCreatedDateTime ,	
	@intCreatedUserId 	,	
	@dteCreationDate 	,	
	@intAmendedUserId 	,	
	@dteAmendedDateTime ,	
	@intNumberOfUnits 	,	
	@intNumberOfPartUnits, 	
	@intChargeUnits 		,
	@intChargePartUnits 	,
	@intChargeRate 			,
@intHostId 				,
@source, @intEnrolId, @intSchoolId, @intPriceItemId,
	@intEHostStatusId,@intEHostBookingStatusId, @blnDirect,
	 @txtCode, @dteFromDate, @dteToDate, @quantity, @txtNote,@txtSpecialRequestNote
  , @txtExternalRef,@txtStudentAddress, @intEPackageId,@intHostPriceItemId, @intEHostId = @intEHostId_OUT OUTPUT,@intEHostBookingId = @intEHostBookingId_OUT OUTPUT;
	
	--SELECT @intEHostBookingId_OUT = CASE WHEN IDENT_CURRENT('[' + @source + '].[dbo].[tblEHostBooking]') IS NOT NULL THEN IDENT_CURRENT('[' + @source + '].[dbo].[tblEHostBooking]') ELSE @intEHostBookingId_OUT END;

	--SELECT @intEHostId_OUT = CASE WHEN IDENT_CURRENT('[' + @source + '].[dbo].[tblEHost]') IS NOT NULL THEN IDENT_CURRENT('[' + @source + '].[dbo].[tblEHost]') ELSE @intEHostId_OUT END;

	If @operation = 'I'
			INSERT INTO @a (intPriceItemId,
					intGroupId,
					intEnrolId,
					intEnrolBookingId,
					intModuleId,
					intInvoiceLineId,
					intUserId,
					intActionId,
					dteTimestamp,
					txtUsername,
					decOldValue,
					decNewValue,
					decDifference
					)
			values (
					@intPriceItemId,
					-1,
					CAST(@intEnrolId AS INT),
					@intEHostId_OUT,
					1800,
					-1,
					9999,
					22,
					getdate(),
					'Apttus',
					0,
					0,
					0
					)	
	END;
		
	SEt @sql = 'INSERT INTO [' +  @source + '].[dbo].[tblBookingAudit]	( intPriceItemId ,
	 intGroupId  ,
	 intEnrolId  ,
	 intEnrolBookingId ,
	 intModuleId  ,
	 intInvoiceLineId ,
	 intUserId  ,
	 intActionId,
	 dteTimestamp ,
	 txtUsername  ,
	 dteOldDateValue ,
	 dteNewDatevalue  ,
	 decOldValue  ,
	 decNewValue  ,
	 decDifference ,
	 txtNote  ,
	 txtComment )	
	SELECT  intPriceItemId ,
	 intGroupId  ,
	 intEnrolId  ,
	 intEnrolBookingId ,
	 intModuleId  ,
	 intInvoiceLineId ,
	 intUserId  ,
	 intActionId,
	 dteTimestamp ,
	 txtUsername  ,
	 dteOldDateValue ,
	 dteNewDatevalue  ,
	 decOldValue  ,
	 decNewValue  ,
	 decDifference ,
	 txtNote  ,
	 txtComment  from @a';
	 EXECUTE sp_executesql @sql,N'@a auditLog READONLY',@a
		
			

RETURN 0
END
