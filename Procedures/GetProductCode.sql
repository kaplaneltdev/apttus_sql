USE [ClassIntegration]
GO
/****** Object:  StoredProcedure [APTTUS].[GetProductCode]    Script Date: 18/11/2020 12:28:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [APTTUS].[GetProductCode]
@ProductName VARCHAR(100), 
@ClassDB VARCHAR(14),
@AttributeName1 VARCHAR(200),
@AttributeName2 VARCHAR(200),
@AttributeName3 VARCHAR(200),
@AttributeName4 VARCHAR(200),
@AttributeValue1 VARCHAR(200),
@AttributeValue2 VARCHAR(200),
@AttributeValue3 VARCHAR(200),
@AttributeValue4 VARCHAR(200),
@ProductCode VARCHAR(30) OUT

AS
BEGIN
	SELECT @ProductCode =  p.ProductCode
	FROM APTTUS.PRODUCT p
	INNER JOIN APTTUS.PRODUCTGROUP pg on pg.ProductGroupId = p.ProductGroupId and p.classdb = @ClassDB
	LEFT OUTER JOIN APTTUS.MAP_PRODUCTGROUP_ATTRIBUTE mpg1 on mpg1.ProductGroupId = pg.ProductGroupId
	LEFT OUTER JOIN APTTUS.ATTRIBUTE a1 on a1.AttributeId = mpg1.AttributeId AND rtrim(ltrim(a1.AttributeName)) = rtrim(ltrim(@AttributeName1))
	LEFT OUTER JOIN APTTUS.MAP_PRODUCT_ATTRIBUTE mpa1 on mpa1.ProductId = p.ProductId AND mpa1.AttributeId = a1.AttributeId	AND REPLACE(rtrim(ltrim(mpa1.value)),'''s','s')  = REPLACE(rtrim(ltrim(@AttributeValue1)),'''s','s')
	LEFT OUTER JOIN APTTUS.MAP_PRODUCTGROUP_ATTRIBUTE mpg2 on mpg2.ProductGroupId = pg.ProductGroupId
	LEFT OUTER JOIN APTTUS.ATTRIBUTE a2 on a2.AttributeId = mpg2.AttributeId AND rtrim(ltrim(a2.AttributeName)) = rtrim(ltrim(@AttributeName2))
	LEFT OUTER JOIN APTTUS.MAP_PRODUCT_ATTRIBUTE mpa2 on mpa2.ProductId = p.ProductId AND mpa2.AttributeId = a2.AttributeId	AND REPLACE(rtrim(ltrim(mpa2.value)),'''s','s') = REPLACE(rtrim(ltrim(@AttributeValue2)),'''s','s')
	LEFT OUTER JOIN APTTUS.MAP_PRODUCTGROUP_ATTRIBUTE mpg3 on mpg3.ProductGroupId = pg.ProductGroupId
	LEFT OUTER JOIN APTTUS.ATTRIBUTE a3 on a3.AttributeId = mpg3.AttributeId AND rtrim(ltrim(a3.AttributeName)) = rtrim(ltrim(@AttributeName3))
	LEFT OUTER JOIN APTTUS.MAP_PRODUCT_ATTRIBUTE mpa3 on mpa3.ProductId = p.ProductId AND mpa3.AttributeId = a3.AttributeId	AND REPLACE(rtrim(ltrim(mpa3.value)),'''s','s') = REPLACE(rtrim(ltrim(@AttributeValue3)),'''s','s')
	LEFT OUTER JOIN APTTUS.MAP_PRODUCTGROUP_ATTRIBUTE mpg4 on mpg4.ProductGroupId = pg.ProductGroupId
	LEFT OUTER JOIN APTTUS.ATTRIBUTE a4 on a4.AttributeId = mpg4.AttributeId AND rtrim(ltrim(a4.AttributeName)) = rtrim(ltrim(@AttributeName4))
	LEFT OUTER JOIN APTTUS.MAP_PRODUCT_ATTRIBUTE mpa4 on mpa4.ProductId = p.ProductId AND mpa4.AttributeId = a4.AttributeId	AND REPLACE(rtrim(ltrim(mpa4.value)),'''s','s') = REPLACE(rtrim(ltrim(@AttributeValue4)),'''s','s')
	WHERE 
	(
		p.ProductName = @ProductName  AND
		pg.ProductGroupName IN ('Products without Attributes','Special Diet','Close To School') AND
		mpa1.ProductAttributeValueId IS NULL AND 
		mpa2.ProductAttributeValueId IS NULL AND
		mpa3.ProductAttributeValueId IS NULL AND
		mpa4.ProductAttributeValueId IS NULL
	)
	OR
	(
		pg.ProductGroupName = @ProductName  AND
		pg.ProductGroupName IN ('Intensive Courses','GMAT/GRE','GMAT/GRE Extension','GMAT/GRE Higher Score Guarantee','GMAT/GRE Online Access') AND
		(	mpa1.ProductAttributeValueId IS NOT NULL AND
			mpa2.ProductAttributeValueId IS NULL AND
			mpa3.ProductAttributeValueId IS NULL AND
			mpa4.ProductAttributeValueId IS NULL
		)
	) 
	OR
	(
		pg.ProductGroupName = @ProductName  AND  pg.ProductGroupName in ('Homestay', 'Residence', 'Residence ALP','Homestay ALP','Extra Night Homestay', 'Extra Night Residence')
		AND
		(	mpa1.ProductAttributeValueId IS NOT NULL AND
			mpa2.ProductAttributeValueId IS NOT NULL AND
			mpa3.ProductAttributeValueId IS NOT NULL AND
			mpa4.ProductAttributeValueId IS NOT NULL
	
		)
	) 
	OR
	(
		pg.ProductGroupName = @ProductName  AND
		(pg.ProductGroupName IN ('Transfer Service','Academic Year','Academic Semester','Cambridge/EAP', 'Cambridge FCE', 'Cambridge CAE') )AND
		(	mpa1.ProductAttributeValueId IS NOT NULL AND
			mpa2.ProductAttributeValueId IS NOT NULL AND
			mpa3.ProductAttributeValueId IS NULL AND
			mpa4.ProductAttributeValueId IS NULL
		)
	) 
	OR
	(
		pg.ProductGroupName = @ProductName  AND
		(pg.ProductGroupName IN ('Fixed Length Courses') )AND
		(	mpa1.ProductAttributeValueId IS NOT NULL AND
			mpa2.ProductAttributeValueId IS NOT NULL AND
			mpa3.ProductAttributeValueId IS NULL AND
			mpa4.ProductAttributeValueId IS NULL
		)
	);
	--SELECT  @ProductCode as 'productCode';


END

