USE [ClassIntegration]
GO

/****** Object:  StoredProcedure [APTTUS].[UpsertStudent]    Script Date: 03/03/2020 17:05:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [APTTUS].[UpsertStudent]
@txtSexCode varchar(10),
@intPassportCountryId varchar(6),
@intNationalityId varchar(6),
@source varchar(30),
@txtExternalref      varchar(10),
@txtCode      varchar(10),
@txtTitle     varchar(10),
@txtForename  varchar(45),
@txtSurname   varchar(45),
@intOriginCountryId  varchar(6),
@intCountryId varchar(6),
@dteDOB       date,
@txtAddress1  varchar(30),
@txtAddress2  varchar(30),
@txtTown      varchar(30),
@txtRegion    varchar(30),
@txtPostalCode       varchar(20),
@txtEmail     varchar(255),
@txtTelephone varchar(20),
@txtFax       varchar(20),
@txtMobileNumber     varchar(20),
@intLanguageId varchar(6),
@txtNote1 varchar(5120),
@txtPassportNumber varchar(100),
@txtOtherTown VARCHAR(30),
@txtOtherCountry VARCHAR(30),
@txtOtherEMail VARCHAR(255),
@txtOtherAddressee VARCHAR(45),
@txtOtherTelephone VARCHAR(20),
@txtOtherPostalCode VARCHAR(20),
@txtOtherAddress1 VARCHAR(30),
@passportExpiry varchar(15),
@intStudentId_OUT INT OUTPUT



AS
BEGIN

       DECLARE @intStudentid      int = 0;
       DECLARE @sql nvarchar(max);
       SELECT @intPassportCountryId = CASE WHEN @intPassportCountryId  = '0.0' THEN NULL ELSE @intPassportCountryId END,
       @intNationalityId = CASE WHEN @intNationalityId = '0.0' THEN NULL ELSE @intNationalityId END,
       @intCountryId =  CASE WHEN @intCountryId = '0.0' THEN NULL ELSE @intCountryId END,
       @intOriginCountryId =  CASE WHEN  @intOriginCountryId = '0.0' THEN NULL ELSE @intOriginCountryId END,
       @intLanguageId = CASE WHEN  @intLanguageId = '0.0' THEN NULL ELSE @intLanguageId END;
       DECLARE @intModuleId int;
       DECLARE @intSexId int;
       DECLARE @intStudentStatusId int;
       DECLARE @txtCountry varchar(30) = '';

       SET NOCOUNT ON;
       SET @sql = '


       SET @intModuleId = 1000;

       SELECT @intSexId = CASE WHEN @txtSexCode = ''Male'' THEN 1
                                                WHEN @txtSexCode = ''Female'' THEN 2
                                         END;


       SELECT @intStudentStatusId = 200;


       SELECT @txtCountry = txtCode
       FROM [' +@source+ '].[dbo].tblCountry
       WHERE intCountryId = CAST(CAST(@intCountryId AS FLOAT) AS INT);

              SET NOCOUNT ON;



              SELECT @intStudentid = intStudentID
              FROM [' +@source+ '].[dbo].[tblStudent]
              WHERE (       txtForeName = @txtForename
              AND txtSurname = @txtSurname
              AND dteDOB = @dteDOB
              AND isnull(intOriginCountryId,0) = CAST(CAST(isnull(@intOriginCountryId,0) AS FLOAT) AS INT)
              AND intSexId = @intSexId
              AND (  txtEmail = @txtEmail
                           OR  txtAddress1 = @txtAddress1

                     )
              ) OR txtExternalRef = @txtexternalref';

       EXECUTE sp_executesql @sql,N'@intModuleId int out, @intSexId int out, @intStudentStatusId int out, @txtCountry varchar(30) out, @intStudentid int out,@txtSexCode varchar(10), @intCountryId varchar(6), @txtForename   varchar(45),@txtSurname    varchar(45),@intOriginCountryId varchar(6), @dteDOB date,@txtEmail     varchar(255),@txtAddress1  varchar(30),
@txtTelephone varchar(20),@txtMobileNumber      varchar(20),@txtExternalref varchar(10)',@intModuleId  out, @intSexId out, @intStudentStatusId  out, @txtCountry out, @intStudentid out,@txtSexCode , @intCountryId, @txtForename,@txtSurname,@intOriginCountryId, @dteDOB,@txtEmail,@txtAddress1,
@txtTelephone,@txtMobileNumber, @txtExternalref ;

       SET @sql = '
              IF (ISNULL(@intStudentid,0) != 0)
              BEGIN
                     UPDATE ['+@source+ '].[dbo].[tblStudent]
                     SET intSexId = @intSexId,
                     intPassportCountryId = CAST(CAST(@intPassportCountryId AS FLOAT) AS INT),
                     intNationalityId = CAST(CAST(@intNationalityId AS FLOAT) AS INT),
                     txtexternalref = @txtExternalref,
                     txtcode = CASE WHEN @txtCode = txtCode THEN txtCode
                                        WHEN @txtCode LIKE ''%[a-z0-9]%'' AND LEN(@txtCode) <= 10 THEN @txtCode
                                        ELSE txtCode
                                        END,
                     txtTitle = @txtTitle,
                     txtForename = @txtForename,
                     txtSurname = @txtSurname,
                     txtAddressee = @txtTitle + SUBSTRING(@txtForename,1,1) + '' ''+  @txtSurname,
                     intCountryId = CAST(CAST(@intCountryId AS FLOAT) AS INT),
                     intOriginCountryId = CAST(CAST(@intOriginCountryId AS FLOAT) AS INT),
                     intLanguageId =  CAST(CAST(@intLanguageId AS FLOAT) AS INT),
                     dteDOB = @dteDOB,
                     txtAddress1 = CASE WHEN @txtAddress1 = txtAddress1 THEN txtAddress1
                                         WHEN @txtAddress1 LIKE ''%[a-z0-9]%'' AND LEN(@txtAddress1) <= 30 THEN @txtAddress1
                                         ELSE txtAddress1
                                         END,
                     txtAddress2 = CASE WHEN @txtAddress2 = txtAddress2 THEN txtAddress2
                                         WHEN @txtAddress2 LIKE ''%[a-z0-9]%''  AND LEN(@txtAddress2) <= 30 THEN @txtAddress2
                                         ELSE txtAddress2
                                         END,
                     txtTown = CASE WHEN @txtTown = txtTown THEN txtTown
                                         WHEN @txtTown LIKE ''%[a-z0-9]%'' AND LEN(@txtTown) <= 30 THEN @txtTown
                                         ELSE txtTown
                                         END,
                     txtRegion = CASE WHEN @txtRegion = txtRegion THEN txtRegion
                                         WHEN @txtRegion LIKE ''%[a-z0-9]%'' AND LEN(@txtRegion) <= 30 THEN @txtRegion
                                         ELSE txtRegion
                                         END,
                     txtCountry = CASE WHEN @txtCountry = txtCountry THEN txtCountry
                                         WHEN @txtCountry LIKE ''%[a-z0-9]%''  AND LEN(@txtCountry) <= 30 THEN @txtCountry
                                         ELSE txtCountry
                                         END,
                     txtPostalCode = CASE WHEN @txtPostalCode = txtPostalCode THEN txtPostalCode
                                         WHEN @txtPostalCode LIKE ''%[a-z0-9]%'' AND LEN(@txtPostalCode) <= 20 THEN @txtPostalCode
                                         ELSE txtPostalCode
                                         END,
                     txtEmail = CASE WHEN @txtEmail = txtEmail THEN txtEmail
                                         WHEN @txtEmail LIKE ''%[a-z0-9]%'' AND LEN(@txtEmail) <= 255 THEN @txtEmail
                                         ELSE txtEmail
                                         END,
                     txtTelephone = CASE WHEN @txtTelephone = txtTelephone THEN txtTelephone
                                  WHEN @txtTelephone LIKE ''%[a-z0-9]%'' AND LEN(@txtTelephone) <= 20 THEN @txtTelephone
                                  ELSE txtTelephone
                                  END,
                     txtFax = CASE WHEN txtFax LIKE ''%[a-z0-9]%'' AND @source in (''CFW_ASPECT_CAN'',''CFW_ASPECT_USA'') THEN  txtFax ELSE @txtFax END,
                     txtMobileNumber = CASE WHEN  txtFax LIKE ''%[a-z0-9]%'' AND @source in (''CFW_ASPECT_CAN'',''CFW_ASPECT_USA'') THEN txtMobileNumber
													   WHEN @txtMobileNumber = txtMobileNumber THEN txtMobileNumber
                                                       WHEN @txtMobileNumber LIKE ''%[a-z0-9]%'' AND LEN(@txtMobileNumber) <= 20 THEN @txtMobileNumber
                                                       ELSE txtMobileNumber
                                                       END,
                     intAmendedUserId = 9999,
                     dteAmendedDateTime  = getdate(),
                     txtNote1 = CASE WHEN @txtNote1 = txtNote1 THEN txtNote1
                                                WHEN @txtNote1 LIKE ''%[a-z0-9]%'' AND LEN(@txtNote1) <= 5120 THEN @txtNote1
                                                ELSE txtNote1
                                                END,
                     txtPassportNumber = CASE WHEN @txtPassportNumber = txtPassportNumber THEN txtPassportNumber
                                                              WHEN @txtPassportNumber LIKE ''%[a-z0-9]%'' AND LEN(@txtPassportNumber) <= 100 THEN @txtPassportNumber
                                                              ELSE txtPassportNumber
                                                              END,

                    txtOtherTown = CASE WHEN @txtOtherTown = txtOtherTown THEN @txtOtherTown
										WHEN @txtOtherTown like ''%[a-z0-9]%'' THEN @txtOtherTown
										ELSE txtOtherTown END,
					txtOtherCountry = CASE WHEN @txtOtherCountry = txtOtherCountry THEN @txtOtherCountry
										WHEN @txtOtherCountry like ''%[a-z0-9]%'' THEN @txtOtherCountry
										ELSE txtOtherCountry END,
					txtOtherEMail = CASE WHEN @txtOtherEMail = txtOtherEMail THEN @txtOtherEMail
										WHEN @txtOtherEMail like ''%[a-z0-9]%'' THEN @txtOtherEMail
										ELSE txtOtherEMail END,
					txtOtherAddressee = CASE WHEN @txtOtherAddressee = txtOtherAddressee THEN @txtOtherAddressee
										WHEN @txtOtherAddressee like ''%[a-z0-9]%'' THEN @txtOtherAddressee
										ELSE txtOtherAddressee END,
					txtOtherTelephone = CASE WHEN @txtOtherTelephone = txtOtherTelephone THEN @txtOtherTelephone
										WHEN @txtOtherTelephone like ''%[a-z0-9]%'' THEN @txtOtherTelephone
										ELSE txtOtherTelephone END,
					txtOtherPostalCode = CASE WHEN @txtOtherPostalCode = txtOtherPostalCode THEN @txtOtherPostalCode
										WHEN @txtOtherPostalCode like ''%[a-z0-9]%'' THEN @txtOtherPostalCode
										ELSE txtOtherPostalCode END,
					txtOtherAddress1 = CASE WHEN @txtOtherAddress1 = txtOtherAddress1 THEN @txtOtherAddress1
										WHEN @txtOtherAddress1 like ''%[a-z0-9]%'' THEN @txtOtherAddress1
										ELSE txtOtherAddress1 END,
                    dtePassportExpiryDate =  CASE WHEN @passportExpiry = dtePassportExpiryDate or @passportExpiry = ''1900-01-01''  THEN dtePassportExpiryDate
										WHEN @passportExpiry like ''%[a-z0-9]%'' and @passportExpiry != ''1900-01-01'' THEN cast(@passportExpiry as date)
										ELSE dtePassportExpiryDate END
					WHERE intStudentId = @intStudentid
                     IF(@@ROWCOUNT != 1)
                           RAISERROR(''No Student Updated!'', 16, 1)

              END';

       EXECUTE sp_executesql @sql, N'@txtSexCode varchar(10), @intPassportCountryId varchar(6), @intNationalityId varchar(6), @source varchar(30), @txtExternalref   varchar(10), @txtCode       varchar(10),
       @txtTitle     varchar(10), @txtForename  varchar(45), @txtSurname   varchar(45), @intOriginCountryId  varchar(6), @intCountryId  varchar(6), @dteDOB  varchar(10), @txtAddress1       varchar(30), @txtAddress2  varchar(30),
       @txtTown      varchar(30), @txtRegion    varchar(30), @txtPostalCode varchar(20), @txtEmail       varchar(255), @txtTelephone varchar(20), @txtFax varchar(20), @txtMobileNumber     varchar(20), @intLanguageId varchar(6), @txtNote1 varchar(5120), @txtPassportNumber varchar(100),@intModuleId int, @intSexId int, @intStudentStatusId int , @txtCountry varchar(30) , @txtOtherTown VARCHAR(30),
	   @txtOtherCountry VARCHAR(30), @txtOtherEMail VARCHAR(255), @txtOtherAddressee VARCHAR(45), @txtOtherTelephone VARCHAR(20), @txtOtherPostalCode VARCHAR(20), @txtOtherAddress1 VARCHAR(30), @passportExpiry varchar(15),@intStudentId int OUTPUT',
       @txtSexCode, @intPassportCountryId, @intNationalityId, @source, @txtExternalref, @txtCode, @txtTitle, @txtForename, @txtSurname, @intOriginCountryId, @intCountryId, @dteDOB, @txtAddress1, @txtAddress2,
       @txtTown, @txtRegion, @txtPostalCode, @txtEmail, @txtTelephone, @txtFax, @txtMobileNumber, @intLanguageId, @txtNote1, @txtPassportNumber,@intModuleId , @intSexId , @intStudentStatusId  , @txtCountry , @txtOtherTown, @txtOtherCountry,  @txtOtherEMail,  @txtOtherAddressee, @txtOtherTelephone,
       @txtOtherPostalCode, @txtOtherAddress1, @passportExpiry, @intStudentid   OUTPUT;


       select @intStudentId_OUT = @intStudentId;



              IF (ISNULL(@intStudentid,0) = 0)
              BEGIN
       SET @sql = '         INSERT INTO ['+@source+ '].[dbo].[tblStudent](
                     intSexId,
                     intModuleId,
                     intStudentStatusId,
                     txtexternalref,
                     txtcode,
                     txtTitle,
                     txtForename,
                     txtSurname,
                     intCountryId,
                     intOriginCountryId,
                     intPassportCountryId,
                     intNationalityId,
                     intLanguageId,
                     dteDOB,
                     txtAddressee,
                     txtAddress1,
                     txtAddress2,
                     txtTown,
                     txtRegion,
                     txtCountry,
                     txtPostalCode,
                     txtEmail,
                     txtTelephone,
                     txtFax,
                     txtMobileNumber,
                     intCreatedUserId,
                     dteCreatedDateTime,
                     dteCreationDate,
                     intAmendedUserId,
                     dteAmendedDateTime,
                     txtStatementAddressee,
                     txtStatementAddress1,
                     txtStatementAddress2,
                     txtStatementTown,
                     txtStatementRegion,
                     txtStatementCountry,
                     txtStatementPostalCode,
                     txtStatementTelephone,
                     txtStatementFax,
                     txtStatementEMail,
                     txtOtherAddressee,
                     txtOtherAddress1,
                     txtOtherAddress2,
                     txtOtherTown,
                     txtOtherRegion,
                     txtOtherCountry,
                     txtOtherPostalCode,
                     txtOtherTelephone,
                     txtOtherFax,
                     txtOtherEMail,
                     txtEnterpriseAccountCode,
                     txtNote1,
                     txtNote2,
                     txtNote3,
                     txtAnalysis1,
                     txtAnalysis2,
                     txtAnalysis3,
                     txtAnalysis4,
                     txtAnalysis5,
                     txtAnalysis6,
                     txtPassportNumber,
					 dtePassportExpiryDate
                     )
                     VALUES(
                     @intSexId,
                     @intModuleId,
                     @intStudentStatusId,
                     @txtexternalref,
                     @txtcode,
                     @txtTitle,
                     @txtForename,
                     @txtSurname,
                     CAST(CAST(@intCountryId AS FLOAT) AS INT),
                     CAST(CAST(@intOriginCountryId AS FLOAT) AS INT),
                     CAST(CAST(@intPassportCountryId AS FLOAT) AS INT),
                     CAST(CAST(@intNationalityId AS FLOAT) AS INT),
                     CAST(CAST(@intLanguageId AS FLOAT) AS INT),
                     @dteDOB,
                     @txtTitle + SUBSTRING(@txtForename,1,1)  + '' ''+  @txtSurname,
                     @txtAddress1,
                     @txtAddress2,
                     @txtTown,
                     @txtRegion,
                     @txtCountry,
                     @txtPostalCode,
                     @txtEmail,
                     @txtTelephone,
                     @txtFax,
                     @txtMobileNumber,
                     9999,
                     GETDATE(),
                     cast(getdate() as date),
                     9999,
                     GETDATE(),
                     '' '',
                     '' '',
                     '' '',
                     '' '',
                     '' '',
                     '' '',
                     '' '',
                     '' '',
                     '' '',
                     '' '',
                     '' '',
                     '' '',
                     '' '',
                     '' '',
                     '' '',
                     '' '',
                     '' '',
                     '' '',
                     '' '',
                     '' '',
                     '' '',
                     ISNULL(@txtNote1,''''),
                     '' '',
                     '' '',
                     '' '',
                     '' '',
                     '' '',
                     '' '',
                     '' '',
                     '' '',
                     @txtPassportNumber,
					 cast(@passportExpiry as date)
                     )

                     IF(@@ROWCOUNT != 1)
                           RAISERROR(''No Student Inserted!'', 16, 1)

                     SELECT @intStudentid = SCOPE_IDENTITY();

       ';
       EXECUTE sp_executesql @sql, N'@txtSexCode varchar(10), @intPassportCountryId varchar(6), @intNationalityId varchar(6), @source varchar(30), @txtExternalref   varchar(10), @txtCode       varchar(10),
       @txtTitle     varchar(10), @txtForename  varchar(45), @txtSurname   varchar(45), @intOriginCountryId  varchar(6), @intCountryId  varchar(6), @dteDOB  varchar(10), @txtAddress1       varchar(30), @txtAddress2  varchar(30),
       @txtTown      varchar(30), @txtRegion    varchar(30), @txtPostalCode varchar(20), @txtEmail       varchar(255), @txtTelephone varchar(20), @txtFax varchar(20), @txtMobileNumber     varchar(20), @intLanguageId varchar(6), @txtNote1 varchar(5120), @txtPassportNumber varchar(100),@intModuleId int, @intSexId int, @intStudentStatusId int , @txtCountry varchar(30),  @passportExpiry varchar(15) , @intStudentId int OUTPUT',
       @txtSexCode, @intPassportCountryId, @intNationalityId, @source, @txtExternalref, @txtCode, @txtTitle, @txtForename, @txtSurname, @intOriginCountryId, @intCountryId, @dteDOB, @txtAddress1, @txtAddress2,
       @txtTown, @txtRegion, @txtPostalCode, @txtEmail, @txtTelephone, @txtFax, @txtMobileNumber, @intLanguageId, @txtNote1, @txtPassportNumber,@intModuleId , @intSexId , @intStudentStatusId  , @txtCountry , @passportExpiry , @intStudentid = @intStudentId_OUT OUTPUT;
       select @intStudentId_OUT = CASE WHEN @intStudentId != 0 THEN @intStudentId ELSE @intStudentId_OUT END;
       END;


RETURN 0
END


GO
