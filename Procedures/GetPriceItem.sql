USE [ClassIntegration]
GO
/****** Object:  StoredProcedure [APTTUS].[GetPriceItem]    Script Date: 18/11/2020 12:25:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [APTTUS].[GetPriceItem]
@airport_Station VARCHAR(100),
@vehicle VARCHAR(50),
@ModuleId VARCHAR(10),
@ClassDB VARCHAR(14),
@startDate VARCHAR(30),
@EndDate VARCHAR(30),
@txtSchoolCode VARCHAR(10),
@homestayRoomType varchar(100),
@locationZone varchar(100),
@mealOptions varchar(100),
@private_Bath varchar(100),
@residenceName varchar(100),
@residenceRoomType varchar(100),
@productName varchar(100), 
@CourseIntensity varchar(100), 
@fixedWeeks varchar(100),
@intensity varchar(100),
@discountCode varchar(100),
@id varchar(100),
@intPriceItemId INT output,
@intLanguageId varchar(6),
@txtPriceItemName_OUT VARCHAR(100) OUTPUT ,
@txtPriceItemLanguageName_OUT VARCHAR(200) OUTPUT ,
@intPackageId INT output,
@txtPackageName VARCHAR(100) OUTPUT ,
@txtPackageLanguageName VARCHAR(200) OUTPUT 

--,
--@intCurNumberofUnits int output,
--@intCurNumberofPartUnits int output

AS
BEGIN
	DECLARE @sql nvarchar(max);
	DECLARE @intSchoolId int;
	DECLARE @txtCode VARCHAR(30);

	SELECT @intLanguageId = CASE WHEN  @intLanguageId = '0.0' THEN NULL ELSE @intLanguageId END;
	EXEC APTTUS.GetSchoolId @txtSchoolCode,@ClassDB,@intSchoolId OUT;
	IF @locationZone = 'Close To School'
		SELECT @locationZone = 'NA'
	IF @productName = 'Arrival Transfer Service' OR @productName = 'Departure Transfer Service' or @productName = 'Alpadia Arrival Transfer Service' or @productName = 'Alpadia Departure Transfer Service'
		SELECT @productName = 'Transfer Service';
	--IF @productName like '%Academic%' 
	--	SELECT @productName = 'Academic Year/Semester';
	DECLARE @weeks int = 0;
	IF @fixedWeeks != 'null'
		SELECT @weeks = CAST(CAST(@fixedweeks AS  decimal) as INT);
    ELSE 
		SELECT @weeks = 0;
	IF @discountCode != ' ' AND @discountCode != 'Check Product' AND @discountCode != 'null' AND len(@id) in (19,20) and @id like '%d'
	BEGIN
		SELECT @txtCode = @discountCode;
	END
	ELSE IF @ModuleId = '1550'
	BEGIN
		--If everthing works out may be this could be removed as the  ELSE Clause might work
		EXEC APTTUS.GetProductCode @ProductName,@ClassDB,null,null,null,null,null,null,null,null, @txtCode OUT;	
	END
	ELSE IF @ModuleId = '1800'
	BEGIN
		IF @productName in ( 'Residence', 'Extra Night Residence')
			EXEC APTTUS.GetProductCode @ProductName,@ClassDB,'Residence Room Type','Residence Name','Private Bath','Meal Options',@residenceRoomType,@residenceName,@private_Bath,@mealOptions, @txtCode OUT;
		ELSE IF @productName in ( 'Homestay', 'Extra Night Homestay','Fixed Length Accommodation (Homestay)')
	
	
			EXEC APTTUS.GetProductCode @ProductName,@ClassDB,'Homestay Room Type','Location(Zone)','Private Bath','Meal Options',@homestayRoomType,@locationZone,@private_Bath,@mealOptions, @txtCode OUT;
		
		ELSE 
			EXEC APTTUS.GetProductCode @ProductName,@ClassDB,null,null,null,null,null,null,null,null, @txtCode OUT;
	END		
	

	ELSE IF @ModuleId = '2000'
		EXEC APTTUS.GetProductCode @ProductName,@ClassDB,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, @txtCode OUT;

    ELSE IF @ModuleId = '1600' AND (@productName like '%Academic%' or @productName like '%Cambridge/EAP%' )
		--EXEC APTTUS.GetProductCode @ProductName,@ClassDB,'Fixed Weeks','Intensity','Intensive Course Type',null,@fixedWeeks,@intensity,@CourseIntensity,null, @txtCode OUT;
		EXEC APTTUS.GetProductCode @ProductName,@ClassDB,'Fixed Weeks','Intensity',null,null,@weeks,@intensity,null,null, @txtCode OUT;
	ELSE IF @ModuleId = '1600' AND (@productName like '%Intensive Courses%' OR @productName in ('GMAT/GRE Extension','GMAT/GRE Online Access','GMAT/GRE Higher Score Guarantee','GMAT/GRE Online Access'))
		EXEC APTTUS.GetProductCode @ProductName,@ClassDB,'Intensive Course Type',null,null,null,@CourseIntensity,null,null,null, @txtCode OUT;
    ELSE IF @ModuleId = '1600' AND  @productName in ( 'GMAT/GRE')
		EXEC APTTUS.GetProductCode @ProductName,@ClassDB,'Intensity',null,null,null,@intensity,null,null,null, @txtCode OUT;
    ELSE IF @ModuleId = '1900' AND @productName != 'No Transfer' AND @productName not like '%Arrival Transfer Service (Junior)%' AND @productName not like '%Departure Transfer Service (Junior)%'
		EXEC APTTUS.GetProductCode @ProductName,@ClassDB,'airport_station','vehicle',null,null,@airport_Station,@vehicle,null,null, @txtCode OUT;
	ELSE IF @ModuleId = '1900' AND ( @productName = 'No Transfer' or @productName = 'Group Arrival Transfer Service' or @productName = 'Group Departure Transfer Service' )
		EXEC APTTUS.GetProductCode @ProductName,@ClassDB,null,null,null,null,null,null,null,null, @txtCode OUT;	
	ELSE 
		EXEC APTTUS.GetProductCode @ProductName,@ClassDB,null,null,null,null,null,null,null,null, @txtCode OUT;	
	select @txtCode as txtcode 
	DECLARE @query nvarchar(max);
	SELECT @intPriceItemId = NULL;
	SET @query = '
					SELECT  @intPriceItemId' + char(61) + ' p.[intPriceItemId] , @txtPriceItemName_OUT' + CHAR(61)   +  ' p.txtName, @txtPriceItemLanguageName_OUT =  ISNULL(l.txtName, '' '')
					FROM  [' + @ClassDB+ '].[dbo].[tblPriceItem] p 
					LEFT OUTER JOIN [' + @ClassDB+ '].[dbo].[tblpriceitemLanguage] l ON l.intPriceItemId = p.intPriceItemId AND CAST(CAST(@intLanguageId AS FLOAT) AS INT) = l.intCorrespondLanguageId
					WHERE [txtCode] = '+ CHAR(39) + @txtCode + CHAR(39) +
					'AND blnActive = 1 AND intSchoolId = ' +  CAST(@intSchoolId AS VARCHAR(4));
					--select @query as 'query';
					EXEC sp_executesql @query, N'@intPriceItemId INT OUT, @txtPriceItemName_OUT VARCHAR(100) OUT,@intLanguageId varchar(6),@txtPriceItemLanguageName_OUT VARCHAR(200) OUT', @intPriceItemId OUT, @txtPriceItemName_OUT OUT,@intLanguageId,@txtPriceItemLanguageName_OUT OUT ;
	                SELECT @intPriceItemId = @intPriceItemId;
	SET @query = '
					SELECT  @intPackageId' + char(61) + ' p.[intPackageId] , @txtPackageName' + CHAR(61)   +  ' p.txtName, @txtPackageLanguageName =  ISNULL(l.txtName, '' '')
					FROM  [' + @ClassDB+ '].[dbo].[tblPackage] p 
					LEFT OUTER JOIN [' + @ClassDB+ '].[dbo].[tblpackagelanguage] l ON l.intPackageId = p.intPackageId AND CAST(CAST(@intLanguageId AS FLOAT) AS INT) = l.intCorrespondLanguageId
					WHERE [txtCode] = '+ CHAR(39) + @txtCode + CHAR(39) +
					'AND blnActive = 1 AND intSchoolId = ' +  CAST(@intSchoolId AS VARCHAR(4));
					--select @query as 'query';
					EXEC sp_executesql @query, N'@intPackageId INT OUT, @txtPackageName VARCHAR(100) OUT,@intLanguageId varchar(6),@txtPackageLanguageName VARCHAR(200) OUT', @intPackageId OUT, @txtPackageName OUT,@intLanguageId,@txtPackageLanguageName OUT ;
									
					
					--SELECT @intCurNumberofUnits  = DATEDIFF(dd,@startDate,@endDate)/7, 
					--@intCurNumberofPartUnits = DATEDIFF(dd,@startDate,@endDate)%7;
					IF @txtCode IS NULL OR @intPriceItemId IS NULL
					BEGIN	
						PRINT @txtSchoolCode;
						PRINT @txtCode;
						PRINT @productName;
						PRINT @intPriceItemId;
					END
END
