USE [ClassIntegration]
GO
/****** Object:  StoredProcedure [APTTUS].[UpsertEPackage]    Script Date: 07/01/2020 15:56:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [APTTUS].[UpsertEPackage] 
	-- Add the parameters for the stored procedure here

	---what are the attributes needed
	@productName varchar(100), 
	@source  varchar(20),
	@intEnrolId varchar(10),
	@txtSchoolCode varchar(3),
	@intEPackageStatusId varchar(10),
	@blndirect varchar(1),
	@txtCode VARCHAR(25),
	@txtNarrative VARCHAR(200),
	@dteFromDate DATETIME,
	@dteToDate DATETIME,
	@quantity varchar(10),
	--Not taking the quantities from apttus
	--deriving them based on the from and to dates
    @txtNote varchar(5120),
	@txtExternalRef varchar(30),
	@txtExternalRefDerivedFrom varchar(30),
	
	@txtPackageCode varchar(25) OUTPUT,
	@intEPackageId int output,
	@txtPackageName VARCHAR(200) OUTPUT
AS
BEGIN
		DECLARE @intSchoolId int;
	    DECLARE @intPackageId int;	
		DECLARE @prevChargeUnits INT;
		DECLARE @prevChargeRate INT;
		DECLARE @prevFromDate Date;
		DECLARE @prevToDate Date;
		DECLARE @prevDirectFlag BIT;
		DECLARE @prevNote VARCHAR(5120);
		DECLARE @prevSpecialRequestNote VARCHAR(5120);
		DECLARE @prevPriceItem INT;
		DECLARE @matchupRelatedChange BIT;
		EXEC APTTUS.GetSchoolId @txtSchoolCode,@source,@intSchoolId OUT;
		EXEC APTTUS.GetProductCode @ProductName,@source,null,null,null,null,null,null,null,null, @txtCode OUT;	
		select @txtcode;
		DECLARE @query nvarchar(max);
		SET @query = '
		SELECT  @intPackageId' + char(61) + ' [intPackageId], @txtPackageName'  + CHAR(61) + ' txtName
		FROM  [' + @source+ '].[dbo].[tblPackage]
		WHERE [txtCode] = '+ CHAR(39) + @txtCode + CHAR(39) +
		' AND blnActive = 1 AND intSchoolId = ' +  CAST(@intSchoolId AS VARCHAR(4));
		EXEC sp_executesql @query, N'@intPackageId INT OUT,@txtPackageName VARCHAR(200) OUT', @intPackageId OUT, @txtPackageName OUT;
		--select @intPackageId,@txtPackageName;
		IF  @intPackageId IS NULL
		BEGIN
			THROW 50000, 'No Item mapped for this Package!', 1
		END
		DECLARE @sql nvarchar(max);
		SET NOCOUNT ON; 
		DECLARE @Rowcount INT;
        DECLARE @dteCreatedDateTime DATETIME;
		SELECT @dteCreatedDateTime = getdate();
        DECLARE @intCreatedUserId INT = 9999;
        DECLARE @dteCreationDate DATE;
		SELECT @dteCreationDate = CONVERT(DATE, getdate());
        DECLARE @intAmendedUserId INT = 9999;
        DECLARE @dteAmendedDateTime DATETIME = getdate();
		DECLARE @intNumberOfUnits INT ; 
		SELECT @intNumberOfUnits = CAST(CAST( CEILING(CONVERT(FLOAT,DATEDIFF(dd,@dteFromDate,@dteToDate))/7 )  AS DECIMAL) AS INT);
		DECLARE @intNumberOfPartUnits INT;
		SELECT @intNumberOfPartUnits = CAST(CAST( DATEDIFF(dd,@dteFromDate,@dteToDate)%7    AS DECIMAL) AS INT);

		select @intEPackageId = intEPackageId
		from APTTUS.EPackage_Mapping 
		where txtExternalRef = @txtExternalRef or txtExternalRef = @txtExternalRefDerivedFrom;
		--select @intEPackageId as intepackageidref,@txtExternalRef,@txtExternalRefDerivedFrom
		--select @txtExternalRef;
		--SET @sql = 'SELECT @intEPackageId = intEPackageId,
		--FROM [' +@source+ '].[dbo].[tblEPackage]
		--WHERE txtExternalRef = @txtExternalrefDerivedFrom
		--OR txtExternalRef = @txtExternalref';
		--EXEC sp_executesql @sql,N'intEPackageId int output, @txtExternalrefDerivedFrom varchar(30),@txtExternalref varchar(30)',
		--@intEPackageId output,@txtExternalrefDerivedFrom,@txtExternalref; 
		IF (ISNULL(@intEPackageId,0) != 0)
		BEGIN
			SET @sql = '
			select ''inside update'';
			UPDATE [' +@source+ '].[dbo].[tblEPackage]
			SET 
			dteFromDate = @dteFromDate,
			dteToDate = @dteToDate,
			intPackageId = @intPackageId,
			intNumberOfUnits  = @intNumberOfUnits,
			intNumberOfPartUnits = @intNumberOfPartUnits,
			txtCode = @txtCode,
			txtNarrative = @txtPackageName,
			intEPackageStatusId = CAST(@intEPackageStatusId AS INT),
			dteAmendedDateTime = @dteAmendedDateTime,
			intAmendedUserId = @intAmendedUserId
		    WHERE intEPackageId = @intEPackageId
			AND intEPackageStatusId != 200;
			
			IF (@@ROWCOUNT = 0)
			BEGIN
				PRINT ERROR_MESSAGE();
				RAISERROR(''No EPackage Updated'',16, 1);
		    END
			
			'
			EXECUTE sp_executesql @sql, 	N'@dteCreatedDateTime DATETIME,@intCreatedUserId INT,@dteCreationDate DATE,@intAmendedUserId INT,
@dteAmendedDateTime DATETIME,@intNumberOfUnits INT,@intNumberOfPartUnits INT,@source  varchar(20),
@intEnrolId varchar(10),@intSchoolId INT,@intPackageId INT,@intEPackageStatusId VARCHAR(10),
@blndirect varchar(1),	@txtCode VARCHAR(25),@dteFromDate DATETIME,@dteToDate DATETIME,@quantity varchar(10),
@txtNote varchar(5120),@txtExternalRef varchar(30),@txtExternalRefDerivedFrom varchar(30),@txtPackageName VARCHAR(200),  @intEPackageId INT OUTPUT',
@dteCreatedDateTime,@intCreatedUserId,@dteCreationDate,@intAmendedUserId 	,	
@dteAmendedDateTime,@intNumberOfUnits,@intNumberOfPartUnits,@source, 
@intEnrolId,@intSchoolId,@intPackageId,@intEPackageStatusId,
@blnDirect,@txtCode,@dteFromDate,@dteToDate,@quantity,
@txtNote,@txtExternalRef,@txtExternalRefDerivedFrom, @txtPackageName , @intEPackageId OUTPUT;

		--SELECT @intEPackageId = CASE WHEN IDENT_CURRENT('[' + @source + '].[dbo].[tblEPackage]') IS NOT NULL THEN IDENT_CURRENT('[' + @source + '].[dbo].[tblEPackage]') ELSE @intEPackageId END;
		update apttus.EPackage_Mapping set txtExternalRef =  @txtExternalRef, DateModified = GetDate() where  txtExternalRef = @txtExternalRef or txtExternalRef = @txtExternalRefDerivedFrom;
		END
		ELSE IF (@intEPackageStatusId != '200')
		BEGIN
		select @txtExternalRef as insideinsert
		SET @sql = '	INSERT INTO ['+@source+'].[dbo].[tblEPackage]
			(
				intEnrolId,
				intPackageId,
				intEPackageStatusId,
				dteCreationDate,
				blnDirect,
				txtCode,
				dteFromDate,
				dteToDate,
				intNumberOfUnits,
				intNumberOfPartUnits,
				txtNote,
				dteCreatedDateTime,
				intCreatedUserId,
				dteAmendedDateTime,
				intAmendedUserId,
				txtNarrative
			)
			VALUES
			(
				CAST(@intEnrolId AS INT),
				@intPackageId,
				CAST(@intEPackageStatusId AS INT),
				@dteCreationDate,
				CAST(@blnDirect AS BIT),
				@txtCode,
				@dteFromDate,
				@dteToDate,
				@intNumberOfUnits,
			    @intNumberOfPartUnits,
				@txtNote,
				@dteCreatedDateTime,
				@intCreatedUserId,
				@dteAmendedDateTime,
				@intAmendedUserId,
				@txtPackageName
				
			);
		
		IF (@@ROWCOUNT = 0)
		BEGIN
			PRINT ERROR_MESSAGE();
			RAISERROR(''No EPackage inserted'',16, 1);
			
		END
		
		SELECT @intEPackageId = SCOPE_IDENTITY()
		
		insert into apttus.EPackage_Mapping (intEPackageId,txtexternalRef,DateCreated,DateModified)
		values (@intEPackageId,@txtExternalRef,getdate(),getdate())';
		EXECUTE sp_executesql @sql, 	N'@dteCreatedDateTime DATETIME,@intCreatedUserId INT,@dteCreationDate DATE,@intAmendedUserId INT,
@dteAmendedDateTime DATETIME,@intNumberOfUnits INT,@intNumberOfPartUnits INT,@source varchar(20),
@intEnrolId varchar(10),@intPackageId INT,@intEPackageStatusId VARCHAR(10), 
@blndirect varchar(1),@txtCode VARCHAR(25),@dteFromDate DATETIME,@dteToDate DATETIME,
@quantity varchar(10),@txtNote varchar(5120),@txtExternalRef varchar(30),@txtPackageName VARCHAR(200),@intEPackageId INT OUTPUT',
@dteCreatedDateTime,@intCreatedUserId,@dteCreationDate,@intAmendedUserId,
@dteAmendedDateTime,@intNumberOfUnits,@intNumberOfPartUnits,@source,
@intEnrolId,@intPackageId,@intEPackageStatusId, 
@blnDirect,@txtCode,@dteFromDate,@dteToDate, 
@quantity, @txtNote,@txtExternalRef,@txtPackageName,@intEPackageId OUTPUT;
		END	
select @intEPackageId as intepackageid
select @txtPackageCode =  @txtcode
RETURN 0
END

