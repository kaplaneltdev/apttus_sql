USE [ClassIntegration]
GO
/****** Object:  StoredProcedure [APTTUS].[UpsertEnrolment]    Script Date: 18/11/2020 12:33:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [APTTUS].[UpsertEnrolment] 
@channel varchar(1),
@source varchar(30),
@intStudentId varchar(10),
@txtCRMOppoReference       varchar(30),
@txtCode      varchar(30),
@txtEnrolBookingStatus     varchar(30),
@txtVisaType varchar(50),
@txtEnrolArrivalStatus varchar(30),
@txtGroupExternalRef      varchar(30),
@txtSchoolCode       varchar(10),
@txtMainAgentCode    varchar(45),
@txtCommissionAgentCode    varchar(45),
@txtStudentAgentCode       varchar(45),
@dteFromDate date,
@dteToDate date,
@txtAnalysis6 varchar(30),
@decAnalysis7 varchar(10),
@txtNote1 varchar(5120),
@txtNote2 varchar(5120),
@cancellationReason varchar(30),
@txtInvoiceComment varchar(5120),
@currencyCode varchar(3),
@stopSync varchar(10),
@visaNumber varchar(40),
@sponsorshipNumber varchar(30),
@extension varchar(30),
@intEnrolId_OUT int OUTPUT



AS
BEGIN
DECLARE @intGroupId int;
DECLARE @sql nvarchar(max);
DECLARE @intCollectionMethodId int = 100;
DECLARE @intModuleId int = 1500;
DECLARE @intSchoolId int;

DECLARE @AlpadiaFlag BIT;

--SELECT @txtCode = 'AP' + @txtCode;
SET @sql = '
SELECT @intSchoolId = intSchoolId,
@AlpadiaFlag = case when txtname like ''%alpadia%'' then 1 else 0 end
FROM [' +@source+ '].[dbo].tblSchool 
WHERE txtCode = @txtSchoolCode;';
EXECUTE sp_executesql @sql,N'@txtSchoolCode     varchar(10),@intSchoolId int out,@AlpadiaFlag bit out',@txtSchoolCode,@intSchoolId out,@AlpadiaFlag out;
DECLARE @intEnrolId  int;

select @AlpadiaFlag as 'flag'
SET @sql = '
SELECT @intEnrolId = intEnrolId
FROM [' +@source+ '].[dbo].[tblEnrol]
WHERE txtExternalRef = @txtCRMOppoReference or txtExternalRef = @txtCode AND intCreatedUserId = 9999;'
EXECUTE sp_executesql @sql,N' @intEnrolId int  output, @txtCRMOppoReference varchar(30), @txtCode varchar(30)', @intEnrolId OUT, @txtCRMOppoReference, @txtCode;

DECLARE @intEnrolBookingStatusId int;
SET @sql = 'SELECT @intEnrolBookingStatusId = intEnrolBookingStatusId
FROM [' +@source+ '].[dbo].tblEnrolBookingStatus
WHERE txtName = @txtEnrolBookingStatus;';
EXECUTE sp_executesql @sql,N' @txtEnrolBookingStatus varchar(30), @intEnrolBookingStatusId int out',@txtEnrolBookingStatus,@intEnrolBookingStatusId out;


DECLARE @intEnrolArrivalStatusId int;
SET @sql = 'SELECT @intEnrolArrivalStatusId = intEnrolArrivalStatusId
FROM [' +@source+ '].[dbo].tblEnrolArrivalStatus
WHERE txtName = @txtEnrolArrivalStatus;';
EXECUTE sp_executesql @sql,N' @txtEnrolArrivalStatus varchar(30), @intEnrolArrivalStatusId int out',@txtEnrolArrivalStatus, @intEnrolArrivalStatusId out;

--DECLARE @intGroupId int;
SET @sql = 'SELECT @intGroupId = intGroupId
FROM classintegration.apttus.group_mapping
WHERE txtExternalRef = substring(@txtGroupExternalRef,0,19)';
EXECUTE sp_executesql @sql,N'@txtGroupExternalRef varchar(30), @intGroupId int out',@txtGroupExternalRef,@intGroupId out;


DECLARE  @currency_code varchar(3);
SET @sql = 'SELECT @currency_code = CASE WHEN @source = ''CFW_ASPECT_GBR'' AND @currencyCode = ''EUR'' THEN ''EUR''
                                                WHEN @source = ''CFW_ASPECT_IRL'' THEN @currencyCode
												WHEN @source = ''CFW_ASPECT_AUS'' THEN ''AUD''
                                                WHEN @source = ''CFW_ASPECT_CAN'' THEN ''CAD''
                                                WHEN @source = ''CFW_ASPECT_GBR'' THEN ''GBP''
                                                WHEN @source = ''CFW_ASPECT_NZL'' THEN ''NZD''
                                                WHEN @source = ''CFW_ASPECT_USA'' THEN ''USD''
                                         END;';
EXECUTE sp_executesql @sql,N'@source varchar(30), @currencyCode varchar(3),@currency_code varchar(3) out',@source, @currencyCode, @currency_code out;


DECLARE @intAgentCurrencyId int;
DECLARE @intStudentCurrencyId int;
DECLARE @intMemoCurrencyId int;
SET @sql = 'SELECT @intstudentCurrencyId = intCurrencyId,
@intAgentCurrencyId = intCurrencyId,
@intMemoCurrencyId = intCurrencyId
FROM [' +@source+ '].[dbo].tblCurrency
WHERE txtCode = @currency_code;';
EXECUTE sp_executesql @sql,N'@intStudentCurrencyId int out, @intAgentCurrencyId int out, @intMemoCurrencyId int out,@currency_code varchar(3)', @intStudentCurrencyId out,@intAgentCurrencyId out,@intMemoCurrencyId out, @currency_code;


DECLARE @intStudentAgentId int;
SET @sql = '
SELECT @intStudentAgentId = intAgentId
FROM [' +@source+ '].[dbo].tblAgent
WHERE txtCode = @txtStudentAgentCode
AND intAgentStatusId = 200
AND intAgentcurrencyId = @intAgentCurrencyId
AND ((@AlpadiaFlag = 1 and @source = ''CFW_ASPECT_IRL'' AND @currency_code = ''EUR'' and txtTitle = ''ALPADIA'') 
OR ( @AlpadiaFlag = 1 and @source = ''CFW_ASPECT_IRL'' AND @currency_code != ''EUR'' and txtTitle != ''ALPADIA'') 
OR ( @AlpadiaFlag = 0 and txtTitle != ''ALPADIA'')
);';
EXECUTE sp_executesql @sql,N'@intAgentCurrencyId int, @txtStudentAgentCode varchar(45), @intStudentAgentId int out,@source varchar(30),@currency_code varchar(3),@AlpadiaFlag bit',@intAgentCurrencyId, @txtStudentAgentCode, @intStudentAgentId out,@source ,@currency_code,@AlpadiaFlag;

DECLARE @intMainAgentId int;
SET @sql = 'SELECT @intMainAgentId = intAgentId
FROM [' +@source+ '].[dbo].tblAgent
WHERE txtCode = @txtMainAgentCode
AND intAgentStatusId = 200
AND intAgentcurrencyId = @intAgentCurrencyId
AND ((@AlpadiaFlag = 1 and @source = ''CFW_ASPECT_IRL'' AND @currency_code = ''EUR'' and txtTitle = ''ALPADIA'') 
OR ( @AlpadiaFlag = 1 and @source = ''CFW_ASPECT_IRL'' AND @currency_code != ''EUR'' and txtTitle != ''ALPADIA'') 
OR ( @AlpadiaFlag = 0 and txtTitle != ''ALPADIA'')
);';
EXECUTE sp_executesql @sql,N'@intAgentCurrencyId int, @txtMainAgentCode varchar(45),@intMainAgentId int out,@source varchar(30),@currency_code varchar(3),@AlpadiaFlag bit', @intAgentCurrencyId, @txtMainAgentCode, @intMainAgentId out,@source ,@currency_code,@AlpadiaFlag;

DECLARE @intCommissionAgentId int;
SET @sql = 'SELECT @intCommissionAgentId = intAgentId 
FROM [' +@source+ '].[dbo].tblAgent 
WHERE txtCode = @txtCommissionAgentCode
AND intAgentStatusId = 200
AND intAgentcurrencyId = @intAgentCurrencyId
AND ((@AlpadiaFlag = 1 and @source = ''CFW_ASPECT_IRL'' AND @currency_code = ''EUR'' and txtTitle = ''ALPADIA'') 
OR ( @AlpadiaFlag = 1 and @source = ''CFW_ASPECT_IRL'' AND @currency_code != ''EUR'' and txtTitle != ''ALPADIA'') 
OR ( @AlpadiaFlag = 0 and txtTitle != ''ALPADIA'')
)
;';
EXECUTE sp_executesql @sql,N'@intAgentCurrencyId int, @txtCommissionAgentCode varchar(45),@intCommissionAgentId int out,@source varchar(30),@currency_code varchar(3),@AlpadiaFlag bit', @intAgentCurrencyId, @txtCommissionAgentCode, @intCommissionAgentId out,@source ,@currency_code,@AlpadiaFlag;
SELECT @intCommissionAgentId = CASE WHEN @intCommissionAgentId IS NULL THEN @intMainAgentId ELSE @intCommissionAgentId END;
DECLARE @intAgentPriceListId int = 1000;
DECLARE @intStudentPriceListId int = 1000;

DECLARE @intVisaTypeId int = 1000;
SET @sql = 'SELECT @intVisaTypeId = intVisaTypeId
FROM [' +@source+ '].[dbo].tblVisaType 
WHERE txtName = @txtVisaType;';
EXECUTE sp_executesql @sql,N'@txtVisaType VARCHAR(50), @intVisaTypeId int out',@txtVisaType, @intVisaTypeId out;

DECLARE @intDurationWeeks INT;
SET @sql = 'SELECT @intDurationWeeks = DATEDIFF(dd,@dteFromDate,@dteToDate)/7 ; ';
EXECUTE sp_executesql @sql,N'@dteFromDate date,@dteToDate date,@intDurationWeeks  int out',@dteFromDate,@dteToDate,@intDurationWeeks out;

DECLARE @intDurationDays INT;
SET @sql = 'SELECT @intDurationDays = DATEDIFF(dd,@dteFromDate,@dteToDate)%7;';
EXECUTE sp_executesql @sql,N'@dteFromDate date,@dteToDate date,@intDurationDays int out',@dteFromDate,@dteToDate,@intDurationDays out;

DECLARE @blnContinuousBooking BIT;
SET @sql = 'SELECT @blnContinuousBooking =   CASE WHEN  EXISTS(      SELECT * from [' +@source+ '].[dbo].tblenrol e1 where e1.intStudentId = CAST(@intStudentId AS INT)
                                                                                                       AND (e1.txtExternalRef != @txtCRMOppoReference AND e1.txtCode != @txtCode ) AND e1.dteFromDate < getdate()
                                                                                                       AND e1.intEnrolBookingStatusId in (100,200) AND e1.intEnrolArrivalStatusId = 300 
                                                                                                       AND DATEDIFF(DD,e1.dteToDate,@dteFromDate) >= 0 
                                                                                                       AND DATEDIFF(DD,e1.dteToDate,@dteFromDate) <= 28) 
                                                                            THEN 1
                                                                          ELSE 0 
                                                        END;';
EXECUTE sp_executesql @sql,N'@intStudentId varchar(10),@txtCRMOppoReference varchar(30), @dteFromDate date, @blnContinuousBooking BIT OUT, @txtCode varchar(30)', @intStudentId,@txtCRMOppoReference,@dteFromDate,@blnContinuousBooking out,@txtCode;
DECLARE @blnReturningStudent BIT;
SET @sql='SELECT @blnReturningStudent = CASE WHEN  EXISTS(    SELECT * from [' +@source+ '].[dbo].tblenrol e1 where e1.intStudentId = CAST(@intStudentId AS INT)
                                                                                                       AND (e1.txtExternalRef != @txtCRMOppoReference AND e1.txtCode != @txtCode ) AND e1.dteFromDate < getdate()
                                                                                                       AND e1.intEnrolBookingStatusId in (100,200) AND e1.intEnrolArrivalStatusId = 300 
                                                                                                       AND  DATEDIFF(DD,e1.dteToDate,@dteFromDate) > 28) THEN 1
                                                                          ELSE 0 
                                                                     END;';
EXECUTE sp_executesql @sql,N'@intStudentId varchar(10),@txtCRMOppoReference varchar(30), @dteFromDate date, @blnReturningStudent BIT OUT, @txtCode  varchar(30)', @intStudentId,@txtCRMOppoReference,@dteFromDate,@blnReturningStudent out, @txtCode;
DECLARE @alreadyCancelled BIT = 0;
DECLARE @a  auditLog;
SET @sql = '




       SET NOCOUNT ON;
      

       IF (ISNULL(@intEnrolId,0) != 0)
       BEGIN
              UPDATE [' +@source+ '].[dbo].[tblEnrol]
              SET --txtexternalref = @txtCRMOppoReference,
              --txtcode = @txtCode,
              intEnrolBookingStatusId = CASE WHEN @intEnrolBookingStatusId = 100 AND intEnrolBookingStatusId IN (200,300) THEN intEnrolBookingStatusId
                                                         WHEN  @intEnrolBookingStatusId = 200 AND  intEnrolBookingStatusId = 300 THEN intEnrolBookingStatusId
                                                         ELSE @intEnrolBookingStatusId END ,
			  intEnrolArrivalStatusId = CASE WHEN intEnrolArrivalStatusId = 400 AND dteFromDate < @dteFromDate THEN @intEnrolArrivalStatusId
										ELSE intEnrolArrivalStatusId END,
              dteFromDate = CASE WHEN @stopSync = ''true'' THEN dteFromDate ELSE @dteFromDate END,
              dteToDate = CASE WHEN @stopSync = ''true'' THEN dteToDate ELSE  @dteToDate END,
              intMainAgentId = @intMainAgentId,
              intCommissionAgentId = @intCommissionAgentId,
              intStudentAgentId = @intStudentAgentId,
              intGroupId = @intGroupId,
              intSchoolId = @intSchoolId,
              intAgentCurrencyId = @intAgentCurrencyId,
              intStudentCurrencyId = @intStudentCurrencyId,
              intAgentPriceListId = @intAgentPriceListId,
              intStudentPriceListId = @intStudentPriceListId,
              intAmendedUserId = 9999,
              dteAmendedDateTime  = getdate(),
              txtInvoiceComment = CASE WHEN @txtInvoiceComment = txtInvoiceComment THEN txtInvoiceComment
                                                WHEN @txtInvoiceComment LIKE ''%[a-z0-9]%''  AND LEN(@txtInvoiceComment) <= 5120 THEN @txtInvoiceComment
                                                ELSE txtInvoiceComment
                                                END,
              txtAnalysis6 =             CASE WHEN @txtAnalysis6 = txtAnalysis6 THEN txtAnalysis6
                                                WHEN @txtAnalysis6 LIKE ''%[a-z0-9]%'' AND LEN(@txtAnalysis6) <= 30 THEN  @txtAnalysis6
                                                ELSE txtAnalysis6
                                                END,
                       dteAnalysis9 =  CASE WHEN @intEnrolBookingStatusId = 300  and @source   in ( ''CFW_ASPECT_USA'', ''CFW_ASPECT_CAN'') THEN cast( getdate() as date)   else dteAnalysis9 end,
       
              intVisaTypeId = @intVisaTypeId,
              intDurationWeeks = @intDurationWeeks,
              intDurationDays = @intDurationDays,
              dteInvoiceDueDate = @dteFromDate,
              txtNote1 = CASE WHEN @txtNote1 = txtNote1 THEN txtNote1
                                                WHEN @txtNote1 LIKE ''%[a-z0-9]%'' AND CHARINDEX(@txtNote1,txtNote1) > 0 THEN txtNote1
                                                WHEN @txtNote1 LIKE ''%[a-z0-9]%'' AND LEN(@txtNote1) <= 5120 THEN @txtNote1
                                                ELSE txtNote1
                                                END,
              txtNote2 = CASE WHEN @txtNote2 = txtNote2 THEN txtNote2
                                                WHEN @txtNote2 LIKE ''%[a-z0-9]%'' AND LEN(@txtNote2) <= 5120  THEN @txtNote2
                                                ELSE txtNote2
                                                END, ' + CASE WHEN @source = 'CFW_ASPECT_NZL' OR @source = 'CFW_ASPECT_AUS' THEN 'txtAnalysis5 = 
                                                CASE WHEN @cancellationReason = txtAnalysis5 THEN txtAnalysis5
                                                       WHEN @cancellationReason LIKE ''%[a-z0-9]%'' AND LEN(@cancellationReason) <= 30 THEN  @cancellationReason
                                                       ELSE txtAnalysis5
                                                END '
                                                ELSE 'txtAnalysis4 = 
                                                CASE WHEN @cancellationReason = txtAnalysis4 THEN txtAnalysis4
                                                       WHEN @cancellationReason LIKE ''%[a-z0-9]%'' AND LEN(@cancellationReason) <= 30 THEN  @cancellationReason
                                                       ELSE txtAnalysis4
                                                END  ' END + ',
              blnAgentCreditAndReInvoice = 1 ,
              blnStudentCreditAndReInvoice = 1,
			  txtVisaNumber = CASE WHEN @visaNumber = txtVisaNumber THEN @visaNumber
										WHEN @visaNumber like ''%[a-z0-9]%'' THEN @visaNumber
										ELSE txtVisaNumber END,
			  txtSponsorshipNumber = CASE WHEN @sponsorshipNumber = txtSponsorshipNumber THEN @sponsorshipNumber
										WHEN @sponsorshipNumber like ''%[a-z0-9]%'' THEN @sponsorshipNumber
										ELSE txtSponsorshipNumber END, 
              dteSponsorshipExpiryDate = CASE WHEN @source NOT IN (''CFW_ASPECT_GBR'' ,''CFW_ASPECT_IRL'' ) THEN @dteFromDate
										 ELSE NULL END,
			  txtAnalysis3 = CASE WHEN @extension = ''true'' THEN ''Yes''
							 WHEN @extension = ''false'' THEN ''No''
							 ELSE ''''
							 END,
			  blnFinanceHeld = CASE WHEN (blnFinanceHeld = 1 AND dtefromdate < @dteFromDate AND @intEnrolBookingStatusId != 300  ) 
										OR (blnFinanceHeld = 1 AND @intEnrolBookingStatusId = 300 AND intEnrolBookingStatusId != 300 ) THEN 0
							   ELSE blnFinanceHeld END
							   
              WHERE intEnrolId = @intEnrolid
              AND intEnrolBookingStatusId != 300;
                           
              IF (@@ROWCOUNT = 0)
              BEGIN
                     IF (@intEnrolBookingStatusId = 300)
                           select @alreadyCancelled = 1;
                     
                     ELSE 
                           RAISERROR(''No Enrolment updated!'', 16, 1)
              END
              UPDATE [' +@source+ '].[dbo].[tblBookingTotal]
              SET
                     intMainAgentId = @intMainAgentId,
                     intSchoolId =  @intSchoolId,
                     intStudentCurrencyId = @intStudentCurrencyId,
                     intAgentCurrencyId = @intAgentCurrencyId
              WHERE intRecordId  = @intEnrolId

       END ' ;
       EXECUTE sp_executesql @sql,N'@channel varchar(1), @source varchar(30), @intStudentId varchar(10), @txtCRMOppoReference varchar(30), @txtCode       varchar(30),
       @txtEnrolBookingStatus     varchar(30), @txtVisaType varchar(50), @txtEnrolArrivalStatus varchar(30), @txtGroupExternalRef varchar(30),
       @txtSchoolCode varchar(10), @txtMainAgentCode   varchar(45), @txtCommissionAgentCode varchar(45), @txtStudentAgentCode varchar(45), 
       @dteFromDate date, @dteToDate date, @txtAnalysis6 varchar(30), @decAnalysis7 decimal(18,0), @txtNote1 VARCHAR(5120), @txtNote2 VARCHAR(5120), @cancellationReason varchar(30), @intCollectionMethodId int,@intModuleId int,@intSchoolId int, @intEnrolBookingStatusId int, @intEnrolArrivalStatusId int,@intGroupId int ,@currency_code varchar(3),
@intAgentCurrencyId int,@intStudentCurrencyId int,@intMemoCurrencyId int,@intStudentAgentId int,@intMainAgentId int,@intCommissionAgentId int,
@intAgentPriceListId int,@intStudentPriceListId int, @intVisaTypeId int,@intDurationWeeks INT,@intDurationDays INT,@blnContinuousBooking BIT,@blnReturningStudent BIT,@txtInvoiceComment varchar(5120), @intEnrolId int OUTPUT, @alreadyCancelled BIT OUTPUT,@visaNumber varchar(40),
@sponsorshipNumber varchar(30),@stopSync VARCHAR(10),@extension VARCHAR(30) ',@channel, @source, @intStudentId, @txtCRMOppoReference, @txtCode,
       @txtEnrolBookingStatus, @txtVisaType, @txtEnrolArrivalStatus , @txtGroupExternalRef,
       @txtSchoolCode, @txtMainAgentCode, @txtCommissionAgentCode, @txtStudentAgentCode,
       @dteFromDate, @dteToDate, @txtAnalysis6, @decAnalysis7, @txtNote1, @txtNote2, @cancellationReason, @intCollectionMethodId,@intModuleId ,@intSchoolId , @intEnrolBookingStatusId , @intEnrolArrivalStatusId ,@intGroupId ,@currency_code ,
@intAgentCurrencyId ,@intStudentCurrencyId ,@intMemoCurrencyId ,@intStudentAgentId ,@intMainAgentId ,@intCommissionAgentId ,
@intAgentPriceListId ,@intStudentPriceListId , @intVisaTypeId ,@intDurationWeeks ,@intDurationDays ,@blnContinuousBooking ,@blnReturningStudent ,@txtInvoiceComment, @intEnrolId    OUTPUT, @alreadyCancelled OUTPUT,@visaNumber ,
@sponsorshipNumber , @stopSync,@extension;
       SELECT @intEnrolId_OUT = @intEnrolId;

       
       IF (ISNULL(@intEnrolId,0) = 0)
       BEGIN
SET @sql ='   
              
              INSERT INTO [' +@source+ '].[dbo].[tblEnrol](
			  intGroupId,
              intCreatedUserId,
              dteCreatedDateTime,
              intAmendedUserId,
              dteAmendedDateTime,
              txtexternalref,
              intModuleId,
              intStudentId,
              txtcode,
              intEnrolBookingStatusId,
              intEnrolArrivalStatusId,
              dteFromDate,
              dteToDate,
              intMainAgentId,
              intCommissionAgentId,
              intStudentAgentId,
              intSchoolId,
              intAgentCurrencyId,
              intStudentCurrencyId,
              intMemoCurrencyId,
              intAgentPriceListId,
              intStudentPriceListId,
              intVisaTypeId,
              dteEnrolledDate,
              dteInvoiceDueDate,
              txtInvoiceComment,
              txtNote1,
              txtNote2,
              txtNote3,
              txtAnalysis1,
              txtAnalysis2,
              txtAnalysis3,' 
              + CASE WHEN @source = 'CFW_ASPECT_NZL' OR @source = 'CFW_ASPECT_AUS' THEN 'txtAnalysis4'
                                                              ELSE 'txtAnalysis5' ENd +
              ',txtAnalysis6,
              decAnalysis7,
              intCollectionMethodId,
              intDurationWeeks,
              intDurationDays,
              blnContinuousBooking,
              blnReturnStudent,
              blnDirectStudent, ' + CASE WHEN @source = 'CFW_ASPECT_NZL' OR @source = 'CFW_ASPECT_AUS' THEN 'txtAnalysis5'
                                                              ELSE 'txtAnalysis4' ENd + ' ,
           blnAgentCreditAndReInvoice,
              blnStudentCreditAndReInvoice,
              blnVisaStudentAbsenceMandatory,
			   txtVisaNumber  ,
			  txtSponsorshipNumber ,
			  dteSponsorshipExpiryDate
                     ) 
              VALUES(
              @intGroupId,
			  9999,
              GETDATE(),
              9999,
              GETDATE(),
              @txtcode,
              @intModuleId,
              CAST(@intStudentId AS INT),
              @txtcode,
              @intEnrolBookingStatusId,
              @intEnrolArrivalStatusId,
              @dteFromDate,
              @dteToDate,
              @intMainAgentId,
              @intCommissionAgentId,
              @intStudentAgentId,
              @intSchoolId,
              @intAgentCurrencyId,
              @intStudentCurrencyId,
              @intMemoCurrencyId,
              @intAgentPriceListId,
              @intStudentPriceListId,
              @intVisaTypeId,
              cast(getdate() as date),
              @dteFromDate,
              @txtInvoiceComment,
              @txtNote1,
              @txtNote2,
              '' '',
              '' '',
              '' '',
              CASE WHEN @extension = ''true'' THEN ''Yes''
							 WHEN @extension = ''false'' THEN ''No''
							 ELSE ''''
							 END,
              '' '',
              @txtAnalysis6,
              CAST(@decAnalysis7 AS  decimal(18,0)),
              @intCollectionMethodId,
              @intDurationWeeks,
              @intDurationDays,
              @blnContinuousBooking,
              @blnReturningStudent,
              CAST(@channel AS BIT),
              @cancellationReason,
              1,
              1,
              1,
			  @visaNumber ,
			  @sponsorshipNumber  ,
			  @dteFromDate
              );
              IF(@@ROWCOUNT != 1)
                     RAISERROR(''No Enrolment inserted!'', 16, 1)
              select @intEnrolId_OUT = SCOPE_IDENTITY();
              INSERT INTO [' +@source+ '].[dbo].[tblBookingTotal]
              (
                     intMainAgentId,
                     intStudentId,
                     intSchoolId,
                     intRecordId,
                     intStudentCurrencyId,
                     intAgentCurrencyId
              )
              VALUES
              (
                     @intMainAgentId,
                     @intStudentId,
                     @intSchoolId,
                     @intEnrolId_OUT,
                     @intStudentCurrencyId,
                     @intAgentCurrencyId
              )
       '
       ;
       
       EXECUTE sp_executesql @sql,N'@intGroupId int , @channel varchar(1), @source varchar(30), @intStudentId varchar(10), @txtCRMOppoReference varchar(30), @txtCode       varchar(30),
       @txtEnrolBookingStatus     varchar(30), @txtVisaType varchar(50), @txtEnrolArrivalStatus varchar(30), @txtGroupExternalRef varchar(30),
       @txtSchoolCode varchar(10), @txtMainAgentCode   varchar(45), @txtCommissionAgentCode varchar(45), @txtStudentAgentCode varchar(45), 
       @dteFromDate date, @dteToDate date, @txtAnalysis6 varchar(30), @decAnalysis7 decimal(18,0), @txtNote1 VARCHAR(5120), @txtNote2 VARCHAR(5120), @cancellationReason varchar(30), @intCollectionMethodId int,@intModuleId int,@intSchoolId int, @intEnrolBookingStatusId int, @intEnrolArrivalStatusId int,@currency_code varchar(3),
       @intAgentCurrencyId int,@intStudentCurrencyId int,@intMemoCurrencyId int,@intStudentAgentId int,@intMainAgentId int,@intCommissionAgentId int,
       @intAgentPriceListId int,@intStudentPriceListId int, @intVisaTypeId int,@intDurationWeeks INT,@intDurationDays INT,@blnContinuousBooking BIT,@blnReturningStudent BIT, @txtInvoiceComment VARCHAR(5120), @intEnrolId int OUTPUT,@visaNumber varchar(40),
@sponsorshipNumber varchar(30), @extension VARCHAR(30),@intEnrolId_OUT INT output ',@intGroupId ,@channel, @source, @intStudentId, @txtCRMOppoReference, @txtCode,
       @txtEnrolBookingStatus, @txtVisaType, @txtEnrolArrivalStatus , @txtGroupExternalRef,
       @txtSchoolCode, @txtMainAgentCode, @txtCommissionAgentCode, @txtStudentAgentCode,
       @dteFromDate, @dteToDate, @txtAnalysis6, @decAnalysis7, @txtNote1, @txtNote2, @cancellationReason, @intCollectionMethodId,@intModuleId ,@intSchoolId , @intEnrolBookingStatusId , @intEnrolArrivalStatusId  ,@currency_code ,
       @intAgentCurrencyId ,@intStudentCurrencyId ,@intMemoCurrencyId ,@intStudentAgentId ,@intMainAgentId ,@intCommissionAgentId ,
       @intAgentPriceListId ,@intStudentPriceListId , @intVisaTypeId ,@intDurationWeeks ,@intDurationDays ,@blnContinuousBooking ,@blnReturningStudent, @txtInvoiceComment, @intEnrolId  OUTPUT, @visaNumber ,
@sponsorshipNumber , @extension, @intEnrolId_OUT output;
       END
              --SELECT @intEnrolId_OUT =  CASE WHEN @@IDENTITY IS NOT NULL THEN @@IDENTITY ELSE @intEnrolId END;
              select @alreadyCancelled as '@alreadyCancelled'
       IF (@alreadyCancelled = 1)
              RETURN
    DECLARE @intNumberOfEnrolments int;
      -- SET @sql = 'SELECT @intNumberOfEnrolments = count(distinct intenrolid)
       --FROM [' +@source+ '].[dbo].tblEnrol
       --WHERE intGroupId  = @intGroupId';
       --EXECUTE sp_executesql @sql,N'@intGroupId int, @intNumberOfEnrolments int out',@intGroupId,@intNumberOfEnrolments out;

       --SET @sql = 'Update [' +@source+ '].[dbo].tblGroup set intNumberOfBookings = @intNumberOfEnrolments     ,intNumberOfArrivals = @intNumberOfEnrolments
       --WHERE intGroupId  = @intGroupId_OUT or intGroupId = @intGroupId';
      -- EXECUTE sp_executesql @sql,N'@intGroupId_OUT int,@intGroupId int,@intNumberOfEnrolments int',@intGroupId_OUT, @intGroupId,@intNumberOfEnrolments;

    select @intEnrolBookingStatusId as 'intEnrolBookingStatusId'
       If(@intEnrolBookingStatusId = 300)
                                  INSERT INTO @a (intPriceItemId,
                                                              intGroupId,
                                                              intEnrolId,
                                                              intEnrolBookingId,
                                                              intModuleId,
                                                              intInvoiceLineId,
                                                              intUserId,
                                                              intActionId,
                                                              dteTimestamp,
                                                              txtUsername,
                                                              decOldValue,
                                                              decNewValue,
                                                              decDifference
                                                              )
                                  values               (
                                                              -1,
                                                              -1,
                                                              CAST(@intEnrolId AS INT),
                                                              -1,
                                                              1500,
                                                              -1,
                                                              9999,
                                                              26,
                                                              getdate(),
                                                              'Apttus',
                                                              0,
                                                              0,
                                                              0
                                                              )      
                                                              SEt @sql = 'INSERT INTO [' +  @source + '].[dbo].[tblBookingAudit] ( intPriceItemId ,
       intGroupId  ,
       intEnrolId  ,
       intEnrolBookingId ,
       intModuleId  ,
       intInvoiceLineId ,
       intUserId  ,
       intActionId,
       dteTimestamp ,
       txtUsername  ,
       dteOldDateValue ,
       dteNewDatevalue  ,
       decOldValue  ,
       decNewValue  ,
       decDifference ,
       txtNote  ,
       txtComment ) 
       SELECT  intPriceItemId ,
       intGroupId  ,
       intEnrolId  ,
       intEnrolBookingId ,
       intModuleId  ,
       intInvoiceLineId ,
       intUserId  ,
       intActionId,
       dteTimestamp ,
       txtUsername  ,
       dteOldDateValue ,
       dteNewDatevalue  ,
       decOldValue  ,
       decNewValue  ,
       decDifference ,
       txtNote  ,
       txtComment  from @a';
       EXECUTE sp_executesql @sql,N'@a auditLog READONLY',@a
              
RETURN 0
END
