USE [ClassIntegration]
GO
/****** Object:  StoredProcedure [APTTUS].[CreateCreditInvoiceLine]    Script Date: 07/01/2020 15:52:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [APTTUS].[CreateCreditInvoiceLine] 

	@intEnrolId VARCHAR(10),
	@intRecordId VARCHAR(10),
	@intModuleId VARCHAR(10),
	@txtCode VARCHAR(25),
	@source VARCHAR(30),
	@txtNarrative VARCHAR(200),
	@intPriceItemId VARCHAR(10),
	@dteFromDate DATETIME,
	@dteToDate DATETIME,
	@curBasic MONEY,
	@curDue MONEY,
	@curMemoDue MONEY,
	@intDiscountPercent VARCHAR(10),
	@curDiscount MONEY,
	@txtInvoiceType CHAR(1),
	@intPriceListId VARCHAR(10),
	@intTaxId VARCHAR(10),
	@curTax MONEY,
	@blnDirect VARCHAR(1),
	@DebitInvoiceLineId VARCHAR(10)
	
AS

DECLARE @sql nvarchar(max);
	SET NOCOUNT ON; 
	DECLARE @duplicateFlag BIT;
	
	IF @intRecordId  IS NULL 
	BEGIN
		THROW 50000, 'No Booking Id passed to Credit Line Creation!', 1
	END
	IF  @intPriceItemId IS NULL
	BEGIN
		THROW 50000, 'No PriceItemId passed to Credit Line Creation!', 1
	END
	SET @sql = 'SELECT @duplicateFlag =  CASE WHEN ISNULL(a.cnt,0) >= d.cnt   THEN 1
	ELSE  0 END 
	FROM (select  count(intinvoicelineid) as cnt,intEnrolId,intModuleId,intRecordId FROM ' + @source + '.[dbo].[tblInvoiceLine] where   txtInvoiceType = ''D'' 
group by intEnrolId,intModuleId,intRecordId) d
left outer join (select  count(intinvoicelineid) as cnt,intEnrolId,intModuleId,intRecordId from
	 ' +  @source + '.[dbo].[tblInvoiceLine]  WHERE txtInvoiceType = ''C''  
	 group by intEnrolId,intModuleId,intRecordId) a on a.intEnrolId = d.intEnrolId and a.intModuleId = d.intModuleId and a.intRecordId = d.intRecordId
WHERE
	d.intEnrolId = CAST(@intEnrolId AS INT) AND
	d.intRecordId = CAST(@intRecordId AS INT)
	AND d.intModuleId = CAST(@intModuleId AS INT)
	';
	EXECUTE sp_executesql @sql,N'@source  VARCHAR(30),@intEnrolId  VARCHAR(10),@intRecordId VARCHAR(10),
	@intModuleId VARCHAR(10),@duplicateFlag BIT OUT',@source,@intEnrolId,@intRecordId,@intModuleId,@duplicateFlag OUT;
	SELECT @duplicateFlag;
	IF @duplicateFlag = 0
	BEGIN
	SET @sql = '
DECLARE @Rowcount INT;
DECLARE @intInvoiceHeaderId INT = NULL;
DECLARE @blnLive  BIT;

MERGE [' +@source+ '].[dbo].tblInvoiceLine AS target  
USING (SELECT i.intInvoiceLineId,
				CASE WHEN i.intInvoiceHeaderId IS NOT NULL AND  h.txtInvoiceType = ''D'' THEN 1
						ELSE 0
				END	AS Live,
				CASE WHEN i.intInvoiceHeaderId IS NULL THEN -1
							ELSE i.intInvoiceHeaderId
					END AS InvoiceHeaderId
		FROM [' +@source+ '].[dbo].tblInvoiceLine i
		LEFT OUTER JOIN .[' +@source+ '].[dbo].tblInvoiceHeader h on i.intInvoiceHeaderId = h.intInvoiceHeaderId
		WHERE i.intInvoiceLineId = @debitInvoiceLineId
		AND i.intEnrolId = @intEnrolId
		AND i.intRecordId = @intRecordId
		AND i.intModuleid = @intModuleId
		--AND i.intPriceItemId = @intPriceItemId
		--AND i.txtCode = @txtCode
		

) AS source (intInvoiceLineId, Live, InvoiceHeaderId)  
ON (target.intInvoiceLineId = source.intInvoiceLineId)  
WHEN MATCHED   
THEN UPDATE SET target.blnLive =  source.Live, target.intInvoiceHeaderId = source.invoiceHeaderId;
DECLARE @txtLanguageName VARCHAR(200);
DECLARE @intEPackageId INT;
DECLARE  @currency_code varchar(3);
DECLARE @intCurrencyId INT;
DECLARE @intCommissionAgentCurrencyId INT;
DECLARE @intMemoCurrencyId INT;
DECLARE @dteCreationDate DATE;
DECLARE @dteBookingDate DATE;
DECLARE @intNumberOfUnits INT;
DECLARE @intNumberOfPartUnits INT;
DECLARE @intCommissionPercent DECIMAL(9);
DECLARE @blnAlwaysCommission BIT;
DECLARE @curCommission MONEY; 
DECLARE @int2ndCommissionPercent DECIMAL(9);
DECLARE @cur2ndCommission MONEY;
DECLARE @blnPrinted BIT = 0; 
DECLARE @blnPosted BIT = 0;
DECLARE @decStatisticalWeeks DECIMAL(9,4);
DECLARE @memInvoicePriceCalculation VARCHAR(5120);
DECLARE @memMemoPriceCalculation VARCHAR(5120);
DECLARE @txtAmended CHAR(1);
DECLARE @blnNotRefinanced BIT;
DECLARE @intPromotionOriginPriceItemId int;
DECLARE @curCostPrice MONEY;
DECLARE @intCostAuthorisedUserId INT;

BEGIN


		SET NOCOUNT OFF;
		SELECT
		   @txtLanguageName 						   = txtLanguageName                   ,
		   @intEPackageId                              = intEPackageId                     ,
--		   @currency_code                              = currency_code                     ,
		   @intCurrencyId                              = i.intCurrencyId                     ,
		   @intCommissionAgentCurrencyId               = intCommissionAgentCurrencyId      ,
		   @intMemoCurrencyId                          = i.intMemoCurrencyId                 ,
		   @dteCreationDate                            = getdate()                   ,
		   @dteBookingDate                             = dteBookingDate                    ,
		   @intNumberOfUnits                           = intNumberOfUnits                  ,
		   @intNumberOfPartUnits                       = intNumberOfPartUnits               ,
		   @intCommissionPercent                       = intCommissionPercent              ,
		   @blnAlwaysCommission                        = blnAlwaysCommission               ,
		   @curCommission                              = curCommission                     ,
		   @int2ndCommissionPercent                    = int2ndCommissionPercent           ,
		   @cur2ndCommission                           = cur2ndCommission                  ,
 		   @decStatisticalWeeks                        = (decStatisticalWeeks * -1)              ,
		   @memInvoicePriceCalculation                 = memInvoicePriceCalculation        ,
		   @memMemoPriceCalculation                    = memMemoPriceCalculation           ,
		   @txtAmended                                 = txtAmended                        ,
		   @blnNotRefinanced                           = blnNotRefinanced                  ,
		   @intPromotionOriginPriceItemId              = intPromotionOriginPriceItemId     ,
		   @curCostPrice                               = curCostPrice                      ,
		   @intCostAuthorisedUserId                    = intCostAuthorisedUserId           ,
		   @txtCode									   = txtCode							,
		   @txtNarrative							   = txtNarrative						,
		   @intPriceItemId							   = intPriceItemId						,
		   @dteFromDate								   = dteFromDate						,
		   @dteToDate								   = dteToDate							,
		   @curBasic								   = curBasic						,
		   @curDue									   = curDue								,
		   @curMemoDue								   = curMemoDue								,
		   @intDiscountPercent						   = intDiscountPercent					,
		   @curDiscount								   = curDiscount							,
		   @txtInvoiceType							   = i.txtInvoiceType						,
		   @intPriceListId							   = intPriceListId						,
		   @intTaxId								   = intTaxId							,
		   @curTax									   = curTax							,
		   @blnDirect								   = i.blnDirect,
		   @blnLive =                                  CASE WHEN i.intInvoiceHeaderId > 0 AND i.blnLive = 1 AND h.txtInvoiceType = ''D'' THEN 1
														ELSE  0
														END
		FROM [' +@source+ '].[dbo].tblInvoiceLine i
		LEFT OUTER JOIN [' +@source+ '].[dbo].tblInvoiceHeader h on h.intInvoiceHeaderId = i.intInvoiceHeaderId 
		WHERE i.intInvoiceLineId = @debitInvoiceLineId
		AND i.intEnrolId = @intEnrolId
		AND i.intRecordId = @intRecordId
		AND i.intModuleid = @intModuleId
		--AND i.intPriceItemId = @intPriceItemId
		--AND i.txtCode = @txtCode;
		SELECT @txtInvoiceType = ''C'';
		INSERT INTO [' +@source+ '].[dbo].tblInvoiceLine
		(
			intEnrolId,
			intInvoiceHeaderId,
			txtInvoiceType,
			intModuleId,
			intRecordId,
			intPriceListId,
			intPriceItemId,
			intEPackageId,
			intCurrencyId,
			intCommissionAgentCurrencyId,
			intMemoCurrencyId,
			intTaxId,
			blnDirect,
			txtCode,
			txtNarrative,
			dteCreationDate,
			dteBookingDate,
			dteFromDate,
			dteToDate,
			intNumberOfUnits,
			intNumberOfPartUnits,
			curBasic,
			intDiscountPercent,
			curDiscount,
			curTax,
			curDue,
			curMemoDue,
			intCommissionPercent,
			blnAlwaysCommission,
			curCommission,
			int2ndCommissionPercent,
			cur2ndCommission,
			blnLive,
			blnPrinted,
			blnPosted,
			decStatisticalWeeks,
			memInvoicePriceCalculation,
			memMemoPriceCalculation,
			txtAmended,
			blnNotRefinanced,
			txtLanguageName,
			intPromotionOriginPriceItemId,
			curCostPrice,
			intCostAuthorisedUserId
	
		)
		VALUES
		(
			CAST (@intEnrolId AS int),
			@intInvoiceHeaderId,
			@txtInvoiceType,
			CAST(@intModuleId AS int),
			CAST (@intRecordId AS int),
			CAST(@intPriceListId AS int),
			CAST(@intPriceItemId AS int),
			@intEPackageId,
			@intCurrencyId,
			@intCommissionAgentCurrencyId,
			@intMemoCurrencyId,
			CAST(@intTaxId AS int),
			CAST(@blnDirect AS BIT),
			@txtCode,
			@txtNarrative,
			@dteCreationDate,
			@dteBookingDate,
			@dteFromDate,
			@dteToDate,
			@intNumberOfUnits,
			@intNumberOfPartUnits,
			@curBasic * -1,
			CAST(CAST(@intDiscountPercent AS  DECIMAL) AS int) ,
			CAST(@curDiscount AS  FLOAT)   * -1,
			CAST(@curTax AS  FLOAT)   * -1,
			CAST(@curDue AS  FLOAT)   * -1,
			CAST(@curMemoDue AS  FLOAT)  * -1,
			@intCommissionPercent,
			@blnAlwaysCommission,
			@curCommission,
			@int2ndCommissionPercent,
			@cur2ndCommission,
			@blnLive,
			@blnPrinted,
			@blnPosted,
			@decStatisticalWeeks,
			@memInvoicePriceCalculation,
			@memMemoPriceCalculation,
			@txtAmended,
			@blnNotRefinanced,
			@txtLanguageName,
			@intPromotionOriginPriceItemId,
			@curCostPrice,
			@intCostAuthorisedUserId
		)

		
		IF(@@ROWCOUNT != 1)
			RAISERROR(''No Invoice Line inserted!'', 16, 1)
		
		END';
		EXECUTE sp_executesql @sql,N'@intEnrolId VARCHAR(10), @intRecordId VARCHAR(10), @intModuleId VARCHAR(10), @txtCode VARCHAR(25),
	@source VARCHAR(30), @txtNarrative VARCHAR(200), @intPriceItemId VARCHAR(10), 
	@dteFromDate DATETIME, @dteToDate DATETIME, @curBasic MONEY, @curDue MONEY, @curMemoDue MONEY, @intDiscountPercent VARCHAR(10),
	@curDiscount MONEY, @txtInvoiceType CHAR(1), @intPriceListId VARCHAR(10), @intTaxId VARCHAR(10),
	@curTax MONEY, @blnDirect VARCHAR(1), @DebitInvoiceLineId VARCHAR(10)', @intEnrolId, @intRecordId, @intModuleId, @txtCode, @source, @txtNarrative,
@intPriceItemId, @dteFromdate,
@dteTodate, @curBasic, @curDue, @curMemoDue, @intDiscountPercent, @curDiscount,
@txtInvoiceType, @intPriceListId, @intTaxId, @curTax,@blnDirect, @DebitInvoiceLineId;
SELECT  SCOPE_IDENTITY() as 'invoicelineid';
	
		END

RETURN 0
