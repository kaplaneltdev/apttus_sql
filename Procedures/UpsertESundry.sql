USE [ClassIntegration]
GO
/****** Object:  StoredProcedure [APTTUS].[UpsertESundry]    Script Date: 18/11/2020 12:26:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [APTTUS].[UpsertESundry] 
	-- Add the parameters for the stored procedure here

	@source  varchar(20),
	@intEnrolId varchar(10),
	@txtSchoolCode varchar(3),
	@productName varchar(100),
	@txtESundryStatus varchar(30),
	@blndirect varchar(1),
	@txtCode VARCHAR(25),
	@txtNarrative VARCHAR(200),
	@dteFromDate DATE,
	@dteToDate DATE,
	@quantity varchar(10),
	--@txtNote varchar(5120),
	@txtExternalRef varchar(30),
	@txtExternalRefDerivedFrom varchar(30),
	@intESundryId_OUT INT OUTPUT,
	@txtPriceItemCode_OUT varchar(30) output,
	@intPriceItemId_OUT int output,
	@txtPriceItemName_OUT varchar(200) output


AS

BEGIN
		DECLARE @intSchoolId int;



		
		--DECLARE @txtNote varchar(5120);
		EXEC APTTUS.GetSchoolId @txtSchoolCode,@source,@intSchoolId OUT;
		IF @txtCode Not LIKE 'D%' AND @txtCode Not LIKE 'ALPD%'
			EXEC APTTUS.GetProductCode @ProductName,@source,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, @txtCode OUT;	

		

		DECLARE @intPriceItemId INT;
		DECLARE @query nvarchar(max);
		SET @query = '
		SELECT  @intPriceItemId' + char(61) + ' [intPriceItemId], @txtNarrative = txtName
		FROM  [' + @source+ '].[dbo].[tblPriceItem]
		WHERE [txtCode] = '+ CHAR(39) + @txtCode + CHAR(39) +
		' AND blnActive = 1 AND intSchoolId = ' +  CAST(@intSchoolId AS VARCHAR(4));
		
		EXEC sp_executesql @query, N'@intPriceItemId INT OUT,@txtNarrative varchar(200) out', @intPriceItemId OUT,@txtNarrative out;
		SELECT @txtPriceItemName_OUT = @txtNarrative
		


		SELECT @txtPriceItemCode_OUT = @txtCode,
		@intPriceItemId_OUT = @intPriceItemId;

     	DECLARE @intEPackageId int = NULL; 
		SET @query = 'SELECT @intEPackageId = intEPackageId
		FROM ['+@source+'].[dbo].[tblEPackage] EP
		INNER JOIN ['+@source+'].[dbo].[tblPackage] P on EP.intPackageId = P.intPackageId
		INNER JOIN  ['+@source+'].[dbo].[tblPackageItem] item on item.intPackageId = P.intPackageId AND item.intPriceItemId = @intPriceItemId and intModuleId = 2000 
		WHERE EP.intEnrolId = @intEnrolId'
		select @query;
		EXEC sp_executesql @query, N'@intEPackageId INT OUT,@intPriceItemId INT,@intEnrolId varchar(10)', @intEPackageId OUT,@intPriceItemId,@intEnrolId;

		
		IF  @intPriceItemId IS NULL
		BEGIN
			THROW 50000, 'No PriceItem mapped for this Sundry Booking!', 1
		END
		DECLARE @intESundryId INT;
		DECLARE @sql nvarchar(max);
		SET NOCOUNT ON; 
		
		DECLARE @operation varchar(1);	
		DECLARE @a  auditLog;
		DECLARE @prevChargeUnits INT;
		DECLARE @prevChargeRate INT;
		DECLARE @prevFromDate Date;
		DECLARE @prevToDate Date;
		DECLARE @prevDirectFlag BIT;
		DECLARE @intESundryStatusId INT;
		DECLARE @prevUnits INT;

		SET @sql = 'SELECT @intESundryStatusId  = intESundryStatusId
		FROM [' +@source+ '].[dbo].[tblESundryStatus]
		WHERE  txtName = @txtESundryStatus';
		EXEC sp_executesql @sql,N'@intESundryStatusId int out,@txtESundryStatus varchar(30)',@intESundryStatusId out,@txtESundryStatus;
		select @intESundryStatusId as 'status';
		DECLARE @Rowcount INT;

		DECLARE @dteCreatedDateTime DATETIME;
		SELECT @dteCreatedDateTime = getdate();

		DECLARE @intCreatedUserId INT = 9999;

		DECLARE @dteCreationDate DATE;
		SELECT @dteCreationDate = CONVERT(DATE, getdate());

		DECLARE @intAmendedUserId INT = 9999;

		DECLARE @dteAmendedDateTime DATETIME;
		SELECT @dteAmendedDateTime = getdate();
		
		DECLARE @intNumberOfUnits INT ; 
		SELECT @intNumberOfUnits = CAST(CAST(@quantity AS FLOAT) AS INT);
		--SELECT @intNumberOfUnits = CEILING(CONVERT(FLOAT,DATEDIFF(dd,@dteFromDate,@dteToDate))/7 );

		DECLARE @intNumberOfPartUnits INT;
		SELECT @intNumberOfPartUnits = 0;
		

		DECLARE @intChargeUnits INT;
		SELECT @intChargeUnits = CAST(CAST(@quantity AS FLOAT) AS INT);
		--SELECT @intChargeUnits = CEILING(CONVERT(FLOAT,DATEDIFF(dd,@dteFromDate,@dteToDate))/7 );

		DECLARE @intChargePartUnits INT = 0;
		

		DECLARE @intChargeRate INT;
		SELECT @intChargeRate = CAST(CAST(@quantity AS FLOAT) AS INT);
		--SELECT @intChargeRate = CEILING(CONVERT(FLOAT,DATEDIFF(dd,@dteFromDate,@dteToDate))/7 );
		DECLARE @alreadyCancelled BIT = 0;
		SET @query = 'SELECT @intESundryId = intESundryId,
		@prevChargeUnits = intChargeUnits,
		@prevChargeRate = intChargeRate,
		@prevFromDate = dteFromDate,
		@prevToDate = dteToDate, 
		@prevDirectFlag = blnDirect,
		@prevUnits = intNumberOfUnits 
		FROM [' +@source+ '].[dbo].[tblESundry]
		WHERE txtExternalRef = @txtExternalRefDerivedFrom OR
		txtExternalRef = @txtExternalRef';
		EXEC sp_executesql @query,N'@intESundryId int out, @prevChargeUnits int output,@prevChargeRate int output, @prevFromDate date output, @prevToDate date output, @prevDirectFlag BIT output,@prevUnits int output,@txtExternalRefDerivedFrom varchar(30),@txtExternalRef varchar(30)',@intESundryId out, @prevChargeUnits out,@prevChargeRate out,  @prevFromDate out, @prevToDate out, @prevDirectFlag out,@prevUnits out,@txtExternalRefDerivedFrom,@txtExternalRef;
		
		IF (ISNULL(@intESundryId,0) != 0)
		BEGIN
			SET @operation = 'U';
			set @sql = 'UPDATE [' +@source+ '].[dbo].[tblESundry]
			SET intESundryStatusId = @intESundryStatusId,
			intPriceItemId = @intPriceItemId,
			txtCode = @txtCode,
			txtNarrative = @txtNarrative,
			blnDirect = CAST(@blnDirect AS BIT),
			dteFromDate = CASE WHEN @dteFromDate = ''1900-01-01 00:00:00.000'' AND @intESundryStatusId = 200 THEN dteFromDate
						  ELSE @dteFromDate END,
			dteToDate = CASE WHEN @dteToDate = ''1900-01-01 00:00:00.000'' AND @intESundryStatusId = 200 THEN dteToDate
						  ELSE @dteToDate END,
            intNumberOfUnits = @intNumberOfUnits,
			intNumberOfPartUnits = @intNumberOfPartUnits,
			--txtNote = CASE WHEN txtNote = @txtNote THEN txtNote
			--			   WHEN @txtNote LIKE ''%[a-z0-9]%'' AND CHARINDEX(@txtNote,txtNote)> 0 THEN txtNote
			--			   WHEN @txtNote LIKE ''%[a-z0-9]%'' AND CHARINDEX(@txtNote,txtNote)= 0 THEN @txtNote
			--			   ELSE @txtNote
			-- 		  END,
			intAmendedUserId = @intAmendedUserId,
			dteAmendedDateTime = @dteAmendedDateTime,
			txtExternalRef = @txtExternalRef,
			intEPackageId = @intEPackageId
			WHERE intEsundryId = @intESundryId
			AND intESundryStatusId != 200 ;
			IF(@@ROWCOUNT != 1)
			BEGIN	
				RAISERROR(''No ESundry updated'',16, 1);
				 IF (@intESundryStatusId = 200)
					SELECT @alreadyCancelled = 1;
			END';
			EXEC sp_executesql @sql,N'@intESundryStatusId int,
@dteCreatedDateTime DATETIME,
@intCreatedUserId INT,
@dteCreationDate DATE,
@intAmendedUserId INT,
@dteAmendedDateTime DATETIME,
@intNumberOfUnits INT ,
@intNumberOfPartUnits INT,
@intChargeUnits INT,
@intChargePartUnits INT,
@intChargeRate INT, @source  varchar(20),
	@intEnrolId VARCHAR(10),
	@intSchoolId INT,
	@intPriceItemId INT,
	@blndirect VARCHAR(1),
	@txtCode VARCHAR(25),
	@txtNarrative VARCHAR(200),
	@dteFromDate DATE,
	@dteToDate DATE,
	@quantity varchar(10),
	--@txtNote varchar(5120),
	@txtExternalRef varchar(30),@intEPackageId int, @txtExternalRefDerivedFrom varchar(30), @intESundryId int output,@alreadyCancelled BIT OUT',
	@intESundryStatusId,
@dteCreatedDateTime,
@intCreatedUserId,
@dteCreationDate,
@intAmendedUserId,
@dteAmendedDateTime,
@intNumberOfUnits,
@intNumberOfPartUnits,
@intChargeUnits,
@intChargePartUnits,
@intChargeRate ,@source, @intEnrolId, @intSchoolId, @intPriceItemId,
	 @blnDirect, @txtCode, @txtNarrative, @dteFromDate,@dteToDate, @quantity
	--,@txtNote
	,@txtExternalRef,@intEPackageId,@txtExternalRefDerivedFrom,  @intESundryId  output,@alreadyCancelled OUT;
	SELECT @intESundryId_OUT = @intESundryId
	IF (@alreadyCancelled = 1)
		 RETURN
	If(@prevUnits != @intNumberOfUnits)
				INSERT INTO @a (intPriceItemId,
									intGroupId,
									intEnrolId,
									intEnrolBookingId,
									intModuleId,
									intInvoiceLineId,
									intUserId,
									intActionId,
									dteTimestamp,
									txtUsername,
									decOldValue,
									decNewValue,
									decDifference
									)
				values				(
									@intPriceItemId,
									-1,
									CAST(@intEnrolId AS INT),
									@intESundryId_OUT,
									2000,
									-1,
									9999,
									12,
									getdate(),
									'Apttus',
									@prevUnits,
									@intNumberOfUnits,
									@intNumberOfUnits - @prevUnits
	)						
	If(@prevChargeUnits != @intChargeUnits)
				INSERT INTO @a (intPriceItemId,
									intGroupId,
									intEnrolId,
									intEnrolBookingId,
									intModuleId,
									intInvoiceLineId,
									intUserId,
									intActionId,
									dteTimestamp,
									txtUsername,
									decOldValue,
									decNewValue,
									decDifference
									)
				values				(
									@intPriceItemId,
									-1,
									CAST(@intEnrolId AS INT),
									@intESundryId_OUT,
									2000,
									-1,
									9999,
									40,
									getdate(),
									'Apttus',
									@prevChargeUnits,
									@intChargeUnits,
									@intChargeUnits - @prevChargeUnits
									)						
			If(@prevFromDate != @dteFromDate)
					INSERT INTO @a (intPriceItemId,
									intGroupId,
									intEnrolId,
									intEnrolBookingId,
									intModuleId,
									intInvoiceLineId,
									intUserId,
									intActionId,
									dteTimestamp,
									txtUsername,
									dteOldDateValue,
									dteNewDateValue,
									decDifference
									)
					values			(
									@intPriceItemId,
									-1,
									CAST(@intEnrolId AS INT),
									@intESundryId_OUT,
									2000,
									-1,
									9999,
									10,
									getdate(),
									'Apttus',
									@prevFromDate,
									@dteFromDate,
									DATEDIFF(dd,@dteFromDate, @prevFromDate)
									)						
			If(@prevToDate != @dteToDate)
					INSERT INTO @a (intPriceItemId,
									intGroupId,
									intEnrolId,
									intEnrolBookingId,
									intModuleId,
									intInvoiceLineId,
									intUserId,
									intActionId,
									dteTimestamp,
									txtUsername,
									dteOldDateValue,
									dteNewDateValue,
									decDifference
									)
					values			(
									@intPriceItemId,
									-1,
									CAST(@intEnrolId AS INT),
									@intESundryId_OUT,
									2000,
									-1,
									9999,
									11,
									getdate(),
									'Apttus',
									@prevToDate,
									@dteToDate,
									DATEDIFF(dd,@dteToDate ,@prevToDate)
									)	
			If(@prevChargeRate != @intChargeRate)
					INSERT INTO @a (intPriceItemId,
									intGroupId,
									intEnrolId,
									intEnrolBookingId,
									intModuleId,
									intInvoiceLineId,
									intUserId,
									intActionId,
									dteTimestamp,
									txtUsername,
									decOldValue,
									decNewValue,
									decDifference
									)
					values			(
									@intPriceItemId,
									-1,
									CAST(@intEnrolId AS INT),
									@intESundryId_OUT,
									2000,
									-1,
									9999,
									42,
									getdate(),
									'Apttus',
									@prevChargeRate,
									@intChargeRate,
									@intChargeRate - @prevChargeRate
									)	
			
				If(@prevDirectFlag != @blnDirect)
					INSERT INTO @a (intPriceItemId,
									intGroupId,
									intEnrolId,
									intEnrolBookingId,
									intModuleId,
									intInvoiceLineId,
									intUserId,
									intActionId,
									dteTimestamp,
									txtUsername,
									decOldValue,
									decNewValue,
									decDifference
									)
					values			(
									@intPriceItemId,
									-1,
									CAST(@intEnrolId AS INT),
									@intESundryId_OUT,
									2000,
									-1,
									9999,
									47,
									getdate(),
									'Apttus',
									@prevDirectFlag,
									@blnDirect,
									0
									)	
			If(@intESundryStatusId = 200)
					INSERT INTO @a (intPriceItemId,
									intGroupId,
									intEnrolId,
									intEnrolBookingId,
									intModuleId,
									intInvoiceLineId,
									intUserId,
									intActionId,
									dteTimestamp,
									txtUsername,
									decOldValue,
									decNewValue,
									decDifference
									)
					values			(
									@intPriceItemId,
									-1,
									CAST(@intEnrolId AS INT),
									@intESundryId_OUT,
									2000,
									-1,
									9999,
									31,
									getdate(),
									'Apttus',
									0,
									0,
									0
									)	
		END
		ELSE IF (@txtESundryStatus != 'Cancelled')
		BEGIN
			set @operation = 'I';
			SET @sql = 'INSERT INTO [' +@source+ '].[dbo].[tblESundry]
			(
				intEnrolId,
				intPriceItemId,
				intESundryStatusId,
				dteCreationDate,
				txtCode,
				txtNarrative,
				dteFromDate,
				dteToDate,
				txtNote,
				blnDirect,
				txtExternalRef,
				dteCreatedDateTime,
				intCreatedUserId,
				dteAmendedDateTime,
				intAmendedUserId,
				intNumberOfUnits,
				intNumberOfPartUnits,
				intChargeRate,
				intChargeUnits,
				intChargePartUnits,
				intEPackageId
				
			)
			VALUES
			(
				CAST(@intEnrolId AS INT),
				@intPriceItemId,
				@intESundryStatusId,
				@dteCreationDate,
				@txtCode,
				@txtNarrative,
				@dteFromDate,
				@dteToDate,
				'' '',
				--@txtNote,
				CAST(@blnDirect AS BIT),
				@txtExternalRef,
				@dteCreatedDateTime,
				@intCreatedUserId,
				@dteAmendedDateTime,
				@intAmendedUserId,
				@intNumberOfUnits,
				@intNumberOfPartUnits,
				@intChargeRate,
				@intChargeUnits,
				@intChargePartUnits,
				@intEPackageId
				
			)
			
			IF(@@ROWCOUNT = 0)
			BEGIN
				PRINT ERROR_MESSAGE();
				RAISERROR(''No ESundry inserted'',16, 1);
			END
			SELECT @intESundryId =SCOPE_IDENTITY();'
	EXECUTE sp_executesql @sql, 	N'@intESundryStatusId INT,
@dteCreatedDateTime DATETIME,
@intCreatedUserId INT,
@dteCreationDate DATE,
@intAmendedUserId INT,
@dteAmendedDateTime DATETIME,
@intNumberOfUnits INT ,
@intNumberOfPartUnits INT,
@intChargeUnits INT,
@intChargePartUnits INT,
@intEPackageId int,
@intChargeRate INT,@source  varchar(20),
	@intEnrolId VARCHAR(10),
	@intSchoolId INT,
	@intPriceItemId INT,
	@blndirect VARCHAR(1),
	@txtCode VARCHAR(25),
	@txtNarrative VARCHAR(200),
	@dteFromDate DATE,
	@dteToDate DATE,
	@quantity varchar(10),
	--@txtNote varchar(5120),
	@txtExternalRef varchar(30), @intESundryId int output',@intESundryStatusId,
@dteCreatedDateTime,
@intCreatedUserId,
@dteCreationDate,
@intAmendedUserId,
@dteAmendedDateTime,
@intNumberOfUnits,
@intNumberOfPartUnits,
@intChargeUnits,
@intChargePartUnits,
@intEPackageId,
@intChargeRate,@source, @intEnrolId, @intSchoolId, @intPriceItemId,
	 @blnDirect, @txtCode, @txtNarrative, @dteFromDate,@dteToDate, @quantity
	--,@txtNote
	,@txtExternalRef, @intESundryId = @intESundryId_OUT output;

	If @operation = 'I'
			INSERT INTO @a (intPriceItemId,
					intGroupId,
					intEnrolId,
					intEnrolBookingId,
					intModuleId,
					intInvoiceLineId,
					intUserId,
					intActionId,
					dteTimestamp,
					txtUsername,
					decOldValue,
					decNewValue,
					decDifference
					)
			values (
					@intPriceItemId,
					-1,
					CAST(@intEnrolId AS INT),
					@intESundryId_OUT,
					2000,
					-1,
					9999,
					22,
					getdate(),
					'Apttus',
					0,
					0,
					0
					)	
	END;
	
	SEt @sql = 'INSERT INTO [' +  @source + '].[dbo].[tblBookingAudit]	( intPriceItemId ,
	 intGroupId  ,
	 intEnrolId  ,
	 intEnrolBookingId ,
	 intModuleId  ,
	 intInvoiceLineId ,
	 intUserId  ,
	 intActionId,
	 dteTimestamp ,
	 txtUsername  ,
	 dteOldDateValue ,
	 dteNewDatevalue  ,
	 decOldValue  ,
	 decNewValue  ,
	 decDifference ,
	 txtNote  ,
	 txtComment )	
	SELECT  intPriceItemId ,
	 intGroupId  ,
	 intEnrolId  ,
	 intEnrolBookingId ,
	 intModuleId  ,
	 intInvoiceLineId ,
	 intUserId  ,
	 intActionId,
	 dteTimestamp ,
	 txtUsername  ,
	 dteOldDateValue ,
	 dteNewDatevalue  ,
	 decOldValue  ,
	 decNewValue  ,
	 decDifference ,
	 txtNote  ,
	 txtComment  from @a';
	 EXECUTE sp_executesql @sql,N'@a auditLog READONLY',@a

	
	

RETURN 0
END
