USE [ClassIntegration]
GO
/****** Object:  StoredProcedure [APTTUS].[GetSchoolId]    Script Date: 07/01/2020 15:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [APTTUS].[GetSchoolId]
@SchoolCode VARCHAR(3), 
@ClassDB VARCHAR(14),
@SchoolId INT OUT
AS
BEGIN
	DECLARE @sql nvarchar(max);
	
	SET @sql = '
	SELECT  @SchoolId' + char(61) + ' [intSchoolId]  
    FROM  [' + @ClassDB+ '].[dbo].[tblSchool]
    WHERE [txtCode] = '+ CHAR(39) + @SchoolCode + CHAR(39) ;
	--SELECT @sql;
	EXEC sp_executesql @sql, N'@SchoolId INT OUT', @SchoolId OUT;
	SELECT @SchoolId
    RETURN 0;
	END




