USE [ClassIntegration]
GO
/****** Object:  StoredProcedure [APTTUS].[UpsertUDF]    Script Date: 07/01/2020 15:57:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [APTTUS].[UpsertUDF] 
 @APTS_Progression_Type__c varchar(75) ,
 @APTS_Institution__c varchar(75) ,
 @APTS_Degree__c varchar(75) ,
 @APTS_Intake_Date__c varchar(75) ,
 @APTS_SELT_Requirement__c varchar(75) ,
 @APTS_SELT_Listening__c varchar(75) ,
 @APTS_SELT_Reading__c varchar(75) ,
 @APTS_SELT_Writing__c varchar(75) ,
 @APTS_SELT_Speaking__c varchar(75) ,
 @APTS_Current_Fee__c varchar(75) ,
 @APTS_Pre_Departure_SELT_Overall__c varchar(75) ,
 @APTS_Pre_Departure_SELT_Reading__c varchar(75) ,
 @APTS_Pre_Departure_SELT_Listening__c varchar(75) ,
 @APTS_Pre_Departure_SELT_Writing__c varchar(75),
 @APTS_Pre_Departure_SELT_Speaking__c varchar(75) ,
 @intstudentid int
AS
BEGIN


select @APTS_Progression_Type__c  = case when @APTS_Progression_Type__c = 'null' then '' else @APTS_Progression_Type__c end,
 @APTS_Institution__c = case when @APTS_Institution__c = 'null' then '' else @APTS_Institution__c end,
 @APTS_Degree__c  = case when @APTS_Degree__c = 'null' then '' else @APTS_Degree__c end,
 @APTS_Intake_Date__c = case when @APTS_Intake_Date__c = 'null' then null else @APTS_Intake_Date__c end,
 @APTS_SELT_Requirement__c = case when @APTS_SELT_Requirement__c = 'null' then '' else @APTS_SELT_Requirement__c end,
 @APTS_SELT_Listening__c = case when @APTS_SELT_Listening__c = 'null' then '' else @APTS_SELT_Listening__c end,
 @APTS_SELT_Reading__c = case when @APTS_SELT_Reading__c = 'null' then '' else @APTS_SELT_Reading__c end,
 @APTS_SELT_Writing__c = case when @APTS_SELT_Writing__c = 'null' then '' else @APTS_SELT_Writing__c end,
 @APTS_SELT_Speaking__c = case when @APTS_SELT_Speaking__c = 'null' then '' else @APTS_SELT_Speaking__c end,
 @APTS_Current_Fee__c = case when @APTS_Current_Fee__c = 'null' then '' else @APTS_Current_Fee__c end,
 @APTS_Pre_Departure_SELT_Overall__c  = case when @APTS_Pre_Departure_SELT_Overall__c = 'null' then '' else @APTS_Pre_Departure_SELT_Overall__c end,
 @APTS_Pre_Departure_SELT_Reading__c  = case when @APTS_Pre_Departure_SELT_Reading__c = 'null' then '' else @APTS_Pre_Departure_SELT_Reading__c end,
 @APTS_Pre_Departure_SELT_Listening__c = case when @APTS_Pre_Departure_SELT_Listening__c = 'null' then '' else @APTS_Pre_Departure_SELT_Listening__c end,
 @APTS_Pre_Departure_SELT_Writing__c = case when @APTS_Pre_Departure_SELT_Writing__c = 'null' then '' else @APTS_Pre_Departure_SELT_Writing__c end,
 @APTS_Pre_Departure_SELT_Speaking__c = case when @APTS_Pre_Departure_SELT_Speaking__c = 'null' then '' else @APTS_Pre_Departure_SELT_Speaking__c end;


DECLARE @sql nvarchar(max);
SET @sql = '
MERGE [CFW_ASPECT_GBR].[dbo].tblAnalysisUDFitem item
USING (
select case when m.apttusfieldname = ''APTS_Progression_Type__c'' then @APTS_Progression_Type__c 
when m.apttusfieldname = ''APTS_Institution__c'' then @APTS_Institution__c
when m.apttusfieldname = ''APTS_Degree__c'' then @APTS_Degree__c
when m.apttusfieldname = ''APTS_Current_Fee__c'' then @APTS_Current_Fee__c
when m.apttusfieldname = ''APTS_SELT_Requirement__c'' then @APTS_SELT_Requirement__c
when m.apttusfieldname = ''APTS_SELT_Listening__c'' then @APTS_SELT_Listening__c
when m.apttusfieldname = ''APTS_SELT_Reading__c'' then @APTS_SELT_Reading__c
when m.apttusfieldname = ''APTS_SELT_Writing__c'' then @APTS_SELT_Writing__c
when m.apttusfieldname = ''APTS_SELT_Speaking__c'' then @APTS_SELT_Speaking__c
when m.apttusfieldname = ''APTS_Pre_Departure_SELT_Overall__c'' then @APTS_Pre_Departure_SELT_Overall__c
when m.apttusfieldname = ''APTS_Pre_Departure_SELT_Listening__c'' then @APTS_Pre_Departure_SELT_Listening__c
when m.apttusfieldname = ''APTS_Pre_Departure_SELT_Reading__c'' then @APTS_Pre_Departure_SELT_Reading__c
when m.apttusfieldname = ''APTS_Pre_Departure_SELT_Writing__c'' then @APTS_Pre_Departure_SELT_Writing__c
when m.apttusfieldname = ''APTS_Pre_Departure_SELT_Speaking__c'' then @APTS_Pre_Departure_SELT_Speaking__c
when m.apttusfieldname = ''APTS_Intake_Date__c'' then @APTS_Intake_Date__c
else null end as value,
n.intAnalysisUDFNameId,
n.txtName,n.intAnalysisUDFSectionId,n.intModuleId,n.blnActive
from [CFW_ASPECT_GBR].[dbo].tblAnalysisUDFName n 
inner join [CFW_ASPECT_GBR].[dbo].tblAnalysisUDFSection s on n.intAnalysisUDFSectionId = s.intAnalysisUDFSectionId and s.blnActive = 1 and s.intModuleId = 1000
inner join ClassIntegration.apttus.Udf_Mapping m  on  m.classdb = ''CFW_ASPECT_GBR'' and n.txtName = m.classfieldname
where  s.txtname  = ''UPS Progression Route #1'' and n.blnActive = 1
--AND case when m.apttusfieldname = ''APTS_Progression_Type__c'' then @APTS_Progression_Type__c 
--when m.apttusfieldname = ''APTS_Institution__c'' then @APTS_Institution__c
--when m.apttusfieldname = ''APTS_Degree__c'' then @APTS_Degree__c
--when m.apttusfieldname = ''APTS_Current_Fee__c'' then @APTS_Current_Fee__c
--when m.apttusfieldname = ''APTS_SELT_Requirement__c'' then @APTS_SELT_Requirement__c
--when m.apttusfieldname = ''APTS_SELT_Listening__c'' then @APTS_SELT_Listening__c
--when m.apttusfieldname = ''APTS_SELT_Reading__c'' then @APTS_SELT_Reading__c
--when m.apttusfieldname = ''APTS_SELT_Writing__c'' then @APTS_SELT_Writing__c
--when m.apttusfieldname = ''APTS_SELT_Speaking__c'' then @APTS_SELT_Speaking__c
--when m.apttusfieldname = ''APTS_Pre_Departure_SELT_Overall__c'' then @APTS_Pre_Departure_SELT_Overall__c
--when m.apttusfieldname = ''APTS_Pre_Departure_SELT_Listening__c'' then @APTS_Pre_Departure_SELT_Listening__c
--when m.apttusfieldname = ''APTS_Pre_Departure_SELT_Reading__c'' then @APTS_Pre_Departure_SELT_Reading__c
--when m.apttusfieldname = ''APTS_Pre_Departure_SELT_Writing__c'' then @APTS_Pre_Departure_SELT_Writing__c
--when m.apttusfieldname = ''APTS_Pre_Departure_SELT_Speaking__c'' then @APTS_Pre_Departure_SELT_Speaking__c
--else null end is not null
) tmpTable
ON 
   item.intrecordid = @intStudentid  and item.intAnalysisUDFNameId = tmpTable.intAnalysisUDFNameId --AND tmpTable.value is not null 

WHEN MATCHED AND (tmpTable.value is not null AND tmpTable.value != '''' ) THEN
       UPDATE
       SET item.txtcontent = tmpTable.value
WHEN NOT MATCHED BY TARGET AND (tmpTable.value is not null AND tmpTable.value != '''')  THEN
insert (intAnalysisUDFNameId,intRecordId,txtContent,txtNote)
values (tmpTable.intAnalysisUDFNameId,@intStudentId,tmpTable.value,'''');
         
       select    @@ROWCOUNT  as updatedRowCount'
       select @sql
	   print @sql

EXECUTE sp_executesql @sql,N'@intStudentid int,@APTS_Progression_Type__c   varchar(75),   @APTS_Institution__c varchar(75),
     @APTS_Degree__c varchar(75) ,@APTS_Intake_Date__c varchar(75),  @APTS_SELT_Requirement__c varchar(75),
@APTS_SELT_Listening__c varchar(75), @APTS_SELT_Reading__c varchar(75), @APTS_SELT_Writing__c varchar(75),
@APTS_SELT_Speaking__c varchar(75), @APTS_Current_Fee__c varchar(75) ,  @APTS_Pre_Departure_SELT_Overall__c varchar(75),
  @APTS_Pre_Departure_SELT_Reading__c varchar(75), @APTS_Pre_Departure_SELT_Listening__c varchar(75),  @APTS_Pre_Departure_SELT_Writing__c varchar(75) ,
  @APTS_Pre_Departure_SELT_Speaking__c varchar(75)',@intStudentid,@APTS_Progression_Type__c  ,   @APTS_Institution__c ,
     @APTS_Degree__c  ,@APTS_Intake_Date__c,  @APTS_SELT_Requirement__c,
@APTS_SELT_Listening__c , @APTS_SELT_Reading__c , @APTS_SELT_Writing__c ,
@APTS_SELT_Speaking__c , @APTS_Current_Fee__c  ,  @APTS_Pre_Departure_SELT_Overall__c,
  @APTS_Pre_Departure_SELT_Reading__c , @APTS_Pre_Departure_SELT_Listening__c ,  @APTS_Pre_Departure_SELT_Writing__c ,
  @APTS_Pre_Departure_SELT_Speaking__c 



--  SET @sql = '
--MERGE [CFW_ASPECT_GBR].[dbo].tblAnalysisUDFitem item
--USING (
--select case when m.apttusfieldname = ''APTS_Intake_Date__c'' then @APTS_Intake_Date__c
--else null end as value,
--n.intAnalysisUDFNameId,
--n.txtName,n.intAnalysisUDFSectionId,n.intModuleId,n.blnActive
--from [CFW_ASPECT_GBR].[dbo].tblAnalysisUDFName n 
--inner join [CFW_ASPECT_GBR].[dbo].tblAnalysisUDFSection s on n.intAnalysisUDFSectionId = s.intAnalysisUDFSectionId and s.blnActive = 1 and s.intModuleId = 1000
--inner join ClassIntegration.apttus.Udf_Mapping m  on  m.classdb = ''CFW_ASPECT_GBR'' and n.txtName = m.classfieldname
--where  s.txtname  = ''UPS Progression Route #1'' and n.blnActive = 1
--AND m.ApttusFieldName = ''APTS_Intake_Date__c''
----AND  case when m.apttusfieldname = ''APTS_Intake_Date__c'' then @APTS_Intake_Date__c
----else null end  is not null
--) tmpTable
--ON 
--   item.intrecordid = @intStudentid  and item.intAnalysisUDFNameId = tmpTable.intAnalysisUDFNameId --AND tmpTable.value is not null 

--WHEN MATCHED  THEN
--       UPDATE
--       SET item.dteDate = tmpTable.value
--WHEN NOT MATCHED BY TARGET  AND (tmpTable.value is not null )  THEN
--insert (intAnalysisUDFNameId,intRecordId,dteDate,txtNote)
--values (tmpTable.intAnalysisUDFNameId,@intStudentId,tmpTable.value,'''');
         
--       select    @@ROWCOUNT  as updatedRowCount'
--       select @sql

--EXECUTE sp_executesql @sql,N'@intStudentid int,@APTS_Intake_Date__c varchar(75)',@intStudentid,@APTS_Intake_Date__c


              
RETURN 0
END
