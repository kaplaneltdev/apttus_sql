USE [ClassIntegration]
GO
/****** Object:  UserDefinedTableType [dbo].[auditLog]    Script Date: 08/04/2018 17:41:07 ******/
CREATE TYPE [dbo].[auditLog] AS TABLE(
	[intPriceItemId] [int] NOT NULL,
	[intGroupId] [int] NOT NULL,
	[intEnrolId] [int] NOT NULL,
	[intEnrolBookingId] [int] NOT NULL,
	[intModuleId] [int] NOT NULL,
	[intInvoiceLineId] [int] NOT NULL,
	[intUserId] [int] NOT NULL,
	[intActionId] [int] NOT NULL,
	[dteTimestamp] [datetime] NOT NULL,
	[txtUsername] [char](30) NOT NULL,
	[dteOldDateValue] [date] NULL,
	[dteNewDatevalue] [date] NULL,
	[decOldValue] [decimal](18, 2) NOT NULL DEFAULT ((0)),
	[decNewValue] [decimal](18, 2) NOT NULL DEFAULT ((0)),
	[decDifference] [decimal](18, 2) NOT NULL,
	[txtNote] [varchar](5120) NULL,
	[txtComment] [char](30) NULL
)
GO
