/****** Changeset for APTCPQ-1282-new-price-item-online-ielts-bootcamp on 2020-09-03 11:00 ******/

USE [ClassIntegration]
GO

SET IDENTITY_INSERT [APTTUS].[PRODUCT] ON
INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'OLIELTSBOOT', N'Online IELTS Bootcamp', N'CFW_ASPECT_GBR')
SET IDENTITY_INSERT [APTTUS].[PRODUCT] OFF
