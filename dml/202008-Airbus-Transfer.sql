/****** Changeset for Airbus Transfer Items on 2020-08-06 14:20 ******/

USE [ClassIntegration]
GO

SET IDENTITY_INSERT [APTTUS].[PRODUCT] ON
INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 9, N'TSABLAX', N'Transfer Service Los Angeles International', N'CFW_ASPECT_USA')
SET IDENTITY_INSERT [APTTUS].[PRODUCT] OFF

SET IDENTITY_INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ON
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductAttributeValueId], [ProductId], [AttributeId], [Value]) VALUES ((SELECT MAX(ProductAttributeValueId) from [APTTUS].[MAP_PRODUCT_ATTRIBUTE]) +1, (SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Transfer Service Los Angeles International'), 16, N'Los Angeles International Airport')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductAttributeValueId], [ProductId], [AttributeId], [Value]) VALUES ((SELECT MAX(ProductAttributeValueId) from [APTTUS].[MAP_PRODUCT_ATTRIBUTE]) +1, (SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Transfer Service Los Angeles International'), 17, N'Airbus')
SET IDENTITY_INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] OFF
