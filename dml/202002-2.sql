/****** Changeset for Group Transfer dated on 2020-02-21 14:45 ******/

USE [ClassIntegration]
GO

SET IDENTITY_INSERT [APTTUS].[PRODUCT] ON

INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'GRPTSA', N'Group Arrival Transfer Service', N'CFW_ASPECT_CAN')
INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'GRPTSD', N'Group Departure Transfer Service', N'CFW_ASPECT_CAN')

INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'GRPTSA', N'Group Arrival Transfer Service', N'CFW_ASPECT_USA')
INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'GRPTSD', N'Group Departure Transfer Service', N'CFW_ASPECT_USA')

INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'GRPTSA', N'Group Arrival Transfer Service', N'CFW_ASPECT_IRL')
INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'GRPTSD', N'Group Departure Transfer Service', N'CFW_ASPECT_IRL')

INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'GRPTSA', N'Group Arrival Transfer Service', N'CFW_ASPECT_AUS')
INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'GRPTSD', N'Group Departure Transfer Service', N'CFW_ASPECT_AUS')

INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'GRPTSA', N'Group Arrival Transfer Service', N'CFW_ASPECT_NZL')
INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'GRPTSD', N'Group Departure Transfer Service', N'CFW_ASPECT_NZL')

SET IDENTITY_INSERT [APTTUS].[PRODUCT] OFF
