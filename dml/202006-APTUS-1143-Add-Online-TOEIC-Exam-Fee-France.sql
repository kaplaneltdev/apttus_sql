/****** Changeset for APTCPQ-1143 for adding Online TOEIC Exam Fee - France on 2020-06-11 10:20 ******/

USE [ClassIntegration]
GO

SET IDENTITY_INSERT [APTTUS].[PRODUCT] ON
INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'OLTCEXAMFEE', N'Online TOEIC Exam Fee - France', N'CFW_ASPECT_GBR')
INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'OLTCEXAMFEE', N'Online TOEIC Exam Fee - France', N'CFW_ASPECT_IRL')
INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'OLTCEXAMFEE', N'Online TOEIC Exam Fee - France', N'CFW_ASPECT_CAN')
INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'OLTCEXAMFEE', N'Online TOEIC Exam Fee - France', N'CFW_ASPECT_USA')
INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'OLTCEXAMFEE', N'Online TOEIC Exam Fee - France', N'CFW_ASPECT_AUS')
INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'OLTCEXAMFEE', N'Online TOEIC Exam Fee - France', N'CFW_ASPECT_NZL')
SET IDENTITY_INSERT [APTTUS].[PRODUCT] OFF
