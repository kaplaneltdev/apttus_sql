/****** Changeset for APTCPQ-1241: New Residence For Edinburgh & Oxford  ******/

USE [ClassIntegration]
GO

SET IDENTITY_INSERT [APTTUS].[PRODUCT] ON
INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 3, N'LSRAREHSPN', N'Residence Arran House Single No Meals', N'CFW_ASPECT_GBR')
INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 3, N'LSRSCOSPN', N'Residence Student Castle Single No Meals', N'CFW_ASPECT_GBR')
SET IDENTITY_INSERT [APTTUS].[PRODUCT] OFF

SET IDENTITY_INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ON
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductAttributeValueId], [ProductId], [AttributeId], [Value]) VALUES ((SELECT MAX(ProductAttributeValueId) from [APTTUS].[MAP_PRODUCT_ATTRIBUTE]) +1, (SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Residence Arran House Single No Meals'), 2, N'Single')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductAttributeValueId], [ProductId], [AttributeId], [Value]) VALUES ((SELECT MAX(ProductAttributeValueId) from [APTTUS].[MAP_PRODUCT_ATTRIBUTE]) +1, (SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Residence Arran House Single No Meals'), 4, N'Arran House')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductAttributeValueId], [ProductId], [AttributeId], [Value]) VALUES ((SELECT MAX(ProductAttributeValueId) from [APTTUS].[MAP_PRODUCT_ATTRIBUTE]) +1, (SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Residence Arran House Single No Meals'), 5, N'Yes')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductAttributeValueId], [ProductId], [AttributeId], [Value]) VALUES ((SELECT MAX(ProductAttributeValueId) from [APTTUS].[MAP_PRODUCT_ATTRIBUTE]) +1, (SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Residence Arran House Single No Meals'), 6, N'No Meals')

INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductAttributeValueId], [ProductId], [AttributeId], [Value]) VALUES ((SELECT MAX(ProductAttributeValueId) from [APTTUS].[MAP_PRODUCT_ATTRIBUTE]) +1, (SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Residence Student Castle Single No Meals'), 2, N'Single')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductAttributeValueId], [ProductId], [AttributeId], [Value]) VALUES ((SELECT MAX(ProductAttributeValueId) from [APTTUS].[MAP_PRODUCT_ATTRIBUTE]) +1, (SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Residence Student Castle Single No Meals'), 4, N'Student Castle')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductAttributeValueId], [ProductId], [AttributeId], [Value]) VALUES ((SELECT MAX(ProductAttributeValueId) from [APTTUS].[MAP_PRODUCT_ATTRIBUTE]) +1, (SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Residence Student Castle Single No Meals'), 5, N'Yes')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductAttributeValueId], [ProductId], [AttributeId], [Value]) VALUES ((SELECT MAX(ProductAttributeValueId) from [APTTUS].[MAP_PRODUCT_ATTRIBUTE]) +1, (SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Residence Student Castle Single No Meals'), 6, N'No Meals')
SET IDENTITY_INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] OFF
