/****** Changeset for APTCPQ-1241: Mappings for Cambridge courses fce ******/

USE [ClassIntegration]
GO
declare @fceproductgroupid int;
INSERT [APTTUS].[PRODUCTGROUP] ([ProductGroupName]) VALUES (N'Cambridge FCE')
select @fceproductgroupid = @@IDENTITY

INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupId], [AttributeId]) VALUES (@fceproductgroupid, 18)
INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupId], [AttributeId]) VALUES (@fceproductgroupid, 19)

PRINT 'Inserted the FCE ProductGroup mappings'

INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@fceproductgroupid, N'CAMFGEN4', N'Cambridge FCE 4 weeks', N'CFW_ASPECT_GBR')
INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@fceproductgroupid, N'CAMFGEN4', N'Cambridge FCE 4 weeks', N'CFW_ASPECT_IRL')
INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@fceproductgroupid, N'CAMFGEN4', N'Cambridge FCE 4 weeks', N'CFW_ASPECT_USA')
INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@fceproductgroupid, N'CAMFGEN4', N'Cambridge FCE 4 weeks', N'CFW_ASPECT_CAN')

PRINT 'Inserted the Product..Cambridge FCE 4 weeks'

INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@fceproductgroupid, N'CAMFGEN8', N'Cambridge FCE 8 weeks', N'CFW_ASPECT_GBR')
INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@fceproductgroupid, N'CAMFGEN8', N'Cambridge FCE 8 weeks', N'CFW_ASPECT_IRL')
INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@fceproductgroupid, N'CAMFGEN8', N'Cambridge FCE 8 weeks', N'CFW_ASPECT_USA')
INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@fceproductgroupid, N'CAMFGEN8', N'Cambridge FCE 8 weeks', N'CFW_ASPECT_CAN')

PRINT 'Inserted the Product..Cambridge FCE 8 weeks'

INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@fceproductgroupid, N'CAMFINT4', N'Cambridge FCE Intensive 4 weeks', N'CFW_ASPECT_GBR')
INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@fceproductgroupid, N'CAMFINT4', N'Cambridge FCE Intensive 4 weeks', N'CFW_ASPECT_IRL')
INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@fceproductgroupid, N'CAMFINT4', N'Cambridge FCE Intensive 4 weeks', N'CFW_ASPECT_USA')
INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@fceproductgroupid, N'CAMFINT4', N'Cambridge FCE Intensive 4 weeks', N'CFW_ASPECT_CAN')

PRINT 'Inserted the Product..Cambridge FCE Intensive 4 weeks'

INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@fceproductgroupid, N'CAMFINT8', N'Cambridge FCE Intensive 8 weeks', N'CFW_ASPECT_GBR')
INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@fceproductgroupid, N'CAMFINT8', N'Cambridge FCE Intensive 8 weeks', N'CFW_ASPECT_IRL')
INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@fceproductgroupid, N'CAMFINT8', N'Cambridge FCE Intensive 8 weeks', N'CFW_ASPECT_USA')
INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@fceproductgroupid, N'CAMFINT8', N'Cambridge FCE Intensive 8 weeks', N'CFW_ASPECT_CAN')

PRINT 'Inserted the Product..Cambridge FCE Intensive 8 weeks'

INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE 4 weeks' and p.classdb='CFW_ASPECT_GBR'), 18, N'4')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE 4 weeks' and p.classdb='CFW_ASPECT_GBR'), 19, N'Cambridge')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE 4 weeks' and p.classdb='CFW_ASPECT_IRL'), 18, N'4')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE 4 weeks' and p.classdb='CFW_ASPECT_IRL'), 19, N'Cambridge')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE 4 weeks' and p.classdb='CFW_ASPECT_USA'), 18, N'4')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE 4 weeks' and p.classdb='CFW_ASPECT_USA'), 19, N'Cambridge')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE 4 weeks' and p.classdb='CFW_ASPECT_CAN'), 18, N'4')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE 4 weeks' and p.classdb='CFW_ASPECT_CAN'), 19, N'Cambridge')

PRINT 'Inserted the Product attributes..Cambridge FCE 4 weeks'

INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE 8 weeks' and p.classdb='CFW_ASPECT_GBR'), 18, N'8')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE 8 weeks' and p.classdb='CFW_ASPECT_GBR'), 19, N'Cambridge')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE 8 weeks' and p.classdb='CFW_ASPECT_IRL'), 18, N'8')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE 8 weeks' and p.classdb='CFW_ASPECT_IRL'), 19, N'Cambridge')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE 8 weeks' and p.classdb='CFW_ASPECT_USA'), 18, N'8')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE 8 weeks' and p.classdb='CFW_ASPECT_USA'), 19, N'Cambridge')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE 8 weeks' and p.classdb='CFW_ASPECT_CAN'), 18, N'8')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE 8 weeks' and p.classdb='CFW_ASPECT_CAN'), 19, N'Cambridge')

PRINT 'Inserted the Product attributes..Cambridge FCE 8 weeks'

INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE Intensive 4 weeks' and p.classdb='CFW_ASPECT_GBR'), 18, N'4')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE Intensive 4 weeks' and p.classdb='CFW_ASPECT_GBR'), 19, N'Cambridge Intensive')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE Intensive 4 weeks' and p.classdb='CFW_ASPECT_IRL'), 18, N'4')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE Intensive 4 weeks' and p.classdb='CFW_ASPECT_IRL'), 19, N'Cambridge Intensive')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE Intensive 4 weeks' and p.classdb='CFW_ASPECT_USA'), 18, N'4')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE Intensive 4 weeks' and p.classdb='CFW_ASPECT_USA'), 19, N'Cambridge Intensive')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE Intensive 4 weeks' and p.classdb='CFW_ASPECT_CAN'), 18, N'4')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE Intensive 4 weeks' and p.classdb='CFW_ASPECT_CAN'), 19, N'Cambridge Intensive')

PRINT 'Inserted the Product attributes..Cambridge FCE Intensive 4 weeks'

INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE Intensive 8 weeks' and p.classdb='CFW_ASPECT_GBR'), 18, N'8')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE Intensive 8 weeks' and p.classdb='CFW_ASPECT_GBR'), 19, N'Cambridge Intensive')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE Intensive 8 weeks' and p.classdb='CFW_ASPECT_IRL'), 18, N'8')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE Intensive 8 weeks' and p.classdb='CFW_ASPECT_IRL'), 19, N'Cambridge Intensive')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE Intensive 8 weeks' and p.classdb='CFW_ASPECT_USA'), 18, N'8')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE Intensive 8 weeks' and p.classdb='CFW_ASPECT_USA'), 19, N'Cambridge Intensive')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE Intensive 8 weeks' and p.classdb='CFW_ASPECT_CAN'), 18, N'8')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge FCE Intensive 8 weeks' and p.classdb='CFW_ASPECT_CAN'), 19, N'Cambridge Intensive')

PRINT 'Inserted the Product attributes..Cambridge FCE Intensive 8 weeks'
