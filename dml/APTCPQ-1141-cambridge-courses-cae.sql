/****** Changeset for APTCPQ-1241: Mappings for Cambridge courses cae ******/

USE [ClassIntegration]
GO
declare @caeproductgroupid int;

INSERT [APTTUS].[PRODUCTGROUP] ([ProductGroupName]) VALUES (N'Cambridge CAE')
select @caeproductgroupid = @@IDENTITY

INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupId], [AttributeId]) VALUES (@caeproductgroupid, 18)
INSERT [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] ([ProductGroupId], [AttributeId]) VALUES (@caeproductgroupid, 19)

PRINT 'Inserted the ProductGroup mappings'

INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@caeproductgroupid, N'CAMCGEN4', N'Cambridge CAE 4 weeks', N'CFW_ASPECT_GBR')
INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@caeproductgroupid, N'CAMCGEN4', N'Cambridge CAE 4 weeks', N'CFW_ASPECT_IRL')
INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@caeproductgroupid, N'CAMCGEN4', N'Cambridge CAE 4 weeks', N'CFW_ASPECT_USA')
INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@caeproductgroupid, N'CAMCGEN4', N'Cambridge CAE 4 weeks', N'CFW_ASPECT_CAN')

PRINT 'Inserted the Product..Cambridge CAE 4 weeks'

INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@caeproductgroupid, N'CAMCGEN8', N'Cambridge CAE 8 weeks', N'CFW_ASPECT_GBR')
INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@caeproductgroupid, N'CAMCGEN8', N'Cambridge CAE 8 weeks', N'CFW_ASPECT_IRL')
INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@caeproductgroupid, N'CAMCGEN8', N'Cambridge CAE 8 weeks', N'CFW_ASPECT_USA')
INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@caeproductgroupid, N'CAMCGEN8', N'Cambridge CAE 8 weeks', N'CFW_ASPECT_CAN')

PRINT 'Inserted the Product..Cambridge CAE 8 weeks'

INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@caeproductgroupid, N'CAMCINT4', N'Cambridge CAE Intensive 4 weeks', N'CFW_ASPECT_GBR')
INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@caeproductgroupid, N'CAMCINT4', N'Cambridge CAE Intensive 4 weeks', N'CFW_ASPECT_IRL')
INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@caeproductgroupid, N'CAMCINT4', N'Cambridge CAE Intensive 4 weeks', N'CFW_ASPECT_USA')
INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@caeproductgroupid, N'CAMCINT4', N'Cambridge CAE Intensive 4 weeks', N'CFW_ASPECT_CAN')

PRINT 'Inserted the Product..Cambridge CAE intensive 4 weeks'

INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@caeproductgroupid, N'CAMCINT8', N'Cambridge CAE Intensive 8 weeks', N'CFW_ASPECT_GBR')
INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@caeproductgroupid, N'CAMCINT8', N'Cambridge CAE Intensive 8 weeks', N'CFW_ASPECT_IRL')
INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@caeproductgroupid, N'CAMCINT8', N'Cambridge CAE Intensive 8 weeks', N'CFW_ASPECT_USA')
INSERT [APTTUS].[PRODUCT] ([ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES (@caeproductgroupid, N'CAMCINT8', N'Cambridge CAE Intensive 8 weeks', N'CFW_ASPECT_CAN')

PRINT 'Inserted the Product..Cambridge CAE intensive 4 weeks'

INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE 4 weeks' and p.classdb='CFW_ASPECT_GBR'), 18, N'4')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE 4 weeks' and p.classdb='CFW_ASPECT_GBR'), 19, N'Cambridge')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE 4 weeks' and p.classdb='CFW_ASPECT_IRL'), 18, N'4')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE 4 weeks' and p.classdb='CFW_ASPECT_IRL'), 19, N'Cambridge')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE 4 weeks' and p.classdb='CFW_ASPECT_USA'), 18, N'4')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE 4 weeks' and p.classdb='CFW_ASPECT_USA'), 19, N'Cambridge')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE 4 weeks' and p.classdb='CFW_ASPECT_CAN'), 18, N'4')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE 4 weeks' and p.classdb='CFW_ASPECT_CAN'), 19, N'Cambridge')

PRINT 'Inserted the Product attributes..Cambridge CAE 4 weeks'

INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE 8 weeks' and p.classdb='CFW_ASPECT_GBR'), 18, N'4')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE 8 weeks' and p.classdb='CFW_ASPECT_GBR'), 19, N'Cambridge')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE 8 weeks' and p.classdb='CFW_ASPECT_IRL'), 18, N'4')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE 8 weeks' and p.classdb='CFW_ASPECT_IRL'), 19, N'Cambridge')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE 8 weeks' and p.classdb='CFW_ASPECT_USA'), 18, N'4')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE 8 weeks' and p.classdb='CFW_ASPECT_USA'), 19, N'Cambridge')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE 8 weeks' and p.classdb='CFW_ASPECT_CAN'), 18, N'4')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE 8 weeks' and p.classdb='CFW_ASPECT_CAN'), 19, N'Cambridge')

PRINT 'Inserted the Product attributes..Cambridge CAE 8 weeks'

INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE Intensive 4 weeks' and p.classdb='CFW_ASPECT_GBR'), 18, N'4')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE Intensive 4 weeks' and p.classdb='CFW_ASPECT_GBR'), 19, N'Cambridge Intensive')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE Intensive 4 weeks' and p.classdb='CFW_ASPECT_IRL'), 18, N'4')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE Intensive 4 weeks' and p.classdb='CFW_ASPECT_IRL'), 19, N'Cambridge Intensive')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE Intensive 4 weeks' and p.classdb='CFW_ASPECT_USA'), 18, N'4')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE Intensive 4 weeks' and p.classdb='CFW_ASPECT_USA'), 19, N'Cambridge Intensive')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE Intensive 4 weeks' and p.classdb='CFW_ASPECT_CAN'), 18, N'4')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE Intensive 4 weeks' and p.classdb='CFW_ASPECT_CAN'), 19, N'Cambridge Intensive')

PRINT 'Inserted the Product attributes..Cambridge CAE intensive 4 weeks'

INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE Intensive 8 weeks' and p.classdb='CFW_ASPECT_GBR'), 18, N'8')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE Intensive 8 weeks' and p.classdb='CFW_ASPECT_GBR'), 19, N'Cambridge Intensive')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE Intensive 8 weeks' and p.classdb='CFW_ASPECT_IRL'), 18, N'8')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE Intensive 8 weeks' and p.classdb='CFW_ASPECT_IRL'), 19, N'Cambridge Intensive')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE Intensive 8 weeks' and p.classdb='CFW_ASPECT_USA'), 18, N'8')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE Intensive 8 weeks' and p.classdb='CFW_ASPECT_USA'), 19, N'Cambridge Intensive')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE Intensive 8 weeks' and p.classdb='CFW_ASPECT_CAN'), 18, N'8')
INSERT [APTTUS].[MAP_PRODUCT_ATTRIBUTE] ([ProductId], [AttributeId], [Value]) VALUES ((SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName = 'Cambridge CAE Intensive 8 weeks' and p.classdb='CFW_ASPECT_CAN'), 19, N'Cambridge Intensive')

PRINT 'Inserted the Product attributes..Cambridge CAE intensive 8 weeks'
