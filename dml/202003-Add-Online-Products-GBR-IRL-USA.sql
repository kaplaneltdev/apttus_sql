/****** Changeset for New Residence Items on 2020-03-12 14:20 ******/

USE [ClassIntegration]
GO

SET IDENTITY_INSERT [APTTUS].[PRODUCT] ON
INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'OLSPEAK', N'Online Speaking', N'CFW_ASPECT_GBR')
INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'OLSPEAK', N'Online Speaking', N'CFW_ASPECT_IRL')
INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'OLSPEAK', N'Online Speaking', N'CFW_ASPECT_USA')

INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'OLGRAM', N'Online Grammar and Vocabulary', N'CFW_ASPECT_GBR')
INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'OLGRAM', N'Online Grammar and Vocabulary', N'CFW_ASPECT_IRL')
INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'OLGRAM', N'Online Grammar and Vocabulary', N'CFW_ASPECT_USA')

INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'OLCOMBI', N'Online Combined', N'CFW_ASPECT_GBR')
INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'OLCOMBI', N'Online Combined', N'CFW_ASPECT_IRL')
INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'OLCOMBI', N'Online Combined', N'CFW_ASPECT_USA')

INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'OLOTOILS', N'Online One to One', N'CFW_ASPECT_GBR')
INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'OLOTOILS', N'Online One to One', N'CFW_ASPECT_IRL')
INSERT [APTTUS].[PRODUCT] ([ProductId], [ProductGroupId], [ProductCode], [ProductName], [classdb]) VALUES ((SELECT MAX(ProductId) from [APTTUS].[PRODUCT]) +1, 1, N'OLOTOILS', N'Online One to One', N'CFW_ASPECT_USA')
SET IDENTITY_INSERT [APTTUS].[PRODUCT] OFF
