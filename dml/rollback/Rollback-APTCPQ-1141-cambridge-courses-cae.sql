DECLARE @RecordCount int

--- Delete the product attribute mappings
BEGIN TRANSACTION RollBackCaeCambProdAttributes

  DELETE
		FROM [ClassIntegration].[APTTUS].[MAP_PRODUCT_ATTRIBUTE] mpa where mpa.ProductId in (SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName in ('Cambridge CAE 4 weeks', 'Cambridge CAE 8 weeks', 'Cambridge CAE Intensive 4 weeks', 'Cambridge CAE Intensive 8 weeks'))

	IF (@@ROWCOUNT > 0)
		BEGIN
      		COMMIT TRANSACTION RollBackCaeCambProdAttributes
      		PRINT 'CFE products deleted successfully'
    	END
  	ELSE
    	BEGIN
      		ROLLBACK TRANSACTION RollBackCaeCambProdAttributes
      		PRINT 'Rolling back the Cae Cambrige Product-Attribute Mappings'
    	END
  END

  --- Delete the product mappings
  BEGIN TRANSACTION RollBackCaeCambProductMappings

    DELETE
  		FROM [ClassIntegration].[APTTUS].[PRODUCT] p where p.ProductCode in ('CAMCGEN4','CAMCGEN8','CAMCINT4','CAMCINT8')

  	IF (@@ROWCOUNT > 0)
  		BEGIN
        		COMMIT TRANSACTION RollBackCaeCambProductMappings
        		PRINT 'CFE product mappings deleted successfully'
      	END
    	ELSE
      	BEGIN
        		ROLLBACK TRANSACTION RollBackCaeCambProductMappings
        		PRINT 'Rolling back the Cae Cambrige Product Mappings'
      	END
    END

    --- Delete the product group mappings
    BEGIN TRANSACTION RollBackCaeCambProductGroupMappings

      DELETE
    		FROM [ClassIntegration].[APTTUS].[PRODUCTGROUP] mp where mp.ProductGroupName = 'Cambridge CAE'

    	IF (@@ROWCOUNT > 0)
    		BEGIN
          		COMMIT TRANSACTION RollBackCaeCambProductGroupMappings
          		PRINT 'CFE product mappings deleted successfully'
        	END
      	ELSE
        	BEGIN
          		ROLLBACK TRANSACTION RollBackCaeCambProductGroupMappings
          		PRINT 'Rolling back the Cae Cambrige Product-group Mappings'
        	END
      END
