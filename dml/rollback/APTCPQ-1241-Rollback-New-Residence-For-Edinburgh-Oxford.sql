DECLARE @RecordCount int

--- Delete the product mappings
BEGIN TRANSACTION RollBackResidenceProducts

	DELETE
		FROM [ClassIntegration].[APTTUS].[MAP_PRODUCT_ATTRIBUTE]  where productId = (Select productId from [ClassIntegration].[APTTUS].[PRODUCT] p
      where ProductName = 'Residence Arran House Single No Meals')

  DELETE
		FROM [ClassIntegration].[APTTUS].[PRODUCT]  where ProductName = 'Residence Arran House Single No Meals'

  DELETE
		FROM [ClassIntegration].[APTTUS].[MAP_PRODUCT_ATTRIBUTE]  where productId = (Select productId from [ClassIntegration].[APTTUS].[PRODUCT] p
      where ProductName = 'Residence Student Castle Single No Meals')

  DELETE
		FROM [ClassIntegration].[APTTUS].[PRODUCT]  where ProductName = 'Residence Student Castle Single No Meals'



	IF (@@ROWCOUNT > 0)
		BEGIN
      		COMMIT TRANSACTION RollBackResidenceProducts
      		PRINT 'Residence Products deleted successfully'
    	END
  	ELSE
    	BEGIN
      		ROLLBACK TRANSACTION RollBackResidenceProducts
      		PRINT 'Rolling back the RollBackResidenceProducts'
    	END
