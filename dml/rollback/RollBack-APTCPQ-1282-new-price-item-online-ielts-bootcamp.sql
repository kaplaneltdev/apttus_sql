DECLARE @RecordCount int

--- Delete the product mappings
BEGIN TRANSACTION RollBackBootCampProduct

  DELETE
		FROM [ClassIntegration].[APTTUS].[PRODUCT]  where ProductName = 'Online IELTS Bootcamp'

	IF (@@ROWCOUNT > 0)
		BEGIN
      		COMMIT TRANSACTION RollBackBootCampProduct
      		PRINT 'BootCamp Product deleted successfully'
    	END
  	ELSE
    	BEGIN
      		ROLLBACK TRANSACTION RollBackBootCampProduct
      		PRINT 'Rolling back the RollBackBootCampProduct'
    	END
