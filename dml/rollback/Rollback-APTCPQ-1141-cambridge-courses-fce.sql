DECLARE @RecordCount int

--- Delete the product attribute mappings
BEGIN TRANSACTION RollBackFceCambProdAttributes

  DELETE
		FROM [ClassIntegration].[APTTUS].[MAP_PRODUCT_ATTRIBUTE] mpa where mpa.ProductId in (SELECT p.ProductId from [APTTUS].[PRODUCT] p where p.ProductName in ('Cambridge FCE 4 weeks', 'Cambridge FCE 8 weeks', 'Cambridge FCE Intensive 4 weeks', 'Cambridge FCE Intensive 8 weeks'))

	IF (@@ROWCOUNT > 0)
		BEGIN
      		COMMIT TRANSACTION RollBackFceCambProdAttributes
      		PRINT 'CFE products deleted successfully'
    	END
  	ELSE
    	BEGIN
      		ROLLBACK TRANSACTION RollBackFceCambProdAttributes
      		PRINT 'Rolling back the Fce Cambrige Product-Attribute Mappings'
    	END
  END

  --- Delete the product mappings
  BEGIN TRANSACTION RollBackFceCambProductMappings

    DELETE
  		FROM [ClassIntegration].[APTTUS].[PRODUCT] p where p.ProductCode in ('CAMFGEN4','CAMFGEN8','CAMFINT4','CAMFINT8')

  	IF (@@ROWCOUNT > 0)
  		BEGIN
        		COMMIT TRANSACTION RollBackFceCambProductMappings
        		PRINT 'CFE product mappings deleted successfully'
      	END
    	ELSE
      	BEGIN
        		ROLLBACK TRANSACTION RollBackFceCambProductMappings
        		PRINT 'Rolling back the Fce Cambrige Product Mappings'
      	END
    END

    --- Delete the product group mappings
    BEGIN TRANSACTION RollBackFceCambProductGroupMappings

      DELETE
    		FROM [ClassIntegration].[APTTUS].[PRODUCTGROUP] mp where mp.ProductGroupName = 'Cambridge FCE'

    	IF (@@ROWCOUNT > 0)
    		BEGIN
          		COMMIT TRANSACTION RollBackFceCambProductGroupMappings
          		PRINT 'CFE product mappings deleted successfully'
        	END
      	ELSE
        	BEGIN
          		ROLLBACK TRANSACTION RollBackFceCambProductGroupMappings
          		PRINT 'Rolling back the Fce Cambrige Product-group Mappings'
        	END
      END
