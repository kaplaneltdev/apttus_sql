USE ClassIntegration
GRANT EXECUTE ON TYPE::[dbo].[auditLog] TO [Mule.Integration.User] AS [dbo]
GRANT SELECT ON [APTTUS].[ATTRIBUTE] TO [Mule.Integration.User] AS [dbo]
GRANT SELECT ON [APTTUS].[MAP_PRODUCT_ATTRIBUTE] TO [Mule.Integration.User] AS [dbo]
GRANT SELECT ON [APTTUS].[MAP_PRODUCTGROUP_ATTRIBUTE] TO [Mule.Integration.User] AS [dbo]
GRANT SELECT ON [APTTUS].[PRODUCT] TO [Mule.Integration.User] AS [dbo]
GRANT SELECT ON [APTTUS].[PRODUCTGROUP] TO [Mule.Integration.User] AS [dbo]
GRANT EXECUTE ON [APTTUS].[CreateCreditInvoiceLine] TO [Mule.Integration.User] AS [dbo]
GRANT EXECUTE ON [APTTUS].[CreateInvoiceLine] TO [Mule.Integration.User] AS [dbo]
GRANT EXECUTE ON [APTTUS].[GetBookedItem] TO [Mule.Integration.User] AS [dbo]
GRANT EXECUTE ON [APTTUS].[GetPriceItem] TO [Mule.Integration.User] AS [dbo]
GRANT EXECUTE ON [APTTUS].[GetProductCode] TO [Mule.Integration.User] AS [dbo]
GRANT EXECUTE ON [APTTUS].[UpsertECourse] TO [Mule.Integration.User] AS [dbo]
GRANT EXECUTE ON [APTTUS].[UpsertEHost] TO [Mule.Integration.User] AS [dbo]
GRANT EXECUTE ON [APTTUS].[UpsertEnrolment] TO [Mule.Integration.User] AS [dbo]
GRANT EXECUTE ON [APTTUS].[UpsertESundry] TO [Mule.Integration.User] AS [dbo]
GRANT EXECUTE ON [APTTUS].[UpsertETransfer] TO [Mule.Integration.User] AS [dbo]
GRANT EXECUTE ON [APTTUS].[UpsertStudent] TO [Mule.Integration.User] AS [dbo]
GRANT EXECUTE ON [APTTUS].[GetSchoolId] TO [Mule.Integration.User] AS [dbo]